<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:updateEndCustomerAddressResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <updateEndCustomerResponse>
            <nh:updateEndCustomerAddressResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
               </nh:header>
               <nh:result>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2e99cd42-0e74-4783-aeef-0f7cda28290e</nhm:transactionId>
               </nh:result>
            </nh:updateEndCustomerAddressResponse>
         </updateEndCustomerResponse>
      </ser-root:updateEndCustomerAddressResponse>
   </soapenv:Body>
</soapenv:Envelope>