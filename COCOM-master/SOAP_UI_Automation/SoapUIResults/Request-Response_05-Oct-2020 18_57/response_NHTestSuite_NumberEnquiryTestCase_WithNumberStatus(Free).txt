<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:numberEnquiryResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <numberQueryResponse>
            <nh:numberQueryResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>success</nhm:errorType>
                     <nhm:errorCode>0</nhm:errorCode>
                     <nhm:errorDescription>Action Performed SuccessFully</nhm:errorDescription>
                     <nhm:businessErrorDescription/>
                  </nhm:errorMessage>
               </nh:header>
               <nh:queryOutputDetails>
                  <nhm:numberList xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:cliDetails>
                        <nhm:areaCode>021</nhm:areaCode>
                        <nhm:areaCodeExtn>560</nhm:areaCodeExtn>
                        <nhm:rangeStart>5499</nhm:rangeStart>
                        <nhm:rangeEnd>5499</nhm:rangeEnd>
                     </nhm:cliDetails>
                     <nhm:cliStatus>Free</nhm:cliStatus>
                  </nhm:numberList>
                  <nhm:numberList xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:cliDetails>
                        <nhm:areaCode>021</nhm:areaCode>
                        <nhm:areaCodeExtn>560</nhm:areaCodeExtn>
                        <nhm:rangeStart>6145</nhm:rangeStart>
                        <nhm:rangeEnd>6145</nhm:rangeEnd>
                     </nhm:cliDetails>
                     <nhm:cliStatus>Free</nhm:cliStatus>
                  </nhm:numberList>
                  <nhm:numberList xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:cliDetails>
                        <nhm:areaCode>021</nhm:areaCode>
                        <nhm:areaCodeExtn>560</nhm:areaCodeExtn>
                        <nhm:rangeStart>6148</nhm:rangeStart>
                        <nhm:rangeEnd>6148</nhm:rangeEnd>
                     </nhm:cliDetails>
                     <nhm:cliStatus>Free</nhm:cliStatus>
                  </nhm:numberList>
                  <nhm:numberList xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:cliDetails>
                        <nhm:areaCode>021</nhm:areaCode>
                        <nhm:areaCodeExtn>560</nhm:areaCodeExtn>
                        <nhm:rangeStart>6149</nhm:rangeStart>
                        <nhm:rangeEnd>6149</nhm:rangeEnd>
                     </nhm:cliDetails>
                     <nhm:cliStatus>Free</nhm:cliStatus>
                  </nhm:numberList>
                  <nhm:numberList xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:cliDetails>
                        <nhm:areaCode>021</nhm:areaCode>
                        <nhm:areaCodeExtn>560</nhm:areaCodeExtn>
                        <nhm:rangeStart>6154</nhm:rangeStart>
                        <nhm:rangeEnd>6154</nhm:rangeEnd>
                     </nhm:cliDetails>
                     <nhm:cliStatus>Free</nhm:cliStatus>
                  </nhm:numberList>
               </nh:queryOutputDetails>
            </nh:numberQueryResponse>
         </numberQueryResponse>
      </ser-root:numberEnquiryResponse>
   </soapenv:Body>
</soapenv:Envelope>