<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:portInResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <newPortInResponse>
            <nh:newPortInResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>inputValidation</nhm:errorType>
                     <nhm:errorCode>WM_01</nhm:errorCode>
                     <nhm:errorDescription>Error: Customer Type is required at path: ../endCustomerDetails/customerType
Error: 1) The Porting Date should be excluding Saturdays.
2) The Porting Date should be excluding Sundays.
3) The Porting Date should be at least 7 working days ahead of current date and not more than 14 working days in the future.
 at path: newPortInRequest/portInDetails/portInDate</nhm:errorDescription>
                     <nhm:businessErrorDescription>Please enter valid data.</nhm:businessErrorDescription>
                  </nhm:errorMessage>
               </nh:header>
            </nh:newPortInResponse>
         </newPortInResponse>
      </ser-root:portInResponse>
   </soapenv:Body>
</soapenv:Envelope>