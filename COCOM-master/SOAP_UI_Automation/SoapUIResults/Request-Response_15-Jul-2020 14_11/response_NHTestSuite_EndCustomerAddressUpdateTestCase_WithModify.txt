<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:updateEndCustomerAddressResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <updateEndCustomerResponse>
            <nh:updateEndCustomerAddressResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>inputValidation</nhm:errorType>
                     <nhm:errorCode>WM_01</nhm:errorCode>
                     <nhm:errorDescription>Error: [ISC.0082.9009] Child element cifNIF {http://www.colt.net/xml/ns/cbe/nhm/internal/v1.0} at position 5 is unexpected at path: /updateEndCustomerAddressRequest/endCustomerAddressUpdateDetail/endCustomerDetails/endCustomerAddress/</nhm:errorDescription>
                     <nhm:businessErrorDescription>Please enter valid data.</nhm:businessErrorDescription>
                  </nhm:errorMessage>
               </nh:header>
            </nh:updateEndCustomerAddressResponse>
         </updateEndCustomerResponse>
      </ser-root:updateEndCustomerAddressResponse>
   </soapenv:Body>
</soapenv:Envelope>