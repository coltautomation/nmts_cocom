<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:validateGeoNumberResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <validateGeoNumberResponse>
            <nh:validateGeoNumberResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>Service Availability</nhm:errorType>
                     <nhm:errorCode>WM_10</nhm:errorCode>
                     <nhm:errorDescription>LAC Address validation failed.
ErrorDescription: [ISS.0088.9138] Input parameters do not conform to targetInputSignature: 
	errorCode=VV-005
	pathName=/tns:validateGeoNumber/geoNumberRequest/gvr:validateGeoNumberRequest[0]/city
	errorMessage=[ISC.0082.9034] Field is absent, field must exist</nhm:errorDescription>
                     <nhm:businessErrorDescription>Your transaction has not been completed. Please resubmit or contact ResellerSupport.Voice@colt.net.</nhm:businessErrorDescription>
                  </nhm:errorMessage>
               </nh:header>
            </nh:validateGeoNumberResponse>
         </validateGeoNumberResponse>
      </ser-root:validateGeoNumberResponse>
   </soapenv:Body>
</soapenv:Envelope>