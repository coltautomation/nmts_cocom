<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.colt.net/numberHosting/v1" xmlns:v11="http://www.colt.net/xml/ns/NumberHosting/v1.0" xmlns:v12="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
   <soapenv:Header/>
   <soapenv:Body>
      <v1:getTransactionDetails>
         <getTransactionDetailsRequest>
            <v11:getTransactionDetailsRequest>
               <v11:serviceHeader>
                  <!--Optional:-->
                  <v12:senderSystem>${senderSystem}</v12:senderSystem>
                  <!--Optional:-->
                  <v12:creationTime></v12:creationTime>
               </v11:serviceHeader>
               <v11:transactionDetails>
                  <v12:resellerProfile>
                     <v12:serviceProfile>${serviceProfile}</v12:serviceProfile>
                     <v12:productID>${productID}</v12:productID>
                     <v12:Country>${Country}</v12:Country>
                     <!--Optional:-->
                     <v12:subResellerID></v12:subResellerID>
                  </v12:resellerProfile>
                  <v12:transactionId>${transactionId}</v12:transactionId>
                  <!--Optional:-->
                  <v12:transactionUpdateDate></v12:transactionUpdateDate>
               </v11:transactionDetails>
            </v11:getTransactionDetailsRequest>
         </getTransactionDetailsRequest>
      </v1:getTransactionDetails>
   </soapenv:Body>
</soapenv:Envelope>