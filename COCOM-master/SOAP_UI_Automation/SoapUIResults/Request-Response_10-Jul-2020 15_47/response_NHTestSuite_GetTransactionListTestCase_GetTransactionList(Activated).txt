<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:getTransactionListResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <getTransactionListResponse>
            <nh:getTransactionListResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
               </nh:header>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">67e1e638-ff1e-4af6-be1f-f130e0b806c5</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-09-10T05:30:10.000+01:00</nhm:transactionDate>
               </nh:transactionList>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">1e0a7133-8bf2-486e-92ba-3da98056afa8</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-09-20T06:46:48.000+01:00</nhm:transactionDate>
               </nh:transactionList>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">60955f99-5472-4881-8c86-d901682854fb</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-09-24T08:41:43.000+01:00</nhm:transactionDate>
               </nh:transactionList>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ee853b01-9aa7-452b-924e-7279d3e4eec8</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-09-17T10:19:14.000+01:00</nhm:transactionDate>
               </nh:transactionList>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">151dc5cb-6932-4a1c-a0c8-93d565e2c9c8</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-09-06T10:40:48.000+01:00</nhm:transactionDate>
               </nh:transactionList>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">e09b4f31-36e9-419c-b005-dfa8e32b742b</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-09-11T12:01:29.000+01:00</nhm:transactionDate>
               </nh:transactionList>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">83d46e2f-ea78-4c35-81c2-2a693a32459a</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-09-10T06:52:48.000+01:00</nhm:transactionDate>
               </nh:transactionList>
               <nh:transactionList>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">fd415685-2f9d-4fc3-871d-ea335c86ea04</nhm:transactionId>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Activation</nhm:transactionType>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2019-10-09T02:36:25.000+01:00</nhm:transactionDate>
               </nh:transactionList>
            </nh:getTransactionListResponse>
         </getTransactionListResponse>
      </ser-root:getTransactionListResponse>
   </soapenv:Body>
</soapenv:Envelope>