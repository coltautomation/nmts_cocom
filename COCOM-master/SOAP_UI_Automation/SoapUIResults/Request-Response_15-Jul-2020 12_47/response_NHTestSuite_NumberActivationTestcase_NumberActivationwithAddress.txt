<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:numberActivationResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <numberActivationResponse>
            <nh:numberActivationResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>inputValidation</nhm:errorType>
                     <nhm:errorCode>WM_01</nhm:errorCode>
                     <nhm:errorDescription>Error: [ISC.0082.9010] Incomplete content - one or more child elements are expected at path: /numberActivationRequest/activationDetails/cliDetails/
Error: [ISC.0082.9460] No matching enumeration value at path: /numberActivationRequest/activationDetails/directoryServicesDetails/secretListing
Error: Range Start should always be between 3 to 4 digits, Range End should always be between 3 to 4 digits, Total Length of Number Combination of LAC, Main Number and Range Must be 9. at path: ../cliDetails
Error: Telephone Number of Directory Service Update is required. at path: ../directoryServicesDetails/telephoneNumber
Error: Range Start should always be between 3 to 4 digits, Range End should always be between 3 to 4 digits, Total Length of Number Combination of LAC, Main Number and Range Must be 9. at path: ..directoryServiceDetails/cliDetails
Error: Directory Service Update UsoWpStatusFalg is required for Belgium at path: ../directoryServicesDetails/usoWPStatusFlag
Error: Directory Service Update OtheMediaStatusFlag is required for Belgium at path: ../directoryServicesDetails/otheMediaStatusFlag
Error: Directory Service Update UsoDAStatusFlag is required for Belgium at path: ../directoryServicesDetails/usoDAStatusFlag
Error: Directory Service Update SalesFlag is required for Belgium at path: ../directoryServicesDetails/salesFlag
Error: Directory Service Update ReverseQueryFlag is required for Belgium at path: ../directoryServicesDetails/reverseQueryFlag
Error: Directory Service Update ServiceType is required for Belgium at path: ../directoryServicesDetails/serviceType
Error: Directory Service Update DeviceTypeId is required for Belgium at path: ../directoryServicesDetails/deviceTypeId
Error: Directory Service Update ListingLanguage is required for Belgium at path: ../directoryServicesDetails/listingLanguage
Error: Value entered is not allowed at path: ../activationDetails/directoryServicesDetails/orderType</nhm:errorDescription>
                     <nhm:businessErrorDescription>Please enter valid data.</nhm:businessErrorDescription>
                  </nhm:errorMessage>
               </nh:header>
            </nh:numberActivationResponse>
         </numberActivationResponse>
      </ser-root:numberActivationResponse>
   </soapenv:Body>
</soapenv:Envelope>