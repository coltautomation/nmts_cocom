<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:numberEnquiryResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <numberQueryResponse>
            <nh:numberQueryResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>supportServiceAvailability</nhm:errorType>
                     <nhm:errorCode>WM_10</nhm:errorCode>
                     <nhm:errorDescription>Exception in NumberEnquiry service. com.wm.app.b2b.server.AccessException: [ISS.0084.9004] Access Denied
	at com.wm.app.b2b.server.ACLManager.process(ACLManager.java:238)
	at com.wm.app.b2b.server.invoke.DispatchProcessor.process(Dispat</nhm:errorDescription>
                     <nhm:businessErrorDescription/>
                  </nhm:errorMessage>
               </nh:header>
            </nh:numberQueryResponse>
         </numberQueryResponse>
      </ser-root:numberEnquiryResponse>
   </soapenv:Body>
</soapenv:Envelope>