<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:getTransactionDetailsResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <getTransactionDetailsResponse>
            <nh:getTransactionDetailsResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
               </nh:header>
               <nh:transactionDetails>
                  <nhm:transactionType xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">Number Deactivation</nhm:transactionType>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">acbca201-90fb-4b39-a4ca-674700dde18d</nhm:transactionId>
                  <nhm:cliDetails xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:areaCode>021</nhm:areaCode>
                     <nhm:areaCodeExtn>560</nhm:areaCodeExtn>
                     <nhm:rangeStart>6161</nhm:rangeStart>
                     <nhm:rangeEnd>6161</nhm:rangeEnd>
                  </nhm:cliDetails>
                  <nhm:transactionDate xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">2020-07-09T08:51:41.000+01:00</nhm:transactionDate>
                  <nhm:endCustomerDetails xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:endCustomerAddress>
                        <nhm:postalZipCode/>
                     </nhm:endCustomerAddress>
                  </nhm:endCustomerDetails>
                  <nhm:userName xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">colt345</nhm:userName>
                  <nhm:transactionStatus xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:transactionStatus>Completed</nhm:transactionStatus>
                     <nhm:transactionDescription>Request has been processed successfully.</nhm:transactionDescription>
                     <nhm:errorDescription/>
                  </nhm:transactionStatus>
                  <nhm:portNumberDetails xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:cliDetails>
                        <nhm:areaCode>021</nhm:areaCode>
                        <nhm:areaCodeExtn>560</nhm:areaCodeExtn>
                        <nhm:rangeStart>6161</nhm:rangeStart>
                        <nhm:rangeEnd>6161</nhm:rangeEnd>
                     </nhm:cliDetails>
                  </nhm:portNumberDetails>
               </nh:transactionDetails>
            </nh:getTransactionDetailsResponse>
         </getTransactionDetailsResponse>
      </ser-root:getTransactionDetailsResponse>
   </soapenv:Body>
</soapenv:Envelope>