<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:numberReactivationResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <numberReactivationResponse>
            <nh:numberReActivationResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
               </nh:header>
               <nh:result>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">0da5d62f-dddf-465c-8105-3b7c91259dcb</nhm:transactionId>
               </nh:result>
            </nh:numberReActivationResponse>
         </numberReactivationResponse>
      </ser-root:numberReactivationResponse>
   </soapenv:Body>
</soapenv:Envelope>