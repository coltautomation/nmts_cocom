<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://www.colt.net/numberHosting/v1" xmlns:v11="http://www.colt.net/xml/ns/NumberHosting/v1.0" xmlns:v12="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
   <soapenv:Header/>
   <soapenv:Body>
      <v1:updateEndCustomerAddress>
         <updateEndCustomerRequest>
            <v11:updateEndCustomerAddressRequest>
               <v11:serviceHeader>
                  <!--Optional:-->
                  <v12:senderSystem>${senderSystem}</v12:senderSystem>
                  <!--Optional:-->
                  <v12:creationTime></v12:creationTime>
               </v11:serviceHeader>
               <v11:endCustomerAddressUpdateDetail>
                  <v12:resellerProfile>
                     <v12:serviceProfile>${serviceProfile}</v12:serviceProfile>
                     <v12:productID>${productID}</v12:productID>
                     <v12:Country>${Country}</v12:Country>
                     <!--Optional:-->
                     <v12:subResellerID></v12:subResellerID>
                  </v12:resellerProfile>
                  <!--Optional:-->
                  <v12:action>${action1}</v12:action>
                  <v12:cliDetails>
                     <!--Optional:-->
                     <v12:areaCode>${areaCode}</v12:areaCode>
                     <!--Optional:-->
                     <v12:areaCodeExtn> ${areaCodeExtn}</v12:areaCodeExtn>
                     <!--Optional:-->
                     <v12:rangeStart>${rangeStart1}</v12:rangeStart>
                     <!--Optional:-->
                     <v12:rangeEnd>${rangeEnd1}</v12:rangeEnd>
                     <!--Optional:-->
                     <v12:startFullNumber></v12:startFullNumber>
                     <!--Optional:-->
                     <v12:endFullNumber></v12:endFullNumber>
                  </v12:cliDetails>
                  <!--Optional:-->
                  <v12:endCustomerDetails>
                     <!--Optional:-->
                     <v12:endCustomerName></v12:endCustomerName>
                     <!--Optional:-->
                     <v12:customerType></v12:customerType>
                     <!--Optional:-->
                     <v12:title></v12:title>
                     <!--Optional:-->
                     <v12:firstName></v12:firstName>
                     <!--Optional:-->
                     <v12:lastName></v12:lastName>
                     <!--Optional:-->
                     <v12:registeredName></v12:registeredName>
                     <!--Optional:-->
                     <v12:customerVATnumber></v12:customerVATnumber>
                     <!--Optional:-->
                     <v12:customerIDNumber></v12:customerIDNumber>
                     <!--Optional:-->
                     <v12:endCustomerDateOfBirth></v12:endCustomerDateOfBirth>
                     <!--Optional:-->
                     <v12:endCustomerLanguage></v12:endCustomerLanguage>
                     <v12:endCustomerAddress>
                        <!--Optional:-->
                        <v12:floorSuite></v12:floorSuite>
                        <!--Optional:-->
                        <v12:flatNumber></v12:flatNumber>
                        <!--Optional:-->
                        <v12:premisesNumber></v12:premisesNumber>
                        <!--Optional:-->
                        <v12:premisesNumberLetter></v12:premisesNumberLetter>
                        <!--Optional:-->
                        <v12:buildingName></v12:buildingName>
                        <!--Optional:-->
                        <v12:departmentBranch></v12:departmentBranch>
                        <!--Optional:-->
                        <v12:streetName></v12:streetName>
                        <!--Optional:-->
                        <v12:municipalityName></v12:municipalityName>
                        <!--Optional:-->
                        <v12:cityTown></v12:cityTown>
                        <!--Optional:-->
                        <v12:state></v12:state>
                        <v12:postalZipCode></v12:postalZipCode>
                        <!--Optional:-->
                        <v12:country></v12:country>
                        <!--Optional:-->
                        <v12:poBoxNumber></v12:poBoxNumber>
                        <!--Optional:-->
                        <v12:cifNIF></v12:cifNIF>
                        <!--Optional:-->
                        <v12:extensionNumber></v12:extensionNumber>
                     </v12:endCustomerAddress>
                  </v12:endCustomerDetails>
                  <!--Optional:-->
                  <v12:userName></v12:userName>
                  <!--Optional:-->
                  <v12:additionalCustRef1></v12:additionalCustRef1>
                  <!--Optional:-->
                  <v12:additionalCustRef2></v12:additionalCustRef2>
                  <!--Optional:-->
                  <v12:additionalCustRef3></v12:additionalCustRef3>
               </v11:endCustomerAddressUpdateDetail>
            </v11:updateEndCustomerAddressRequest>
         </updateEndCustomerRequest>
      </v1:updateEndCustomerAddress>
   </soapenv:Body>
</soapenv:Envelope>