<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:numberDeactivationResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <numberDeactivationResponse>
            <nh:numberDeactivationResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
               </nh:header>
               <nh:result>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">fa2c08bc-d608-4a4b-9183-8fbdaf0128fb</nhm:transactionId>
               </nh:result>
            </nh:numberDeactivationResponse>
         </numberDeactivationResponse>
      </ser-root:numberDeactivationResponse>
   </soapenv:Body>
</soapenv:Envelope>