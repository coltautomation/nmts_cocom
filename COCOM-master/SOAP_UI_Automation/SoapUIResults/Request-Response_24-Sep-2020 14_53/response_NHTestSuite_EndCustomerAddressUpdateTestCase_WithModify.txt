<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:updateEndCustomerAddressResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <updateEndCustomerResponse>
            <nh:updateEndCustomerAddressResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>inputValidation</nhm:errorType>
                     <nhm:errorCode>WM_03</nhm:errorCode>
                     <nhm:errorDescription>service profile being searched is invalid and API authorization has failed</nhm:errorDescription>
                     <nhm:businessErrorDescription>Invalid user name or user password. Please enter valid user name or password.</nhm:businessErrorDescription>
                  </nhm:errorMessage>
               </nh:header>
            </nh:updateEndCustomerAddressResponse>
         </updateEndCustomerResponse>
      </ser-root:updateEndCustomerAddressResponse>
   </soapenv:Body>
</soapenv:Envelope>