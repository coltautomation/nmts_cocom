<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:numberActivationResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <numberActivationResponse>
            <nh:numberActivationResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
               </nh:header>
               <nh:result>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">bff44b6c-01e7-48e1-ba8d-9b08dd735cc6</nhm:transactionId>
               </nh:result>
            </nh:numberActivationResponse>
         </numberActivationResponse>
      </ser-root:numberActivationResponse>
   </soapenv:Body>
</soapenv:Envelope>