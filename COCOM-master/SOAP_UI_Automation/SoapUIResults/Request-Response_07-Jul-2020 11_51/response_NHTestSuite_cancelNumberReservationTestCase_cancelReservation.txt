<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:cancelReservationResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <cancelReservationResponse>
            <nh:cancelReservationResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">SUCCESS</nhm:status>
               </nh:header>
               <nh:result>
                  <nhm:transactionId xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">159ecb66-550c-4c67-8643-0c330492b88a</nhm:transactionId>
               </nh:result>
            </nh:cancelReservationResponse>
         </cancelReservationResponse>
      </ser-root:cancelReservationResponse>
   </soapenv:Body>
</soapenv:Envelope>