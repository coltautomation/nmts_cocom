<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:portInResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <newPortInResponse>
            <nh:newPortInResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>inputValidation</nhm:errorType>
                     <nhm:errorCode>WM_01</nhm:errorCode>
                     <nhm:errorDescription>Error: [ISC.0082.9469] Value does not match pattern(s) at path: /newPortInRequest/portInDetails/companyRegistrationNumber
Error: Directory Service Update Telephone Number is required. at path: ../directoryServicesDetails/telephoneNumber
Error: Both file name and file content must be present. at path: newPortInRequest/portInDetails/portingAuthority/letterOfAuthority</nhm:errorDescription>
                     <nhm:businessErrorDescription>Please enter valid data.</nhm:businessErrorDescription>
                  </nhm:errorMessage>
               </nh:header>
            </nh:newPortInResponse>
         </newPortInResponse>
      </ser-root:portInResponse>
   </soapenv:Body>
</soapenv:Envelope>