<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <ser-root:numberActivationResponse xmlns:ser-root="http://www.colt.net/numberHosting/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <numberActivationResponse>
            <nh:numberActivationResponse xmlns:nh="http://www.colt.net/xml/ns/NumberHosting/v1.0">
               <nh:header>
                  <nhm:status xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">ERROR</nhm:status>
                  <nhm:errorMessage xmlns:nhm="http://www.colt.net/xml/ns/cbe/nhm/v1.0">
                     <nhm:errorType>inputValidation</nhm:errorType>
                     <nhm:errorCode>WM_01</nhm:errorCode>
                     <nhm:errorDescription>Error: [ISC.0082.9460] No matching enumeration value at path: /numberActivationRequest/activationDetails/directoryServicesDetails/secretListing
Error: Directory Service Update Telephone Number is required. at path: ../directoryServicesDetails/telephoneNumber
Error: Directory Service Update End Customer Name is required at path: ../directoryServicesDetails/endCustomerName
Error: Directory Service Update either Building Name or the Premise Number should be present at path: ../directoryServicesDetails/endCustomerAddress/premisesNumber
Error: Directory Service Update Street Name is required at path: ../directoryServicesDetails/endCustomerAddress/streetName
Error: Directory Service Update CityTown is required at path: ../directoryServicesDetails/endCustomerAddress/cityTown
Error: Directory Service Update Postal Zip Code is required at path: ../directoryServicesDetails/endCustomerAddress/postalZipCode
Error: Directory Service Update Telephone Number is required at path: ../directoryServicesDetails/endCustomerAddress/telephoneNumber
Error: Directory Service Update Line Type is required at path: ../directoryServicesDetails/lineType
Error: Directory Service Update Tariff is required at path: ../directoryServicesDetails/tariff
Error: Directory Service Update Entry Type is required at path: ../directoryServicesDetails/entryType
Error: Directory Service Update Type Face is required at path: ../directoryServicesDetails/typeFace
Error: Directory Service Update Listing Category is required at path: ../directoryServicesDetails/listingCategory
Error: Value entered is not allowed at path: ../activationDetails/directoryServicesDetails/orderType</nhm:errorDescription>
                     <nhm:businessErrorDescription>Please enter valid data.</nhm:businessErrorDescription>
                  </nhm:errorMessage>
               </nh:header>
            </nh:numberActivationResponse>
         </numberActivationResponse>
      </ser-root:numberActivationResponse>
   </soapenv:Body>
</soapenv:Envelope>