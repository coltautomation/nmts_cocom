<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Body>
      <soapenv:Fault>
         <faultcode>soapenv:Server</faultcode>
         <faultstring>[ISS.0088.9112] An Exception was thrown in the server:[ISC.0076.9222] Document/Literal Coder: decoding error; cant convert SOAP Message to IS Document</faultstring>
         <faultactor>http://wmisb151.internal.colt.net:80/ws</faultactor>
         <detail>
            <webM:exception xmlns:webM="http://www.webMethods.com/2001/10/soap/encoding">
               <webM:className>com.wm.app.b2b.server.wss.coder.AxiomCoderException</webM:className>
               <webM:message xml:lang="">[ISC.0076.9222] Document/Literal Coder: decoding error; cant convert SOAP Message to IS Document</webM:message>
            </webM:exception>
         </detail>
      </soapenv:Fault>
   </soapenv:Body>
</soapenv:Envelope>