package ScriptHelper;
import static org.testng.Assert.fail;

import java.awt.List;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import org.bouncycastle.operator.InputDecryptor;
import org.dom4j.DocumentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.corba.se.spi.orbutil.fsm.Input;
import com.sun.xml.internal.ws.org.objectweb.asm.Label;

import Driver.DriverHelper;
import Driver.Log;
import Driver.PropertyReader;
import Driver.xmlreader;
import Reporter.ExtentTestManager;
public class NumberHostingHelper extends DriverHelper 
{
	WebElement el;
	xmlreader xml = new xmlreader("src\\Locators\\NumberHosting.xml");
//my
	public NumberHostingHelper(WebDriver dr) 
	{
		super(dr);
	}

	public void OpenApplication() throws Exception 
	{
		openurl("NH");
		PropertyReader pr = new PropertyReader();
		String URL = null;
		URL = pr.readproperty("NH_URL");
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Opening the Url : <font color='green'> " + URL + " </font>");
	}

	public String NumberQueryFamily(Object[][] Inputdata) 
	{
		String SearchCreteria = null;
		for (int i = 0; i < Inputdata.length; i++) 
		{
			SearchCreteria = Inputdata[i][0].toString();
		}
		return SearchCreteria;
	}

	public void Search(Object[][] Inputdata) throws Exception 
	{
		switch (NumberQueryFamily(Inputdata)) 
		{
		case "Transaction ID": 
		{
			NumberQuiryUsingTransactionID(Inputdata);
			break;
		}
		
		case "Number Range": 
		{
			NumberQuiryUsingNumberRange(Inputdata);
			break;
		}
		case "Customer Reference": 
		{
			NumberQuiryUsingCustomerReferance(Inputdata);
			break;
		}
		case "Number Status": 
		{
			NumberQuiryUsingNumberStatus(Inputdata);
			break;
		}
		case "Port-out":
		{
			portoutforreserverd(Inputdata);
			break;
		}
		case "Cancel Port-in":
		{
			CancelPortINrequest(Inputdata);
			break;
		}
		case "Telephone History":
		{
			TelephoneNuberHistory(Inputdata);
			break;
		}
		case "Number HostingTransaction":
		{
			NumberHosting_Transaction_StatusQuery(Inputdata);
			break;
		}
		case "Change Port-in Date":
		{
			Change_Port_in_Date(Inputdata);
			break;
		}
		case "Update Porting Request":
		{
			Update_porting_request(Inputdata);
			break;
		}
		default: 
		{
			log("It seems like, Selected Creteria is not valid in Sheet!!! Please update it in sheet!! ");
		}
		}
	}
	public void Change_Port_in_Date(Object[][] Inputdata) throws Exception {
		for (int i = 0; i < Inputdata.length; i++) 
		{
			WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
			Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
			log("click on managing porting");
			Thread.sleep(1000);
			Clickon(getwebelement(xml.getlocator("//locators/ChangePortInDate")));
			implicitwait(10);
			log("click on Change Port-in Date request");
//			int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
//			System.out.println(allData);
//			for (int k = 0; k <= allData; k++) 
//			{
//				if (k != 0 && k % 10 == 0) 
//				{
//					Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
//					log("click on ANH and next button");
//				}
//				Thread.sleep(1000); 
//				String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
//				System.out.println(data);
//				Thread.sleep(1000);
//				if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
//				{
//					Thread.sleep(2000);
//					safeJavaScriptClick(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
//					System.out.println(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
//					log("click on ANH");
//					// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
//					Thread.sleep(2000);
//					break;
//				}
//			}
			String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
			int allData=Integer.parseInt(gett);
			//int allData = getwebelementscount(xml.getlocator("//locators/pagelimt"));//
			System.out.println(allData);
			for (int k = 0; k <= allData; k++) 
			{
				Thread.sleep(3000);
//				if (k != 0 && k % 10 == 0) 
//				{
//					Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
//					log("click on ANH and next button");
//				}
//				Thread.sleep(1000); 
//				String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
//				System.out.println(data);
//				Thread.sleep(1000);
//				if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
//				{
				if(isElementPresent(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())))
				{	
					Thread.sleep(2000);
					safeJavaScriptClick(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
					System.out.println(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
					log("click on ANH");
					// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
					Thread.sleep(2000);
					break;
				}
				
					Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
					log("click on next button");
					Thread.sleep(3000);
				
					
//				}
			}
			SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),Inputdata[i][18].toString());
			log("Enter the Transaction ID:-"+Inputdata[i][18].toString());
			javascriptInput(Inputdata[i][159].toString(),getwebelement(xml.getlocator("//locators/PortingDate")));
			log("Porting Date is:-"+Inputdata[i][159].toString());
			implicitwait(5);
			Select(getwebelement(xml.getlocator("//locators/PortingWindow")), Inputdata[i][138].toString());
			log("porting winodw is:-"+Inputdata[i][138]);
			if(Inputdata[i][2].equals("DENMARK"))
			{
				SendKeys(getwebelement(xml.getlocator("//locators/firstnameportin")), Inputdata[i][82].toString().trim());
				implicitwait(5);
				SendKeys(getwebelement(xml.getlocator("//locators/lastnameportin")), Inputdata[i][83].toString().trim());
				implicitwait(5);
				SendKeys(getwebelement(xml.getlocator("//locators/phonenumber")), Inputdata[i][84].toString().trim());
				implicitwait(5);
				SendKeys(getwebelement(xml.getlocator("//locators/emailidportin")), Inputdata[i][85].toString().trim());
				
				//log("Enter the Transaction ID:-"+Inputdata[i][18].toString());
			}
			Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
			log("Click on the submit button");
			if(isElementPresent(xml.getlocator("//locators/ErrorUpdatePortinDate")))
			{
				RedLog("please check Transaction id, your Number may be incomplete ur incorrect ... please check the space also");
			}
			else
			{
				GreenLog("change Port-in date completed");
			}
		}
	}

	public void CancelPortINrequest(Object[][] Inputdata) throws Exception {
		for (int i = 0; i < Inputdata.length; i++) 
		{
			WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
			Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
			log("click on managing porting");
			Thread.sleep(1000);
			Clickon(getwebelement(xml.getlocator("//locators/CancelPortIn")));
			implicitwait(10);
			log("click on CancelPortIn request");
			String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
			int allData=Integer.parseInt(gett);
			//int allData = getwebelementscount(xml.getlocator("//locators/pagelimt"));//
			System.out.println(allData);
			for (int k = 0; k <= allData; k++) 
			{
				Thread.sleep(3000);
//				if (k != 0 && k % 10 == 0) 
//				{
//					Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
//					log("click on ANH and next button");
//				}
//				Thread.sleep(1000); 
//				String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
//				System.out.println(data);
//				Thread.sleep(1000);
//				if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
//				{
				if(isElementPresent(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())))
				{	
					Thread.sleep(2000);
					safeJavaScriptClick(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
					System.out.println(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
					log("click on ANH");
					// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
					Thread.sleep(2000);
					break;
				}
				
					Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
					log("click on next button");
					Thread.sleep(3000);
				
					
//				}
			}
			SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),Inputdata[i][18].toString());
			log("Enter the Transaction ID:-"+Inputdata[i][18].toString());
//			if(Inputdata[i][2].equals("DENMARK"))
//			{
////				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),Inputdata[i][18].toString());
////				log("Enter the Transaction ID:-"+Inputdata[i][18].toString());
//			}
			//statusQueryTransactionID
			if(Inputdata[i][2].equals("DENMARK"))
			{
				SendKeys(getwebelement(xml.getlocator("//locators/firstnameportin")), Inputdata[i][82].toString().trim());
				implicitwait(5);
				SendKeys(getwebelement(xml.getlocator("//locators/lastnameportin")), Inputdata[i][83].toString().trim());
				implicitwait(5);
				SendKeys(getwebelement(xml.getlocator("//locators/phonenumber")), Inputdata[i][84].toString().trim());
				implicitwait(5);
				SendKeys(getwebelement(xml.getlocator("//locators/emailidportin")), Inputdata[i][85].toString().trim());
				
				//log("Enter the Transaction ID:-"+Inputdata[i][18].toString());
			}
			Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
			log("Click on the Submit button");
			if(isElementPresent(xml.getlocator("//locators/errorcancelport-in")))
			{
				RedLog("please check Transaction id, your Number may be incomplete ur incorrect ... please check the space also");
			}
			else
			{
				GreenLog("Cancel Port-in completed");
				Thread.sleep(2000);
				String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/portinTransactionlink")));
				TransactionId.set(Transactionresult);
				log("Transaction id received");
				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequest")));
				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequest")));
				// =========================================================================
				log("click on the update part in request");
				waitandForElementDisplayed(xml.getlocator("//locators/statusQueryTransactionID"));
				log(TransactionId.get().toString().trim().toLowerCase());
				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),TransactionId.get().toString().trim().toLowerCase());
				Thread.sleep(3000);
				System.out.println(TransactionId.get());
				log("send the transaction ID: " + TransactionId.get());
				// =========================================================================
				Thread.sleep(10000);
				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
				Thread.sleep(3000);
				log("click on the search Button");
				String CurrentStatus1 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
				CurrentStatus.set(CurrentStatus1);
				log("CurrentStatus received");
				log("check the current status");
//				if (CurrentStatus.get().contains("Validation In Progress")) 
//				{
					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
					log("click on the checkbox");
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/ActionPortin")), Inputdata[i][16].toString());
					log("select the Accept/Reject");
					Thread.sleep(5000);
					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
					log("click on the go button");
//				}
//				else 
//				{
//					RedLog("CurrentStatus is incorrect");
//					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
//				}
				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
				log("click on the submit button");
				Thread.sleep(2000);
				AcceptJavaScriptMethod();
				log("submitted succesfully");
				Thread.sleep(10000);
//				boolean flag = false;
//				do 
//				{
//					flag = isElementPresent(xml.getlocator("//locators/verifystatus2"));
//					// ChildCurrentStat.set(ChildCurrentStatus);
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: ChildCurrentStatus is : " + ChildCurrentStat.get());
//					log("if child Current Status shown than go to the next step button otherwise click on the refresh button");
					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//				} 
//				while (!flag == true);
//				log("childcurrent status id successfully submited");
				Thread.sleep(5000);
			}
				
			
			
		}
	}

	public void TelephoneNuberHistory(Object[][] Inputdata) throws InterruptedException, DocumentException, IOException
	{
		for(int i=0;i<Inputdata.length;i++)
		{
			Clickon(getwebelement(xml.getlocator("//locators/ManageNumber")));
			log("Click on the Manage Nuber");
			Clickon(getwebelement(xml.getlocator("//locators/TelephoneNumberHistory")));
			log("Click on the Telephone Number History");
			SendKeys(getwebelement(xml.getlocator("//locators/TeleNumberHistory")),Inputdata[i][64].toString());
			log("Enter the Telephone Number:-"+Inputdata[i][64].toString());
			Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
			log("Click on the submit button in the telephone number history");
			implicitwait(5);
			if(isElementPresent(xml.getlocator("//locators/errorinTelephone1")))
					{
						RedLog("please check Telephone Number, your Number may be incomplete ur incorrect ... please check the space also");
					}
			else
			{
				GreenLog("Telephone Numbe history completed.");
			}
		}
	}
	//20/05/2020
	//again
	//again2
	public void NumberQuiryUsingNumberStatus(Object[][] Inputdata)throws Exception 
	{
		for (int i = 0; i < Inputdata.length; i++) 
		{
			Clickon(getwebelement(xml.getlocator("//locators/ManageNumber")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Manage Number");
			Clickon(getwebelement(xml.getlocator("//locators/NumberEnquiry")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Number Enquiry");
			Select(getwebelement(xml.getlocator("//locators/ResellerName")), Inputdata[i][1].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Reseller name: " + Inputdata[i][1].toString());
			Select(getwebelement(xml.getlocator("//locators/Country")), Inputdata[i][2].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Country: " + Inputdata[i][2].toString());
			implicitwait(20);
			waitandForElementDisplayed(xml.getlocator("//locators/ServiceProfile"));
			Select(getwebelement(xml.getlocator("//locators/ServiceProfile")), Inputdata[i][3].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Service Profile: " + Inputdata[i][3].toString());
			Thread.sleep(3000);
			Select(getwebelement(xml.getlocator("//locators/SearchCriteria")), "Number Status");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search Criteria as Number Status");
			switch (Inputdata[i][4].toString()) 
			{
			case "Reserved": // Reserved to Active Scenario
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), Inputdata[i][4].toString());
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(3000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with search creteria!!");
					//DownloadExcel("NumberEnquiryReport");
					Thread.sleep(2000);
					if (Inputdata[i][20].toString().contains("WithAddress")) 
					{
						if (Inputdata[i][19].toString().contains("Activated")) 
						{
							implicitwait(10);
							Thread.sleep(2000);
							
							String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
							int allData=Integer.parseInt(gett);
							int len=0;
							for (int k = 0; k < allData; k++) 
							{
							
								Thread.sleep(1000); 
								if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
								{
									len=len+1;
									String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
									if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
									{
										Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
										log("Click on the radio button");
										Thread.sleep(2000);
										if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
										{
										WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
										Thread.sleep(2000);
										Select(tempdriver, Inputdata[i][16].toString());
										}
										else
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
										Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
										break;
									}
									else 
									{
										Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
										log("click on next button");
										//break;
									}
								}
								else
								{
									log("Telephone number is inocrrect next try");
							
									Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
									log("click on next button");
									
								}
							}
							Thread.sleep(2000);
							if(len==0) {
								Assert.fail("Number is incorrect");
							}
							

							Thread.sleep(1000);
							ActivateSwitchCountriesName(Inputdata);
							Thread.sleep(5000);
							waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
//							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdwithoutAddress")));
//							TransactionId.set(Transactionresult);
//							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
//							Thread.sleep(3000);
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
							log("Transaction id is:-"+Transactionresult);
							implicitwait(20);
							StatuQuerybyTransitionId(Transactionresult);
							log("Successfully Updated status");
							if(Inputdata[i][21].toString().equals("Port-out")) 
							{
								portoutforreserverd(Inputdata);
							}	
						} 
						else 
						{
							log("No action performed!!");
							log("In Sheet, Perform Action column does not have data to perform action with Activated!");
						}
					} 
					else if (Inputdata[i][20].toString().contains("WithoutAddress")) 
					{
						log("Start");
						if(Inputdata[i][2].toString().equals("Italy")) 
						{
							RedLog("For Italy without address scenario is not possible");
						}
//						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
//						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
//						Select(getwebelement(xml.getlocator("//locators/UserAction")), "Activate");
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as Activate");
//						Clickon(getwebelement(xml.getlocator("//locators/UserGo")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(1000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
									log("click on next button");
									//break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						implicitwait(10);
						Thread.sleep(2000);

						log("Address is not mandatory for this transaction ID");
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						Thread.sleep(4000);
						waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(2000);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
						log("Successfully Updated status");
					}
					//23-04-2020 cancel reservaion code by number status
					else if(Inputdata[i][19].toString().contains("Cancel Reservation"))
					{
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(1000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
									log("click on next button");
									//break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						implicitwait(10);
						Thread.sleep(2000);
						
						Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//						Thread.sleep(4000);
//						waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
//						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
//						TransactionId.set(Transactionresult);
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
//						Thread.sleep(2000);
						Thread.sleep(5000);
						waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(3000);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
						log("Successfully Updated status");
						//submitagain
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Free": // Free to Reserve Scenario
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Free");
				implicitwait(20);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Free");
				// --------------------
				// for test
//				if (Inputdata[i][24].toString().equals("Geographical Numbers")) 
//				{
				//if ((Inputdata[i][2].toString().equals("DENMARK"))
//							|| (Inputdata[i][2].toString().equals("PORTUGAL"))) 
//					{
//						log("Don't need to click on geographical and nongeographical radio Button");
//					}
//					else 
//					{
//						WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton"));
//						Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton")));
//						implicitwait(10);
//						if(Inputdata[i][24].toString().equals("Search by Local Area"))
//						{		
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search by Local Area");
//							WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton7"));
//							Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton7")));
////							Select(getwebelement(xml.getlocator("//locators/SelectAreaName")), Inputdata[i][5].toString());
////							implicitwait(20);
////							Select(getwebelement(xml.getlocator("//locators/blockSize")), Inputdata[i][22].toString());
////							implicitwait(5);
////							ExtentTestManager.getTest().log(LogStatus.PASS,
////									" Step: Select the Block Size: " + Inputdata[i][22].toString());
////							implicitwait(5);
////							
////							waitandForElementDisplayed(xml.getlocator("//locators/Quantity"));
////							Select(getwebelement(xml.getlocator("//locators/Quantity")), Inputdata[i][23].toString());
////							implicitwait(5);
////							ExtentTestManager.getTest().log(LogStatus.PASS,
////									" Select the Quantity Size: " + Inputdata[i][23].toString());
//							
//						}
//					}
//					Thread.sleep(2000);
////areaCodINFgermany
//					if(Inputdata[i][2].toString().equals("GERMANY")) 
//					{
//					Select(getwebelement(xml.getlocator("//locators/areaCodINFgermany")), Inputdata[i][5].toString());
//					implicitwait(20);
//					Select(getwebelement(xml.getlocator("//locators/blockSize")), Inputdata[i][22].toString());
//					implicitwait(5);
//					ExtentTestManager.getTest().log(LogStatus.PASS,
//							" Step: Select the Block Size: " + Inputdata[i][22].toString());
//					}
//					else 
//					{
//						Select(getwebelement(xml.getlocator("//locators/SelectAreaName")), Inputdata[i][5].toString());
//						implicitwait(20);
//						Select(getwebelement(xml.getlocator("//locators/blockSize")), Inputdata[i][22].toString());
//						implicitwait(5);
//						ExtentTestManager.getTest().log(LogStatus.PASS,
//								" Step: Select the Block Size: " + Inputdata[i][22].toString());
//						implicitwait(5);
//						
//						waitandForElementDisplayed(xml.getlocator("//locators/Quantity"));
//						Select(getwebelement(xml.getlocator("//locators/Quantity")), Inputdata[i][23].toString());
//						implicitwait(5);
//						ExtentTestManager.getTest().log(LogStatus.PASS,
//								" Select the Quantity Size: " + Inputdata[i][23].toString());
//					}
//				}
				if (Inputdata[i][24].toString().equals("Geographical Numbers")) 
				{
						WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton"));
						Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton")));
						implicitwait(10);
						////input[@id='geoNum']
						
						if ((Inputdata[i][2].toString().equals("NETHERLANDS"))||(Inputdata[i][2].toString().equals("IRELAND"))||(Inputdata[i][2].toString().equals("ITALY"))||(Inputdata[i][2].toString().equals("SWITZERLAND"))||(Inputdata[i][2].toString().equals("SWEDEN"))||(Inputdata[i][2].toString().equals("BELGIUM"))||(Inputdata[i][2].toString().equals("AUSTRIA"))) 
						{
							log("no other radio button avilable");
							Select(getwebelement(xml.getlocator("//locators/SelectAreaName")), Inputdata[i][5].toString());
							log("SelectAreaName is:-"+Inputdata[i][5].toString());
							implicitwait(20);	
						}
						else if((Inputdata[i][2].toString().equals("GERMANY")))
						{
							Select(getwebelement(xml.getlocator("//locators/areaCodINFgermany")), Inputdata[i][5].toString());
							log("SelectAreaName is:-"+Inputdata[i][5].toString());
							implicitwait(20);	
						}
						
						else if(Inputdata[i][100].toString().equals("Search by Local Area"))
						{		
							if(Inputdata[i][2].toString().equals("SPAIN"))
							{
								implicitwait(2);
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search by Local Area for spain");
								
								//WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton9"));
								//Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton9")));
								//log("Click on the search by local area radio button");
								Thread.sleep(3000);
								Select(getwebelement(xml.getlocator("//locators/areacodeNew")), Inputdata[i][5].toString());
								log("SelectAreaName is:-"+Inputdata[i][5].toString());
								implicitwait(20);
							}
							else if(Inputdata[i][2].toString().equals("FRANCE"))
							{
								Thread.sleep(3000);
								Select(getwebelement(xml.getlocator("//locators/areacodeNew")), Inputdata[i][5].toString());
								log("SelectAreaName is:-"+Inputdata[i][5].toString());
								implicitwait(20);
							}
							else
							{
								implicitwait(2);
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search by Local Area");
								WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton7"));
								Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton7")));
								Select(getwebelement(xml.getlocator("//locators/SelectAreaName")), Inputdata[i][5].toString());
								log("SelectAreaName is:-"+Inputdata[i][5].toString());
								implicitwait(20);
							}
						}
						else if(Inputdata[i][100].toString().equals("Search by Address"))
						{
							if(Inputdata[i][2].toString().equals("SPAIN"))
							{
								implicitwait(2);
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search by Address for spain");
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton10"));
								Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton10")));
								Select(getwebelement(xml.getlocator("//locators/areacodeNew")), Inputdata[i][5].toString());
								log("SelectAreaName is:-"+Inputdata[i][5].toString());
								implicitwait(20);
							}
							else
							{
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search by Address");
								WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton8"));
								Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton8")));
								if(Inputdata[i][2].toString().equals("FRANCE"))
								{
									Select(getwebelement(xml.getlocator("//locators/numberStatusDepartment")), Inputdata[i][92].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the numberStatusDepartment: " + Inputdata[i][92].toString());
									Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][11].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the City/Town: " + Inputdata[i][11].toString());
									Select(getwebelement(xml.getlocator("//locators/blockSize2")), Inputdata[i][22].toString());
									implicitwait(5);
									ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Block Size: " + Inputdata[i][22].toString());
									Select(getwebelement(xml.getlocator("//locators/Quantity2")), Inputdata[i][23].toString());
									implicitwait(5);
									ExtentTestManager.getTest().log(LogStatus.PASS," Select the Quantity Size: " + Inputdata[i][23].toString());
								}
								else
								{
									Select(getwebelement(xml.getlocator("//locators/numberStatuspostcode")), Inputdata[i][12].toString());
									log("post code is:-"+Inputdata[i][12].toString());
									implicitwait(20);
								}
							}
					}
						else
						{
							log("Please select the Search by Local Aread code or Search By address");
							Thread.sleep(3000);
							if((isElementPresent("//locators/numberRadioButton7"))||(isElementPresent("//locators/numberRadioButton8"))||(isElementPresent("//locators/numberRadioButton9"))||(isElementPresent("//locators/numberRadioButton10")))
							{
								RedLog("please choose the  Search by Local Aread code or Search By address radio button ");
							}
							Thread.sleep(3000);
						}
					Select(getwebelement(xml.getlocator("//locators/blockSize")), Inputdata[i][22].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Block Size: " + Inputdata[i][22].toString());
					implicitwait(5);
					if(Inputdata[i][2].toString().equals("GERMANY"))
					{
						log("Quantity is not present");
					}
					else
					{
					waitandForElementDisplayed(xml.getlocator("//locators/Quantity"));
					Select(getwebelement(xml.getlocator("//locators/Quantity")), Inputdata[i][23].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Select the Quantity Size: " + Inputdata[i][23].toString());
					}
				}
				else if (Inputdata[i][24].toString().equals("Location Independent Numbers")) 
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton2"));
					Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton2")));
					Select(getwebelement(xml.getlocator("//locators/blockSize2")), Inputdata[i][22].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Block Size: " + Inputdata[i][22].toString());
					Select(getwebelement(xml.getlocator("//locators/Quantity2")), Inputdata[i][23].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Select the Quantity Size: " + Inputdata[i][23].toString());
				}
				else if (Inputdata[i][24].toString().equals("UK WIDE (Any Services)")) 
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton3"));
					Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton3")));
					Select(getwebelement(xml.getlocator("//locators/blockSize2")), Inputdata[i][22].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Block Size: " + Inputdata[i][22].toString());
					Select(getwebelement(xml.getlocator("//locators/Quantity2")), Inputdata[i][23].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Select the Quantity Size: " + Inputdata[i][23].toString());
				}

				else if (Inputdata[i][24].toString().equals("UK WIDE (Public Services)")) 
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/NumberRadioButton4"));
					Clickon(getwebelement(xml.getlocator("//locators/NumberRadioButton4")));
					Select(getwebelement(xml.getlocator("//locators/blockSize2")), Inputdata[i][22].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Block Size: " + Inputdata[i][22].toString());
					Select(getwebelement(xml.getlocator("//locators/Quantity2")), Inputdata[i][23].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Select the Quantity Size: " + Inputdata[i][23].toString());
				}
				else if(Inputdata[i][2].toString().equals("DENMARK")||(Inputdata[i][2].toString().equals("PORTUGAL")))
				{
					Select(getwebelement(xml.getlocator("//locators/SelectAreaName")), Inputdata[i][5].toString());
					log("SelectAreaName is:-"+Inputdata[i][5].toString());
					Select(getwebelement(xml.getlocator("//locators/blockSize")), Inputdata[i][22].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Block Size: " + Inputdata[i][22].toString());
					implicitwait(5);
					waitandForElementDisplayed(xml.getlocator("//locators/Quantity"));
					Select(getwebelement(xml.getlocator("//locators/Quantity")), Inputdata[i][23].toString());
					implicitwait(5);
					ExtentTestManager.getTest().log(LogStatus.PASS," Select the Quantity Size: " + Inputdata[i][23].toString());
				}

				else 
				{
					RedLog("Geographical or Nongeographical button is missing");
				}
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Geographical or NonGeographical radio button");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				validate();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(4000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("Reserve"))
					{
						if (isElementPresent(xml.getlocator("//locators/SelectNumber").replace("Number",Inputdata[i][64].toString()))) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/SelectNumberCheck").replace("Number",Inputdata[i][64].toString())));
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on the Number Range Checkbox");
							Select(getwebelement(xml.getlocator("//locators/Action")), "Reserve");
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Action as Reserve");
							Clickon(getwebelement(xml.getlocator("//locators/GoButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
							Thread.sleep(4000);
							SendKeys(getwebelement(xml.getlocator("//locators/CustomerReference")),Inputdata[i][6].toString());
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter Customer Reference number: " + Inputdata[i][6].toString());
							WaitforElementtobeclickable(xml.getlocator("//locators/submitagain"));
							Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
							String TransactionIdres = Gettext(getwebelement(xml.getlocator("//locators/TransactionId")));
							TransactionId.set(TransactionIdres);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: The Transaction Id generated is : " + TransactionIdres);
							Thread.sleep(1000);
							// Status Query ************************************************************
							StatuQuerybyTransitionId(TransactionIdres);
							GreenLog("Numbwer Query status id update Successfully from Free to Reserve");
						} 
						else 
						{
							WaitforElementtobeclickable(xml.getlocator("//locators/NumberRangeCheckbox"));
							Clickon(getwebelement(xml.getlocator("//locators/NumberRangeCheckbox")));
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on the Number Range Checkbox");
							Select(getwebelement(xml.getlocator("//locators/Action")), "Reserve");
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Action as Reserve");
							Clickon(getwebelement(xml.getlocator("//locators/GoButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
							Thread.sleep(4000);
							SendKeys(getwebelement(xml.getlocator("//locators/CustomerReference")),Inputdata[i][6].toString());
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter Customer Reference number: " + Inputdata[i][6].toString());
							WaitforElementtobeclickable(xml.getlocator("//locators/Submit"));
							Clickon(getwebelement(xml.getlocator("//locators/Submit")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
							String TransactionIdres = Gettext(getwebelement(xml.getlocator("//locators/TransactionId")));
							TransactionId.set(TransactionIdres);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: The Transaction Id generated is : " + TransactionIdres);
							Thread.sleep(1000);
							// Status Query ************************************************************
							StatuQuerybyTransitionId(TransactionIdres);
							GreenLog("Numbwer Query status id update Successfully from Free to Reserve");
						}
					} 
					else if (Inputdata[i][20].toString().contains("WithAddress")) 
					{	
					if (Inputdata[i][19].toString().contains("Activated"))
						{	
							
							WaitforElementtobeclickable((xml.getlocator("//locators/SelectNumberCheck").replace("Number",Inputdata[i][64].toString())));
//							Clickon(getwebelement(xml.getlocator("//locators/NumberRangeCheckbox")));
							Clickon(getwebelement(xml.getlocator("//locators/SelectNumberCheck").replace("Number",Inputdata[i][64].toString())));
//						WaitforElementtobeclickable(xml.getlocator("//locators/NumberRangeCheckbox"));
//						Clickon(getwebelement(xml.getlocator("//locators/NumberRangeCheckbox")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Number Range Checkbox");
						
						
						
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on the Number Range Checkbox");
							Select(getwebelement(xml.getlocator("//locators/Action")), "Activate");
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Action as Activate");
							Clickon(getwebelement(xml.getlocator("//locators/GoButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
							Thread.sleep(2000);
							ActivateSwitchCountriesName(Inputdata);
							Thread.sleep(2000);
//							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
//							TransactionId.set(Transactionresult);
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Transaction Id is : " + TransactionId.get());
//							System.out.println(Transactionresult);
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdwithoutAddress")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
							Thread.sleep(3000);
							implicitwait(20);
							StatuQuerybyTransitionId(Transactionresult);
							if (Inputdata[i][21].toString().contains("Port-out")) 
							{
								portoutforreserverd(Inputdata);
								//rest commentted code save in file 1.txt
							}
							else 
							{
								log("done");
								System.out.println("DONE");
							}
						}
						log("Free to Activated with Address completed.");
					}
				 
				else if (Inputdata[i][20].toString().contains("WithoutAddress"))
				{
					log("Start");
					Thread.sleep(2000);
					if (isElementPresent(xml.getlocator("//locators/Griddata")))
					{
						GreenLog("Data are displaying with serach creteria!!");
						WaitforElementtobeclickable((xml.getlocator("//locators/SelectNumberCheck").replace("Number",Inputdata[i][64].toString())));
//						Clickon(getwebelement(xml.getlocator("//locators/NumberRangeCheckbox")));
						Clickon(getwebelement(xml.getlocator("//locators/SelectNumberCheck").replace("Number",Inputdata[i][64].toString())));
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on the Number Range Checkbox");
//						WaitforElementtobeclickable(xml.getlocator("//locators/NumberRangeCheckbox"));
//						Clickon(getwebelement(xml.getlocator("//locators/NumberRangeCheckbox")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Number Range Checkbox");
						
						
						Select(getwebelement(xml.getlocator("//locators/Action")), "Activate");
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Action as Activate");
						Clickon(getwebelement(xml.getlocator("//locators/GoButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						Thread.sleep(5000);
						waitandForElementDisplayed(xml.getlocator("//locators/TransactionIdwithoutAddress"));
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdwithoutAddress")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(3000);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
						log("Free to Activated without info Scenario competed.");
					} 
					else if (Inputdata[i][19].toString() == null)
					{
						RedLog("In Sheet, Perform Action column does not have data to perform action with Free Number Query!");
					} 
					else 
					{
						RedLog("In Sheet, Perform Action column  having data to perform action but not correct spelled with Free Number Query!");
					}
				} 
				}
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString()+ " And Block size " + Inputdata[i][22].toString() + " And Quantity "+ Inputdata[i][23].toString());
				}
			
				break;
			}
			case "Activated": 
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Activated");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Activated");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(5000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("UpdateAddress")) 
					{

						implicitwait(10);
						Thread.sleep(5000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(2000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								//Thread.sleep(2000);
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								Thread.sleep(2000);
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Thread.sleep(2000);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									Thread.sleep(2000);
//									//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
									log("click on next button");
									//break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						
						
						
						Thread.sleep(3000);
						switch(Inputdata[i][2].toString())
						{
							case "UNITED KINGDOM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "AUSTRIA":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								Clear(getwebelement(xml.getlocator("//locators/CustomerName")));
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewPostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "BELGIUM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
//									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
//									implicitwait(5);	
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
								}
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/AddressExtension")), Inputdata[i][].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the AddressExtension: " + Inputdata[i][].toString());
								//AddressExtension
								Select(getwebelement(xml.getlocator("//locators/DirectroyListingOptions")),Inputdata[i][109].toString());
								log("Selct the Directroy Listing Options:-"+Inputdata[i][109]);
								WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
								Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try 
									{
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} 
									catch (Exception e) 
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
								Thread.sleep(3000);
//								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "DENMARK":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
//								if(Inputdata.toString().equals("RFS"))
//								{
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//									implicitwait(5);
//									//
//								}
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								//emerSerUpdBusinessName
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "FRANCE":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/departmentaddingnumber")),Inputdata[i][92].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								implicitwait(5);
								log("City is:-"+Inputdata[i][11].toString());
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								log("postcode is:-"+Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "GERMANY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								}	
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//	locators/PostCode")), Inputdata[i][115].toString());								
								implicitwait(5);
								WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
								Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try 
									{
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} 
									catch (Exception e) 
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
								Thread.sleep(3000);
//								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
//								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "IRELAND":
							{	
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][110].toString().trim());
									implicitwait(5);
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "ITALY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/freeacprovience")),Inputdata[i][25].toString());
								implicitwait(5);
								Thread.sleep(3000);
								Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "NETHERLANDS":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "PORTUGAL":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerNmaeupdate")), Inputdata[i][110].toString().trim());
									SendKeys(getwebelement(xml.getlocator("//locators/bi")), Inputdata[i][89].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/nif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "SPAIN":
							{
//								if(Inputdata[i][81].toString().equals("RFS"))
//								{
//								SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")),Inputdata[i][104].toString().trim());
//								SendKeys(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								}
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreetTypeUpdate")),Inputdata[i][104].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/provinceUpdate")), Inputdata[i][25].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								
								
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								if(Inputdata[i][105].toString().equals("List Number Options "))
								{
									Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
									if(Inputdata[i][106].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
									}
									if(Inputdata[i][107].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
									}
								}
								else
								{
									Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
								}
								
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								//submitagain
								break;
							}
							case "SWEDEN":
							{
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationname")), Inputdata[i][131].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Company Registration nnumber: " + Inputdata[i][131].toString());
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									//company name =customer name in sweden Address update scenario so inputdata column is 108.
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][108].toString().trim());
									implicitwait(5);

								}
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][103].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString().trim());								
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
								implicitwait(5);
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								//Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								break;
							}
							case "SWITZERLAND":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/munciapalityactivate")), Inputdata[i][86].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							default:
							{
								log("please select valid country");
								break;
							}
						}
//						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdwithoutAddress")));
//						TransactionId.set(Transactionresult);
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
//						Thread.sleep(3000);
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						log("Transaction id is:-"+Transactionresult);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
					} 
					else if (Inputdata[i][19].toString().contains("Deactivate")) 
					{
						System.out.println("yesok");
//						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(2000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								Thread.sleep(2000);
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		

							WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
							Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
							Thread.sleep(3000);
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						
						StatuQuerybyTransitionId(TransactionId.get());
					} 
					else 
					{
						log("Search Scenario with activate scenario has been successfull performed");
					}
				}
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Quarantined": 
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Quarantined");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Quarantined");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(5000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					//DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("Reactivate")) 
					{
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(1000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						Thread.sleep(1000);
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						Thread.sleep(3000);
						try 
						{
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							StatuQuerybyTransitionId(Transactionresult);
						}
						catch (StaleElementReferenceException e) 
						{
						}
						implicitwait(20);
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}

				break;
			}
			case "Port In(Allocated)": 
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Port In(Allocated)");
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Number Status as Port In(Allocated)");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					Thread.sleep(5000);
					String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdLink")));
					TransactionId.set(Transactionresult);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
					Thread.sleep(1000);
					implicitwait(20);
					StatuQuerybyTransitionId(Transactionresult);
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "PortIn(Activated)": 
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Port In(Activated)");
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Number Status as Port In(Activated)");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(5000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("UpdateAddress")) 
					{
//						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
//						Select(getwebelement(xml.getlocator("//locators/UserAction2")), Inputdata[i][16].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the User Action: " + Inputdata[i][16].toString());
//						Clickon(getwebelement(xml.getlocator("//locators/UserGo2")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
//						Thread.sleep(5000);
//						if (Inputdata[i][2].toString().equals("AUSTRIA")) 
//						{
//							SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
//							log("customer name is required: " + Inputdata[i][7].toString());
//						}
//						else 
//						{
//							SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
//							log("customer name is required: " + Inputdata[i][7].toString());
//						}
//						SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][10].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][10].toString());
//						if(Inputdata[i][2].toString().equals("SPAIN")) 
//						{
//						Select(getwebelement(xml.getlocator("//locators/freeacprovience")), Inputdata[i][25].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
//						Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][11].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//						}
//						else 
//						{
//							SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][11].toString());
//							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
//						}
//						SendKeys(getwebelement(xml.getlocator("//locators/NewPostCode")), Inputdata[i][12].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
//						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(1000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									Thread.sleep(2000);
									//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
										Thread.sleep(2000);
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
									
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBtn3").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
									log("click on next button");
			
									//break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						Thread.sleep(3000);
						switch(Inputdata[i][2].toString())
						{
							case "UNITED KINGDOM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "AUSTRIA":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								Clear(getwebelement(xml.getlocator("//locators/CustomerName")));
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewPostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "BELGIUM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
									implicitwait(5);	
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
								}
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "DENMARK":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
//								if(Inputdata.toString().equals("RFS"))
//								{
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//									implicitwait(5);
//									//
//								}
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								//emerSerUpdBusinessName
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "FRANCE":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/departmentaddingnumber")),Inputdata[i][92].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								implicitwait(5);
								log("City is:-"+Inputdata[i][11].toString());
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								log("postcode is:-"+Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "GERMANY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								}	
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());								
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "IRELAND":
							{	
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][110].toString().trim());
									implicitwait(5);
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "ITALY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/freeacprovience")),Inputdata[i][25].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "NETHERLANDS":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "PORTUGAL":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerNmaeupdate")), Inputdata[i][110].toString().trim());
									SendKeys(getwebelement(xml.getlocator("//locators/bi")), Inputdata[i][89].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/nif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "SPAIN":
							{
//								if(Inputdata[i][81].toString().equals("RFS"))
//								{
//								SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")),Inputdata[i][104].toString().trim());
//								SendKeys(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								}
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreetTypeUpdate")),Inputdata[i][104].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/provinceUpdate")), Inputdata[i][25].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								
								
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								if(Inputdata[i][105].toString().equals("List Number Options "))
								{
									Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
									if(Inputdata[i][106].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
									}
									if(Inputdata[i][107].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
									}
								}
								else
								{
									Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
								}
								
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								//submitagain
								break;
							}
							case "SWEDEN":
							{
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
									if(Inputdata[i][2].toString().equals("SPAIN"))
									{
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else 
									{
										SendKeys(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationname")), Inputdata[i][131].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Company Registration nnumber: " + Inputdata[i][131].toString());
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									//company name =customer name in sweden Address update scenario so inputdata column is 108.
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][108].toString().trim());
									implicitwait(5);

								}
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][103].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString().trim());								
								implicitwait(5);
								
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								break;
							}
							case "SWITZERLAND":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/munciapalityactivate")), Inputdata[i][86].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							default:
							{
								log("please select valid country");
								break;
							}
						}

						Thread.sleep(1000);
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(1000);
						StatuQuerybyTransitionId(Transactionresult);
					}
					else if (Inputdata[i][19].toString().contains("Deactivate")) 
					{
//						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
//						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Radio button");
//						Select(getwebelement(xml.getlocator("//locators/UserAction2")), "Deactivate");
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select user action as Deactivate");
//						WaitforElementtobeclickable(xml.getlocator("//locators/UserGo2"));
//						Clickon(getwebelement(xml.getlocator("//locators/UserGo2")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
						//Thread.sleep(2000);
//						do
//						{
//							Thread.sleep(2000); 
//							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
//							{
//								Thread.sleep(2000);
//								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
//								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
//								{
//									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
//									log("Click on the radio button");
//									Thread.sleep(2000);
//									//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
//									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
//									{
//									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
//									Thread.sleep(2000);
//									Select(tempdriver, Inputdata[i][16].toString());
//									}
//									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
//									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
//									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
//									break;
//								}
//								else 
//								{
//									if(isElementPresent(xml.getlocator("//locators/NEXT")))
//									{
//										log("number inocrrect");
//									}
//								else
//								{
//								log("Telephone number is inocrrect next try");
//								break;
//								}
//								}
//							}
//							else
//							{	
//								Thread.sleep(2000);
//								if(isElementPresent(xml.getlocator("//locators/NEXT")))
//									{
//										Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
//										log("click on next button");
//									}
//								else
//								{
//								log("Telephone number is inocrrect next try");
//								break;
//								}
//							}						
//						}
//						while(isElementPresent(xml.getlocator("//locators/NEXT")));
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(2000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								System.out.println("yupgotit");
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								Thread.sleep(2000);
								System.out.println("yupgotit");
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Thread.sleep(2000);
									System.out.println("yupgotit");
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									Thread.sleep(2000);
									//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
									if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
										Thread.sleep(2000);
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
									
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Thread.sleep(3000);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBtn3").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									break;
									
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						if(Inputdata[i][81].toString().equals("RFS"))
						{
							if(Inputdata[i][2].toString().equals("BELGIUM")||Inputdata[i][2].toString().contentEquals("AUSTRIA"))
							{
								WaitforElementtobeclickable(xml.getlocator("//locators/subbuttond"));
								Clickon(getwebelement(xml.getlocator("//locators/subbuttond")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
								//subbuttond
							}
						}
						else
						{
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						}
						Thread.sleep(3000);
						String Transactionresult = null;
						try 
						{
							Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						} 
						catch (StaleElementReferenceException e) 
						{
							implicitwait(20);
						}
						StatuQuerybyTransitionId(Transactionresult);
					} 
					else
					{
						log("Search Scenario with port-in activated has been successfully performed");
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Port Out": 
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Port Out");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Port Out");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Returned": 
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Returned");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Returned");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(1000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					try 
					{
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdLink")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Transaction Id is : " + TransactionId.get());
						implicitwait(20);
						// Status Query ************************************************************
						StatuQuerybyTransitionId(Transactionresult);
					} 
					catch (StaleElementReferenceException e) 
					{
					}
				}
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Port In(Quarantined)":
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Port In(Quarantined)");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Port In(Quarantined)");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
	//				DownloadExcel("NumberEnquiryReport");
					implicitwait(10);
					Thread.sleep(2000);
					String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
					int allData=Integer.parseInt(gett);
					int len=0;
					for (int k = 0; k < allData; k++) 
					{
					
						Thread.sleep(1000); 
						if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
						{
							len=len+1;
							String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
							if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
							{
								Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
								log("Click on the radio button");
								//Select(getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
								if(isElementPresent(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString())))
								{
								WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserAction2").replace("Telephone Number Start", Inputdata[i][64].toString()));
								Thread.sleep(2000);
								Select(tempdriver, Inputdata[i][16].toString());
								}
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
								Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN2").replace("Telephone Number Start", Inputdata[i][64].toString())));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
								break;
							}
							else 
							{
								log("Correct Telephone Number but Status is inocrrect please check the Status");
								break;
							}
						}
						else
						{
							log("Telephone number is inocrrect next try");
					
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on next button");
							
						}
					}
					Thread.sleep(2000);
					if(len==0) {
						Assert.fail("Number is incorrect");
					}
	
					Thread.sleep(2000);
					Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
					log("Click on the submit button in the port in quarntined page");
					Thread.sleep(4000);
					waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
					String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
					TransactionId.set(Transactionresult);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
					Thread.sleep(2000);
					} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "All": 
			{
				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "All");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as All");
				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(1000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					DownloadExcel("NumberEnquiryReport");
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			default: 
			{
				RedLog("In Excel data, It seems like Number Status data is misspelled or not correct!! Data is"+ Inputdata[i][4].toString());
			}
			}
			GreenLog("/******************************* current Step  has been finished * ******************************************");
		}

	}
	public void casesforstatusandrange(Object[][] Inputdata) throws InterruptedException, DocumentException, IOException
	{
		for(int i=0;i<Inputdata.length;i++)
		{
			switch (Inputdata[i][4].toString()) 
			{
			//same put on the NumberQuriy Using transaction Id
			case "Reserved": // Reserved to Active Scenario
			{
				Thread.sleep(3000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with search creteria!!");
					DownloadExcel("NumberEnquiryReport");
					implicitwait(10);
					Thread.sleep(2000);
					String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
					int allData=Integer.parseInt(gett);
					int len=0;
					for (int k = 0; k < allData; k++) 
					{
					
						Thread.sleep(1000); 
						
						if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
						{
							len=len+1;
							String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
							if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
							{
								Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
								log("Click on the radio button");
								//DesiredUserACtion
								Thread.sleep(2000);
								if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
								{
								WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
								Thread.sleep(2000);
								Select(tempdriver, Inputdata[i][16].toString());
								}
								
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
								Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
								break;
							}
							else 
							{
								log("Correct Telephone Number but Status is inocrrect please check the Status");
								break;
							}
						}
						else
						{
							log("Telephone number is inocrrect next try");
					
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on next button");
							
						}
					}
					Thread.sleep(2000);
					if(len==0) {
						Assert.fail("Number is incorrect");
					}
	
					Thread.sleep(2000);
					if (Inputdata[i][20].toString().contains("WithAddress")) 
					{
						if (Inputdata[i][19].toString().contains("Activated")) 
						{
							ActivateSwitchCountriesName(Inputdata);
							Thread.sleep(5000);
							waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());					
							StatuQuerybyTransitionId(Transactionresult);
							Thread.sleep(3000);
						} 
						else 
						{
							log("No action performed!!");
							log("In Sheet, Perform Action column does not have data to perform action with Activated!");
						}
					} 
					else if (Inputdata[i][20].toString().contains("WithoutAddress")) 
					{
						log("Start");
						if(Inputdata[i][2].toString().equals("Italy")) 
						{
							RedLog("For Italy without address scenario is not possible");
						}
						log("Address is not mandatory for this transaction ID");
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						Thread.sleep(4000);
						waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(2000);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
						log("Successfully Updated status");
					}
					else if(Inputdata[i][19].toString().contains("Cancel Reservation"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
						//submitagain
						Thread.sleep(4000);
						waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(2000);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
						log("Successfully Updated status");
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Activated": //same put on the Numberstatus Using Transaction Id
			{
				Thread.sleep(3000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("UpdateAddress")) 
					{
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(1000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									//DesiredUserACtion
									Thread.sleep(2000);
									if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
									log("click on next button");
			
									//break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						Thread.sleep(3000);
						switch(Inputdata[i][2].toString())
						{
							case "UNITED KINGDOM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "AUSTRIA":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								Clear(getwebelement(xml.getlocator("//locators/CustomerName")));
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewPostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "BELGIUM":
							{
//								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
//								{
//								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
//								log("Service Type is:"+Inputdata[i][54].toString());
//								}
//								
//								if(Inputdata[i][86].toString().equals("Residential"))
//								{
//									implicitwait(5);
//									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
//									implicitwait(5);
//									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
//									implicitwait(5);	
//								}
//								else 
//								{
//									implicitwait(5);
//									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
//								}
//								implicitwait(5);
//								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								break;
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
//									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
//									implicitwait(5);	
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
								}
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/AddressExtension")), Inputdata[i][].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the AddressExtension: " + Inputdata[i][].toString());
								//AddressExtension
								Select(getwebelement(xml.getlocator("//locators/DirectroyListingOptions")),Inputdata[i][109].toString());
								log("Selct the Directroy Listing Options:-"+Inputdata[i][109]);
								WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
								Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try 
									{
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} 
									catch (Exception e) 
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
								Thread.sleep(3000);
//								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "DENMARK":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
//								if(Inputdata.toString().equals("RFS"))
//								{
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//									implicitwait(5);
//									//
//								}
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								//emerSerUpdBusinessName
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "FRANCE":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Thread.sleep(2000);
								Select(getwebelement(xml.getlocator("//locators/departmentaddingnumber")),Inputdata[i][92].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								implicitwait(5);
								log("City is:-"+Inputdata[i][11].toString());
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								log("postcode is:-"+Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "GERMANY":
							{
//								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
//								{
//								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
//								log("Service Type is:"+Inputdata[i][54].toString());
//								}
//								if(Inputdata[i][86].toString().equals("Residential"))
//								{
//									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
//									implicitwait(5);
//									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
//									implicitwait(5);
//								}
//								else 
//								{
//									implicitwait(5);
//									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//								}	
//								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());								
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								break;
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								}	
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//	locators/PostCode")), Inputdata[i][115].toString());								
								implicitwait(5);
								WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
								Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try 
									{
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} 
									catch (Exception e) 
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
								Thread.sleep(3000);
//								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
//								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "IRELAND":
							{	
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][110].toString().trim());
									implicitwait(5);
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "ITALY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/freeacprovience")),Inputdata[i][25].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "NETHERLANDS":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "PORTUGAL":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerNmaeupdate")), Inputdata[i][110].toString().trim());
									SendKeys(getwebelement(xml.getlocator("//locators/bi")), Inputdata[i][89].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/nif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "SPAIN":
							{
//								if(Inputdata[i][81].toString().equals("RFS"))
//								{
//								SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")),Inputdata[i][104].toString().trim());
//								SendKeys(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								}
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreetTypeUpdate")),Inputdata[i][104].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/provinceUpdate")), Inputdata[i][25].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								
								
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								if(Inputdata[i][105].toString().equals("List Number Options "))
								{
									Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
									if(Inputdata[i][106].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
									}
									if(Inputdata[i][107].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
									}
								}
								else
								{
									Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
								}
								
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								//submitagain
								break;
							}
							case "SWEDEN":
							{
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationname")), Inputdata[i][131].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Company Registration nnumber: " + Inputdata[i][131].toString());
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									//company name =customer name in sweden Address update scenario so inputdata column is 108.
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][108].toString().trim());
									implicitwait(5);

								}
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][103].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString().trim());								
								implicitwait(5);
								
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								break;
							}
							case "SWITZERLAND":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/munciapalityactivate")), Inputdata[i][86].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							default:
							{
								log("please select valid country");
								break;
							}
						}
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						log("Transaction id is:-"+Transactionresult);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
					}  
					else if (Inputdata[i][19].toString().contains("Deactivate")) 
					{
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(1000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									//DesiredUserACtion
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									Thread.sleep(2000);
									if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						try 
						{
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select user action as Deactivate");
//							WaitforElementtobeclickable(xml.getlocator("//locators/userGoRange"));
//							Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
							WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
							Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
							Thread.sleep(3000);
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
							implicitwait(20);
						} 
						catch (StaleElementReferenceException e) 
						{
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select user action as Deactivate");
//							WaitforElementtobeclickable(xml.getlocator("//locators/userGoRange"));
//							Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
							WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
							Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
							Thread.sleep(3000);
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						}
						StatuQuerybyTransitionId(TransactionId.get());
					} 
					else 
					{
						log("Search Scenario with activate scenario has been successfull performed");
					}
				}
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Quarantined": 
			{
				Thread.sleep(5000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("Reactivate")) 
					{
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(1000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									Thread.sleep(2000);
									if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						Thread.sleep(2000);
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						Thread.sleep(3000);
						try 
						{
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							StatuQuerybyTransitionId(Transactionresult);
						}
						catch (StaleElementReferenceException e) 
						{
						}
						implicitwait(20);
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}

				break;
			}
			case "Port In(Allocated)": 
			{
//				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Port In(Allocated)");
//				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Number Status as Port In(Allocated)");
//				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
//				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					Thread.sleep(5000);
					String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdLink")));
					TransactionId.set(Transactionresult);
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
					Thread.sleep(1000);
					implicitwait(20);
					StatuQuerybyTransitionId(Transactionresult);
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "PortIn(Activated)": 
			{
				Thread.sleep(5000);
				implicitwait(10);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("UpdateAddress")) 
					{
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(2000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								//Thread.sleep(2000);
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									Thread.sleep(2000);
									if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
									log("click on next button");
			
									//break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						Thread.sleep(3000);
						switch(Inputdata[i][2].toString())
						{
							case "UNITED KINGDOM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "AUSTRIA":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								Clear(getwebelement(xml.getlocator("//locators/CustomerName")));
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewPostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "BELGIUM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
									implicitwait(5);	
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
								}
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "DENMARK":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
//								if(Inputdata.toString().equals("RFS"))
//								{
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//									implicitwait(5);
//									//
//								}
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								//emerSerUpdBusinessName
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "FRANCE":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/departmentaddingnumber")),Inputdata[i][92].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								implicitwait(5);
								log("City is:-"+Inputdata[i][11].toString());
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								log("postcode is:-"+Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "GERMANY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								}	
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());								
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "IRELAND":
							{	
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][110].toString().trim());
									implicitwait(5);
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "ITALY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/freeacprovience")),Inputdata[i][25].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "NETHERLANDS":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "PORTUGAL":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerNmaeupdate")), Inputdata[i][110].toString().trim());
									SendKeys(getwebelement(xml.getlocator("//locators/bi")), Inputdata[i][89].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/nif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "SPAIN":
							{
//								if(Inputdata[i][81].toString().equals("RFS"))
//								{
//								SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")),Inputdata[i][104].toString().trim());
//								SendKeys(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								}
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreetTypeUpdate")),Inputdata[i][104].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/provinceUpdate")), Inputdata[i][25].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								
								
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								if(Inputdata[i][105].toString().equals("List Number Options "))
								{
									Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
									if(Inputdata[i][106].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
									}
									if(Inputdata[i][107].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
									}
								}
								else
								{
									Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
								}
								
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								//submitagain
								break;
							}
							case "SWEDEN":
							{
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationname")), Inputdata[i][131].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Company Registration nnumber: " + Inputdata[i][131].toString());
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									//company name =customer name in sweden Address update scenario so inputdata column is 108.
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][108].toString().trim());
									implicitwait(5);

								}
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][103].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString().trim());								
								implicitwait(5);
								
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								break;
							}
							case "SWITZERLAND":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/munciapalityactivate")), Inputdata[i][86].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							default:
							{
								log("please select valid country");
								break;
							}
						}

						Thread.sleep(1000);
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(1000);
						StatuQuerybyTransitionId(Transactionresult);
					}
					else if (Inputdata[i][19].toString().contains("Deactivate")) 
					{
						implicitwait(10);
						Thread.sleep(2000);
						String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
						int allData=Integer.parseInt(gett);
						int len=0;
						for (int k = 0; k < allData; k++) 
						{
						
							Thread.sleep(2000); 
							if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
							{
								len=len+1;
								Thread.sleep(2000);
								String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
								if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
								{
									Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									log("Click on the radio button");
									//Thread.sleep(2000);Select(getwebelement(xml.getlocator("//locators/DesiredUserAction").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
									Thread.sleep(2000);
									if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
									{
									WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
									Thread.sleep(2000);
									Select(tempdriver, Inputdata[i][16].toString());
									}
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
									Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
									break;
								}
								else 
								{
									log("Correct Telephone Number but Status is inocrrect please check the Status");
									break;
								}
							}
							else
							{
								log("Telephone number is inocrrect next try");
						
								Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
								log("click on next button");
								
							}
						}
						Thread.sleep(2000);
						if(len==0) {
							Assert.fail("Number is incorrect");
						}
		
						Thread.sleep(2000);
						if(Inputdata[i][81].toString().equals("RFS"))
						{
							if(Inputdata[i][2].toString().equals("BELGIUM")||Inputdata[i][2].toString().contentEquals("AUSTRIA"))
							{
								WaitforElementtobeclickable(xml.getlocator("//locators/subbuttond"));
								Clickon(getwebelement(xml.getlocator("//locators/subbuttond")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
								//subbuttond
							}
						}
						else
						{
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						}
						Thread.sleep(3000);
						String Transactionresult = null;
						try 
						{
							Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						} 
						catch (StaleElementReferenceException e) 
						{
							implicitwait(20);
						}
						StatuQuerybyTransitionId(Transactionresult);
					} 
					else
					{
						log("Search Scenario with port-in activated has been successfully performed");
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Port Out": 
			{
//				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Port Out");
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Port Out");
//				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
//				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Returned": 
			{
//				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Returned");
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Returned");
//				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
//				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(1000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					try 
					{
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdLink")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Transaction Id is : " + TransactionId.get());
						implicitwait(20);
						// Status Query ************************************************************
						StatuQuerybyTransitionId(Transactionresult);
					} 
					catch (StaleElementReferenceException e) 
					{
					}
				}
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Port In(Quarantined)":
			{
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
	//				DownloadExcel("NumberEnquiryReport");
					implicitwait(10);
					Thread.sleep(2000);
					String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
					int allData=Integer.parseInt(gett);
					int len=0;
					for (int k = 0; k < allData; k++) 
					{
					
						Thread.sleep(1000); 
						if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
						{
							len=len+1;
							String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
							if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
							{
								Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
								log("Click on the radio button");
								//Thread.sleep(2000);Select(getwebelement(xml.getlocator("//locators/DesiredUserAction").replace("Telephone Number Start", Inputdata[i][64].toString())), Inputdata[i][16].toString());
								Thread.sleep(2000);
								if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
								{
								WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
								Thread.sleep(2000);
								Select(tempdriver, Inputdata[i][16].toString());
								}
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
								Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
								break;
							}
							else 
							{
								log("Correct Telephone Number but Status is inocrrect please check the Status");
								break;
							}
						}
						else
						{
							log("Telephone number is inocrrect next try");
					
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on next button");
							
						}
					}
					Thread.sleep(2000);
					if(len==0) {
						Assert.fail("Number is incorrect");
					}
	
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
					log("Click on the submit button in the port in quarntined page");
					} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}

			default: 
			{
				RedLog("In Excel data, It seems like Number Status data is misspelled or not correct!! Data is"+ Inputdata[i][4].toString());
			}
			
			}
			
		}
	}

	public void casesforstatusandrange2ForActual(Object[][] Inputdata) throws InterruptedException, DocumentException, IOException
	{
		for(int i=0;i<Inputdata.length;i++)
		{
			switch (Inputdata[i][4].toString()) 
			{
			//same put on the NumberQuriy Using transaction Id
			case "Reserved": // Reserved to Active Scenario
			{
				Thread.sleep(3000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with search creteria!!");
					DownloadExcel("NumberEnquiryReport");
					WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
					Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
					Select(getwebelement(xml.getlocator("//locators/useractionRange")), Inputdata[i][16].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
					Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
					
//					
//					Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
//					Thread.sleep(2000);
//					Select(getwebelement(xml.getlocator("//locators/UserAction")), Inputdata[i][16].toString());
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the User Action: " + Inputdata[i][16].toString());
//					Clickon(getwebelement(xml.getlocator("//locators/userGo")));
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
					Thread.sleep(2000);
					if (Inputdata[i][20].toString().contains("WithAddress")) 
					{
						if (Inputdata[i][19].toString().contains("Activated")) 
						{
							ActivateSwitchCountriesName(Inputdata);
							Thread.sleep(5000);
							waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
							Thread.sleep(3000);
							StatuQuerybyTransitionId(Transactionresult);
							log("Successfully Updated status");
						} 
						else 
						{
							log("No action performed!!");
							log("In Sheet, Perform Action column does not have data to perform action with Activated!");
						}
					} 
					else if (Inputdata[i][20].toString().contains("WithoutAddress")) 
					{
						log("Start");
						if(Inputdata[i][2].toString().equals("Italy")) 
						{
							RedLog("For Italy without address scenario is not possible");
						}
						log("Address is not mandatory for this transaction ID");
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						Thread.sleep(4000);
						waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(2000);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
						log("Successfully Updated status");
					}
					else if(Inputdata[i][19].toString().contains("Cancel Reservation"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
						//submitagain
						Thread.sleep(4000);
						waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(2000);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
						log("Successfully Updated status");
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Activated": //same put on the Numberstatus Using Transaction Id
			{
				Thread.sleep(3000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("UpdateAddress")) 
					{
						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
						Select(getwebelement(xml.getlocator("//locators/useractionRange")), Inputdata[i][16].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
						Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
						Thread.sleep(3000);
						switch(Inputdata[i][2].toString())
						{
							case "UNITED KINGDOM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "AUSTRIA":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								Clear(getwebelement(xml.getlocator("//locators/CustomerName")));
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewPostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "BELGIUM":
							{
//								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
//								{
//								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
//								log("Service Type is:"+Inputdata[i][54].toString());
//								}
//								
//								if(Inputdata[i][86].toString().equals("Residential"))
//								{
//									implicitwait(5);
//									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
//									implicitwait(5);
//									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
//									implicitwait(5);	
//								}
//								else 
//								{
//									implicitwait(5);
//									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
//								}
//								implicitwait(5);
//								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								break;
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
//									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
//									implicitwait(5);	
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
								}
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/AddressExtension")), Inputdata[i][].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the AddressExtension: " + Inputdata[i][].toString());
								//AddressExtension
								Select(getwebelement(xml.getlocator("//locators/DirectroyListingOptions")),Inputdata[i][109].toString());
								log("Selct the Directroy Listing Options:-"+Inputdata[i][109]);
								WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
								Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try 
									{
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} 
									catch (Exception e) 
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
								Thread.sleep(3000);
//								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "DENMARK":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
//								if(Inputdata.toString().equals("RFS"))
//								{
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//									implicitwait(5);
//									//
//								}
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								//emerSerUpdBusinessName
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "FRANCE":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/departmentaddingnumber")),Inputdata[i][92].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								implicitwait(5);
								log("City is:-"+Inputdata[i][11].toString());
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								log("postcode is:-"+Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "GERMANY":
							{
//								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
//								{
//								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
//								log("Service Type is:"+Inputdata[i][54].toString());
//								}
//								if(Inputdata[i][86].toString().equals("Residential"))
//								{
//									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
//									implicitwait(5);
//									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
//									implicitwait(5);
//								}
//								else 
//								{
//									implicitwait(5);
//									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
//									implicitwait(5);
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//								}	
//								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());								
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								break;
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								}	
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//	locators/PostCode")), Inputdata[i][115].toString());								
								implicitwait(5);
								WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
								Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try 
									{
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} 
									catch (Exception e) 
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
								Thread.sleep(3000);
//								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
//								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
//								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "IRELAND":
							{	
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][110].toString().trim());
									implicitwait(5);
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "ITALY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/freeacprovience")),Inputdata[i][25].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "NETHERLANDS":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "PORTUGAL":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerNmaeupdate")), Inputdata[i][110].toString().trim());
									SendKeys(getwebelement(xml.getlocator("//locators/bi")), Inputdata[i][89].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/nif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "SPAIN":
							{
//								if(Inputdata[i][81].toString().equals("RFS"))
//								{
//								SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")),Inputdata[i][104].toString().trim());
//								SendKeys(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								}
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreetTypeUpdate")),Inputdata[i][104].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/provinceUpdate")), Inputdata[i][25].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								
								
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								if(Inputdata[i][105].toString().equals("List Number Options "))
								{
									Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
									if(Inputdata[i][106].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
									}
									if(Inputdata[i][107].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
									}
								}
								else
								{
									Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
								}
								
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								//submitagain
								break;
							}
							case "SWEDEN":
							{
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationname")), Inputdata[i][131].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Company Registration nnumber: " + Inputdata[i][131].toString());
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									//company name =customer name in sweden Address update scenario so inputdata column is 108.
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][108].toString().trim());
									implicitwait(5);

								}
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][103].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString().trim());								
								implicitwait(5);
								
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								break;
							}
							case "SWITZERLAND":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/munciapalityactivate")), Inputdata[i][86].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							default:
							{
								log("please select valid country");
								break;
							}
						}
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						log("Transaction id is:-"+Transactionresult);
						implicitwait(20);
						StatuQuerybyTransitionId(Transactionresult);
					}
					else if (Inputdata[i][19].toString().contains("Deactivate")) 
					{
						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
						Select(getwebelement(xml.getlocator("//locators/useractionRange")), Inputdata[i][16].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
						Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
						
						try 
						{
							WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
							Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
							Thread.sleep(3000);
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
							implicitwait(20);
						} 
						catch (StaleElementReferenceException e) 
						{
							WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
							Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
							Thread.sleep(3000);
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						}
						//StatuQuerybyTransitionId(TransactionId.get());
					} 
					else 
					{
						log("Search Scenario with activate scenario has been successfull performed");
					}
			}
				
				break;
			}
	
			case "Quarantined": 
			{
				Thread.sleep(5000);
				implicitwait(10);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("Reactivate")) 
					{
						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
						Thread.sleep(2000);
						Select(getwebelement(xml.getlocator("//locators/useractionRange")), Inputdata[i][16].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
						Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");						
						//useractionRange 
						
						//userGoRange
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
						Thread.sleep(2000);
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						Thread.sleep(3000);
						try 
						{
							String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							StatuQuerybyTransitionId(Transactionresult);
						}
						catch (StaleElementReferenceException e) 
						{
						}
						implicitwait(20);
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}

				break;
			}
			case "Port In(Allocated)": 
			{
//					WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
//					Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
//					Select(getwebelement(xml.getlocator("//locators/useractionRange")), Inputdata[i][16].toString());
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
//					Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					//Thread.sleep(5000);
//					String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdLink")));
//					TransactionId.set(Transactionresult);
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
//					Thread.sleep(1000);
//					implicitwait(20);
//					StatuQuerybyTransitionId(Transactionresult);
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "PortIn(Activated)": 
			{
				Thread.sleep(5000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					if (Inputdata[i][19].toString().contains("UpdateAddress")) 
					{
						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
						Select(getwebelement(xml.getlocator("//locators/useractionRange")), Inputdata[i][16].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
						Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");						
				
						Thread.sleep(2000);
						switch(Inputdata[i][2].toString())
						{
							case "UNITED KINGDOM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "AUSTRIA":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								Clear(getwebelement(xml.getlocator("//locators/CustomerName")));
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
								log(" customer name is required: " + Inputdata[i][110].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][111].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][111].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][112].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreet")), Inputdata[i][113].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the New Street Field: " + Inputdata[i][113].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the  New City Field: " + Inputdata[i][114].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/NewPostCode")), Inputdata[i][115].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								Thread.sleep(9000);
								break;
							}
							case "BELGIUM":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][95].toString().trim());
									implicitwait(5);	
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
								}
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "DENMARK":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
//								if(Inputdata.toString().equals("RFS"))
//								{
//									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
//									implicitwait(5);
//									//
//								}
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								//emerSerUpdBusinessName
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "FRANCE":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/departmentaddingnumber")),Inputdata[i][92].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								implicitwait(5);
								log("City is:-"+Inputdata[i][11].toString());
								Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								log("postcode is:-"+Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "GERMANY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
									implicitwait(5);
									javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth2")));
									implicitwait(5);
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								}	
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());								
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "IRELAND":
							{	
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][110].toString().trim());
									implicitwait(5);
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "ITALY":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/freeacprovience")),Inputdata[i][25].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][114].toString());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "NETHERLANDS":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][114].toString());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Thread.sleep(5000);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "PORTUGAL":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerNmaeupdate")), Inputdata[i][110].toString().trim());
									SendKeys(getwebelement(xml.getlocator("//locators/bi")), Inputdata[i][89].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								}
								SendKeys(getwebelement(xml.getlocator("//locators/nif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							case "SPAIN":
							{
//								if(Inputdata[i][81].toString().equals("RFS"))
//								{
//								SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")),Inputdata[i][104].toString().trim());
//								SendKeys(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][114].toString().trim());
//								implicitwait(5);
//								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
//								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//								implicitwait(5);
//								Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
//								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
//								}
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/cityagain")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/NewCity")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")), Inputdata[i][110].toString().trim());
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								
								SendKeys(getwebelement(xml.getlocator("//locators/NewStreetTypeUpdate")),Inputdata[i][104].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/provinceUpdate")), Inputdata[i][25].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
								implicitwait(5);
								
								
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								if(Inputdata[i][105].toString().equals("List Number Options "))
								{
									Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
									if(Inputdata[i][106].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
									}
									if(Inputdata[i][107].toString().equals("Yes"))
									{
										Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
										ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
									}
								}
								else
								{
									Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
								}
								
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								//submitagain
								break;
							}
							case "SWEDEN":
							{
								if (Inputdata[i][81].toString().contains("RFS")) 
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
									Thread.sleep(3000);
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
											Inputdata[i][7].toString());
									log(" customer name is required: " + Inputdata[i][7].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")), Inputdata[i][8].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Building name field: " + Inputdata[i][8].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
											Inputdata[i][9].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the building number Field: " + Inputdata[i][9].toString());
									SendKeys(getwebelement(xml.getlocator("//locators/againstreettemp")), Inputdata[i][10].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the New Street Field: " + Inputdata[i][10].toString());
								if(Inputdata[i][2].toString().equals("SPAIN")) {
										//freeacprovience
										Select(getwebelement(xml.getlocator("//locators/againprovince")), Inputdata[i][25].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										
										Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the City Field: " + Inputdata[i][11].toString());
										SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
										implicitwait(5);
										
									}
									else {
										SendKeys(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
										ExtentTestManager.getTest().log(LogStatus.PASS,
												" Step: Fill the  New City Field: " + Inputdata[i][11].toString());
									}
									SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
									ExtentTestManager.getTest().log(LogStatus.PASS,
											" Step: Fill the Post Code: " + Inputdata[i][12].toString());
									Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								else
								{
									if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
									{
									Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
									log("Service Type is:"+Inputdata[i][54].toString());
									}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationname")), Inputdata[i][131].toString());
								ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Company Registration nnumber: " + Inputdata[i][131].toString());
								if(Inputdata[i][86].toString().equals("Residential"))
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
									implicitwait(5);
									SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
								}
								else 
								{
									implicitwait(5);
									Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
									//company name =customer name in sweden Address update scenario so inputdata column is 108.
									SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][108].toString().trim());
									implicitwait(5);

								}
								//new Street number = building Number for spain so column number is 103
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][103].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString().trim());
								Thread.sleep(3000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString().trim());								
								implicitwait(5);
								
								WaitforElementtobeclickable(xml.getlocator("//locators/valdiateAddressupdate"));
								Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
								
								
								
								Thread.sleep(3000);
//								Set<String> handles = driver.getWindowHandles();
//								Iterator<String> iterator = handles.iterator();
//								String parent = iterator.next();
//								String curent = iterator.next();
//								System.out.println("Window handel" + curent);
//								driver.switchTo().window(curent);
								 String parentWinHandle = driver.getWindowHandle();
									Set<String> totalopenwindow=driver.getWindowHandles();
									if(totalopenwindow.size()>1) 
									{
									for(String handle: totalopenwindow)
									{
							            if(!handle.equals(parentWinHandle))
							            {
							            driver.switchTo().window(handle);
							            
							            }
									}
									}
								if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
								{
									RedLog("Excel sheet data are incorrect");
								}
								else
								{//
									Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
									Thread.sleep(2000);
									try {
										safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
									ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
								}
								Thread.sleep(2000);
//								driver.close();
//								Thread.sleep(2000);
								driver.switchTo().window(parentWinHandle);
//								
								
								Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								}
								break;
							}
							case "SWITZERLAND":
							{
								if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
								{
								Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
								log("Service Type is:"+Inputdata[i][54].toString());
								}
								
								SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][110].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][112].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][113].toString().trim());
								implicitwait(5);
								SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][114].toString());
								implicitwait(5);
								Thread.sleep(1000);
								SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][115].toString());
								implicitwait(5);
								Select(getwebelement(xml.getlocator("//locators/munciapalityactivate")), Inputdata[i][86].toString());
								implicitwait(5);
								Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the submit button");
								break;
							}
							default:
							{
								log("please select valid country");
								break;
							}
						}

						Thread.sleep(1000);
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						Thread.sleep(1000);
						StatuQuerybyTransitionId(Transactionresult);
					}
					else if (Inputdata[i][19].toString().contains("Deactivate")) 
					{
						WaitforElementtobeclickable(xml.getlocator("//locators/RadioButton"));
						Clickon(getwebelement(xml.getlocator("//locators/RadioButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
						Select(getwebelement(xml.getlocator("//locators/useractionRange")), Inputdata[i][16].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16].toString());
						Clickon(getwebelement(xml.getlocator("//locators/userGoRange")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");						
						//useractionRange 
						
						//userGoRange
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
						Thread.sleep(2000);
				
						Thread.sleep(2000);
						if(Inputdata[i][81].toString().equals("RFS"))
						{
							if(Inputdata[i][2].toString().equals("BELGIUM")||Inputdata[i][2].toString().contentEquals("AUSTRIA"))
							{
								WaitforElementtobeclickable(xml.getlocator("//locators/subbuttond"));
								Clickon(getwebelement(xml.getlocator("//locators/subbuttond")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
								//subbuttond
							}
						}
						else
						{
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						}
						Thread.sleep(3000);
						String Transactionresult = null;
						try 
						{
							Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionResult")));
							TransactionId.set(Transactionresult);
							ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Id is : " + TransactionId.get());
						} 
						catch (StaleElementReferenceException e) 
						{
							implicitwait(20);
						}
						StatuQuerybyTransitionId(Transactionresult);
					} 
					else
					{
						log("Search Scenario with port-in activated has been successfully performed");
					}
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Port Out": 
			{
//				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Port Out");
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Port Out");
//				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
//				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
				} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Returned": 
			{
//				Select(getwebelement(xml.getlocator("//locators/NumberStatus")), "Returned");
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Number Status as Returned");
//				WaitforElementtobeclickable(xml.getlocator("//locators/SearchButton"));
//				Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Search button");
				Thread.sleep(1000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
					DownloadExcel("NumberEnquiryReport");
					try 
					{
						String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/TransactionIdLink")));
						TransactionId.set(Transactionresult);
						ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Transaction Id is : " + TransactionId.get());
						implicitwait(20);
						// Status Query ************************************************************
						StatuQuerybyTransitionId(Transactionresult);
					} 
					catch (StaleElementReferenceException e) 
					{
					}
				}
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}
			case "Port In(Quarantined)":
			{
				Thread.sleep(2000);
				if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
				{
					GreenLog("Data are displaying with serach creteria!!");
	//				DownloadExcel("NumberEnquiryReport");
					implicitwait(10);
					Thread.sleep(2000);
					String gett=Gettext(getwebelement(xml.getlocator("//locators/pagelimt")));
					int allData=Integer.parseInt(gett);
					int len=0;
					for (int k = 0; k < allData; k++) 
					{
					
						Thread.sleep(1000); 
						if(isElementPresent(xml.getlocator("//locators/DesiredTelephoneNo").replace("Telephone Number Start", Inputdata[i][64].toString())))
						{
							len=len+1;
							String data = GetText(getwebelement(xml.getlocator("//locators/DesiredTelephonNowithStatus").replace("Telephone Number Start", Inputdata[i][64].toString())));
							if(data.equalsIgnoreCase(Inputdata[i][4].toString()))
							{
								Clickon(getwebelement(xml.getlocator("//locators/DesiredRadioBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
								log("Click on the radio button");
							
								Thread.sleep(2000);
								if(isElementPresent(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString())))
								{
								WebElement tempdriver=getwebelement(xml.getlocator("//locators/DesiredUserACtion").replace("Telephone Number Start", Inputdata[i][64].toString()));
								Thread.sleep(2000);
								Select(tempdriver, Inputdata[i][16].toString());
								}
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action as:-"+Inputdata[i][16]);
								Clickon(getwebelement(xml.getlocator("//locators/DesiredGoBTN").replace("Telephone Number Start", Inputdata[i][64].toString())));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
								break;
							}
							else 
							{
								log("Correct Telephone Number but Status is inocrrect please check the Status");
								break;
							}
						}
						else
						{
							log("Telephone number is inocrrect next try");
					
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on next button");
							
						}
					}
					Thread.sleep(2000);
					if(len==0) {
						Assert.fail("Number is incorrect");
					}
	
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/submitagain")));
					log("Click on the submit button in the port in quarntined page");
					} 
				else 
				{
					RedLog("System does not have search result with Number Status with country!! Number status is "
							+ Inputdata[i][4].toString().trim() + " And Country is " + Inputdata[i][2].toString());
				}
				break;
			}

		default: 
			{
				RedLog("In Excel data, It seems like Number Status data is misspelled or not correct!! Data is"+ Inputdata[i][4].toString());
			}
			
			}
			
		}
	}

	
	
	public void NumberQuiryUsingTransactionID(Object[][] Inputdata)throws InterruptedException, DocumentException, IOException 
	{
		for (int i = 0; i < Inputdata.length; i++) 
		{
			Clickon(getwebelement(xml.getlocator("//locators/ManageNumber")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Manage Number");
			Clickon(getwebelement(xml.getlocator("//locators/NumberEnquiry")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Number Enquiry");
			Thread.sleep(2000);
			Select(getwebelement(xml.getlocator("//locators/ResellerName")), Inputdata[i][1].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Reseller name: " + Inputdata[i][1].toString());
			Select(getwebelement(xml.getlocator("//locators/Country")), Inputdata[i][2].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Country: " + Inputdata[i][2].toString());
			implicitwait(20);
			Thread.sleep(1000);
			WaitforElementtobeclickable(xml.getlocator("//locators/ServiceProfile"));
			Select(getwebelement(xml.getlocator("//locators/ServiceProfile")), Inputdata[i][3].toString().trim());
			ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Select the Service Profile: " + Inputdata[i][3].toString());
			Thread.sleep(2000);
			Select(getwebelement(xml.getlocator("//locators/SearchCriteria")), "Transaction ID");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search Criteria as Number Status");
			Thread.sleep(1000);
			SendKeys(getwebelement(xml.getlocator("locators/InputTransactionId")), Inputdata[i][18].toString());
			implicitwait(20);
			Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
			//validate();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:click the Search button");
			//Thread.sleep(1500);
			Thread.sleep(15000);
			//DownloadExcel("NumberEnquiryReport");
			if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
			{
				GreenLog("Data are displaying with serach creteria!!");
				
				
				//=====================================================================
				Thread.sleep(3000);
				casesforstatusandrange2ForActual(Inputdata);
				Thread.sleep(3000);
				//=====================================================================
			}
			else 
			{
				RedLog("System does not have search result with Search Transaction Id with country!! Transection id is "+ Inputdata[i][18].toString() + " And Country is " + Inputdata[i][2].toString());
			}
			GreenLog("/******************************* current Step  has been finished * ******************************************");
		}
	}

	public void NumberQuiryUsingNumberRange(Object[][] Inputdata)throws InterruptedException, DocumentException, IOException 
	{
		for (int i = 0; i < Inputdata.length; i++) 
		{
			Clickon(getwebelement(xml.getlocator("//locators/ManageNumber")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Manage Number");
			Clickon(getwebelement(xml.getlocator("//locators/NumberEnquiry")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Number Enquiry");
			Select(getwebelement(xml.getlocator("//locators/ResellerName")), Inputdata[i][1].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Reseller name: " + Inputdata[i][1].toString());
			Select(getwebelement(xml.getlocator("//locators/Country")), Inputdata[i][2].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Country: " + Inputdata[i][2].toString());
			Thread.sleep(2000);  
			Select(getwebelement(xml.getlocator("//locators/ServiceProfile")), Inputdata[i][3].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Service Profile: " + Inputdata[i][3].toString());
			Thread.sleep(6000);
			Select(getwebelement(xml.getlocator("//locators/SearchCriteria")), "Number Range");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search Criteria as Number Status");
			Thread.sleep(6000);
			if(Inputdata[i][2].toString().equals("GERMANY")) 
			{
				log("no need of main number");
				SendKeys(getwebelement(xml.getlocator("//locators/germanyAreacide")), Inputdata[i][5].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Local Area code: " + Inputdata[i][5].toString());
				
				Thread.sleep(3000);
				Clickon(getwebelement(xml.getlocator("//locators/RangeEnd")));
			}
//			else if(Inputdata[i][2].toString().equals("GERMANY"))
//			{
//				SendKeys(getwebelement(xml.getlocator("//locators/LocalAreaCode")), Inputdata[i][5].toString());
//				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Local Area code: " + Inputdata[i][5].toString());
//				SendKeys(getwebelement(xml.getlocator("//locators/mainNumber")), Inputdata[i][13].toString());
//				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number: " + Inputdata[i][13].toString());
//			}
			else if(Inputdata[i][2].toString().equals("FRANCE"))
			{
				SendKeys(getwebelement(xml.getlocator("//locators/germanyAreacide")), Inputdata[i][5].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Local Area code: " + Inputdata[i][5].toString());
				Thread.sleep(3000);
				Clickon(getwebelement(xml.getlocator("//locators/mainNumber")));
//				SendKeys(getwebelement(xml.getlocator("//locators/mainNumber")), Inputdata[i][13].toString());
//				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number: " + Inputdata[i][13].toString());
			}
			else 
			{
				Select(getwebelement(xml.getlocator("//locators/LocalAreaCode")), Inputdata[i][5].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Local Area code: " + Inputdata[i][5].toString());
				SendKeys(getwebelement(xml.getlocator("//locators/mainNumber")), Inputdata[i][13].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number: " + Inputdata[i][13].toString());
			}
			Thread.sleep(3000);
			SendKeys(getwebelement(xml.getlocator("//locators/RangeStart")), Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Enter the RangeStart: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the RangeEnd: " + Inputdata[i][15].toString());
			Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:click the Search button");
			Thread.sleep(15000);
			implicitwait(10);
			//DownloadExcel("NumberEnquiryReport");
			if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
			{
				GreenLog("Data are displaying with serach creteria!!");
				
				
				//=====================================================================
				Thread.sleep(3000);
				casesforstatusandrange2ForActual(Inputdata);
				Thread.sleep(3000);
				//=====================================================================
			} 
			else 
			{
				RedLog("System does not have search result with SearchNumber Range with country!! Number Range is "
						+ Inputdata[i][14].toString() + " to " + Inputdata[i][15].toString() + " And Country is "+ Inputdata[i][2].toString());
			}
			GreenLog("/******************************* current Step  has been finished * ******************************************");
		}
	}

	public void NumberQuiryUsingCustomerReferance(Object[][] Inputdata)throws InterruptedException, DocumentException, IOException 
	{
		for (int i = 0; i < Inputdata.length; i++)
		{
			Clickon(getwebelement(xml.getlocator("//locators/ManageNumber")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Manage Number");
			Clickon(getwebelement(xml.getlocator("//locators/NumberEnquiry")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Number Enquiry");
			Select(getwebelement(xml.getlocator("//locators/ResellerName")), Inputdata[i][1].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Select the Reseller name: " + Inputdata[i][1].toString());
			Select(getwebelement(xml.getlocator("//locators/Country")), Inputdata[i][2].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Country: " + Inputdata[i][2].toString());
			implicitwait(20);
			Select(getwebelement(xml.getlocator("//locators/ServiceProfile")), Inputdata[i][3].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Select the Service Profile: " + Inputdata[i][3].toString());
			Thread.sleep(3000);
			Select(getwebelement(xml.getlocator("//locators/SearchCriteria")), "Customer Reference");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Search Criteria as Customer Reference");
			// need here to paste customer reference
			SendKeys(getwebelement(xml.getlocator("//locators/CustomerReferenceNumber")),Inputdata[i][6].toString().trim());
			implicitwait(10);
			Clickon(getwebelement(xml.getlocator("//locators/SearchButton")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step:click the Search button");
			Thread.sleep(15000);
			implicitwait(10);
			if (isElementPresent(xml.getlocator("//locators/Griddata"))) 
			{
				GreenLog("Data are displaying with serach creteria!!");
				//=====================================================================
				Thread.sleep(3000);
				casesforstatusandrange(Inputdata);
				Thread.sleep(3000);
				//=====================================================================
			} 
			else 
			{
				RedLog("System does not have search result with Customer Reference  with country!! Customer Reference is "+ Inputdata[i][6].toString().trim() + " And Country is " + Inputdata[i][2].toString());
			}
			GreenLog("/******************************* current Step  has been finished * ******************************************");
		}
	}
	
//	public void ActivateStatus(Object[][] Inputdata, int i)throws InterruptedException, DocumentException, IOException 
//	{
//		log("Start");
//		Thread.sleep(2000);
////		if (isElementPresent(xml.getlocator("locators/ReserveElement").replace("Transactionid", Inputdata[i][35].toString()))) 
////		{
////			int size = getwebelementscount(xml.getlocator("locators/ReserveElementall"));
////			java.util.List<WebElement> all = getwebelements(xml.getlocator("locators/ReserveElementall"));
////			for (int j = 0; j < size; j++)
////			{
////				String transid = all.get(j).getText();
////				System.out.println(transid);
////				javascriptexecutor(getwebelement(xml.getlocator("locators/ReserveElement").replace("Transactionid",Inputdata[i][35].toString())));
////				implicitwait(20);
////				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Radio button");
////				Clickon(getwebelement(xml.getlocator("locators/ReserveElement").replace("Transactionid",Inputdata[i][35].toString())));
////				Thread.sleep(2000);
////				Select(getwebelement(xml.getlocator("//locators/SelectAction").replace("Transactionid",Inputdata[i][35].toString())), "Activate");
////				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the User Action");
////				implicitwait(20);
////				WaitforElementtobeclickable(xml.getlocator("//locators/Gobtnmain").replace("Transactionid", Inputdata[i][35].toString()));
////				Clickon(getwebelement(xml.getlocator("//locators/Gobtnmain").replace("Transactionid", Inputdata[i][35].toString())));
////				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Go button");
////				Thread.sleep(5000);
////				break;
////			}
////			
//				
////			else 
////			{
////				StatuQuerybyTransitionId(Transactionresult);
////			}
//		
//	}
//
	public void validate() throws InterruptedException, DocumentException 
	{
		if (getwebelement(xml.getlocator("//locators/mainError")).isDisplayed()) 
		{
			String errormsg = getwebelement(xml.getlocator("//locators/mainError")).getText();
		}
	}
	public void Update_porting_request(Object[][] Inputdata) throws InterruptedException, IOException, DocumentException
	{
		for (int i = 0; i < Inputdata.length; i++) 
		{
			WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
			Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
			log("click on managing porting");
			Thread.sleep(1000);
			Clickon(getwebelement(xml.getlocator("//locators/UpdatePortingRequest")));
			implicitwait(10);
			log("click on UpdatePortingRequest");
			Thread.sleep(2000);
		NumberHosting_Transaction_StatusQuery(Inputdata);
		Thread.sleep(2000);
		if (isElementPresent(xml.getlocator("//locators/ErrornoRecord"))) 
		{
			RedLog("No record Found");
		}
		else
		{
			WaitforElementtobeclickable((xml.getlocator("//locators/checkboxstatus")));
			Clickon(getwebelement(xml.getlocator("//locators/checkboxstatus")));
			log("click on the checkbox");
			implicitwait(5);
			SendKeys(getwebelement(xml.getlocator("//locators/ActionPortin")), Inputdata[i][16].toString());
			log("select the action as:-"+Inputdata[i][16].toString());
			Thread.sleep(5000);
			WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
			Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
			log("click on the go button");
		}
		
		WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
		Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
		log("click on the submit button");
		Thread.sleep(2000);
		AcceptJavaScriptMethod();
		log("submitted succesfully");
		Thread.sleep(10000);
		boolean flag = false;
		do 
		{
			flag = isElementPresent(xml.getlocator("//locators/verifystatus2"));
			// ChildCurrentStat.set(ChildCurrentStatus);
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: ChildCurrentStatus is : " + ChildCurrentStat.get());
			log("if child Current Status shown than go to the next step button otherwise click on the refresh button");
			WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
			Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
		} 
		while (!flag == true);
		log("childcurrent status id successfully submited");
		Thread.sleep(5000);
		}	
	}
	public void NumberHosting_Transaction_StatusQuery(Object[][] Inputdata) throws InterruptedException, IOException, DocumentException
	{
		for(int i = 0; i<Inputdata.length; i++)
		{
			if(Inputdata[i][0].equals("Number HostingTransaction"))
			{
			Moveon(getwebelement(xml.getlocator("//locators/statusQuery")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Move to Status Query ");
			implicitwait(10);
			WaitforElementtobeclickable(xml.getlocator("//locators/NumberHostingTransaction"));
			Clickon(getwebelement(xml.getlocator("//locators/NumberHostingTransaction")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Number Hosting Transaction");
			}
			if(Inputdata[i][139].toString().equals("Yes"))
			{
				Select(getwebelement(xml.getlocator("//locators/ResellerInTxStatus")), Inputdata[i][140].toString());
				log("Reseller value is:-"+Inputdata[i][140].toString());
				implicitwait(5);
			}
			else
				{
					log("User does not need Reseller");
				}
			if(Inputdata[i][141].toString().equals("Yes"))
			{
				Select(getwebelement(xml.getlocator("//locators/countryInTxStatus")), Inputdata[i][142].toString());
				log("country value is:-"+Inputdata[i][142].toString());
				implicitwait(5);
			}
			else
				{
					log("User does not need country");
				}
			if(Inputdata[i][143].toString().equals("Yes"))
			{
				Select(getwebelement(xml.getlocator("//locators/serviceInTxStatus")), Inputdata[i][144].toString());
				log("Service Profile value is:-"+Inputdata[i][144].toString());
				implicitwait(5);
			}
			else
				{
					log("User does not need Service Profile");
				}
			if(Inputdata[i][145].toString().equals("Yes"))
			{
				Select(getwebelement(xml.getlocator("//locators/transactiontypeInTxStatus")), Inputdata[i][146].toString());
				log("Transaction type value is:-"+Inputdata[i][146].toString());
				implicitwait(5);
			}
			else
				{
					log("User does not need Transaction Type");
				}
			if(Inputdata[i][147].toString().equals("Yes"))
			{
				Select(getwebelement(xml.getlocator("//locators/transactionStatusInTxStatus")), Inputdata[i][148].toString());
				log("Transaction status value is:-"+Inputdata[i][148].toString());
				implicitwait(5);
			}
			else
				{
					log("User does not need Transaction Status");
				}
			if(Inputdata[i][149].toString().equals("Yes"))
			{
				waitandForElementDisplayed(xml.getlocator("//locators/statusQueryTransactionID"));
				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")), Inputdata[i][150].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Transaction ID: " + Inputdata[i][150].toString());
			}
			else
				{
					log("User does not need transaction ID");
				}
			if(Inputdata[i][151].toString().equals("Yes"))
			{
				javaexecutotSendKeys(Inputdata[i][152].toString(),getwebelement(xml.getlocator("//locators/txCreationFromDate")));
			}
			else
			{
				log("Transacation Creation Date from date is not required");
			}
			if(Inputdata[i][153].toString().equals("Yes"))
			{
				javaexecutotSendKeys(Inputdata[i][154].toString(),getwebelement(xml.getlocator("//locators/txcreationToDate")));
			}
			else
			{
				log("Transacation Creation Date To date is not required");	
			}
			if(Inputdata[i][155].toString().equals("Yes"))
			{
				javaexecutotSendKeys(Inputdata[i][156].toString(),getwebelement(xml.getlocator("//locators/txLastUpdateFromDate")));
			}
			else
			{
				log("Transacation Last update Date from date is not required");
			}
			if(Inputdata[i][157].toString().equals("Yes"))
			{
				javaexecutotSendKeys(Inputdata[i][158].toString(),getwebelement(xml.getlocator("//locators/txLastUpdateToDate")));
			}
			else
			{
				log("Transacation last update Date To date is not required");	
			}
			
			Thread.sleep(5000);
			Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on submit button");
		}
	}

	public void StatuQuerybyTransitionId(String TransactionID)throws InterruptedException, DocumentException, IOException 
	{
		ExtentTestManager.getTest().log(LogStatus.PASS, " scenario complete");
		Moveon(getwebelement(xml.getlocator("//locators/statusQuery")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Move to Status Query ");
		implicitwait(20);
//		if(Inputdata[i][].toString().equals("YES"))
//		{
//			Select(getwebelement(xml.getlocator("//locators/ResellerInTxStatus")), Inputdata[i][].toString());
//			implicitwait(5);
//		}
//		else
//			{
//				log("User does not need Reseller");
//			}
//		if(	)
		WaitforElementtobeclickable(xml.getlocator("//locators/NumberHostingTransaction"));
		Clickon(getwebelement(xml.getlocator("//locators/NumberHostingTransaction")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Number Hosting Transaction");
		waitandForElementDisplayed(xml.getlocator("//locators/statusQueryTransactionID"));
		SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")), TransactionID);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Transaction ID: " + TransactionID);
		// WaitforElementtobeclickable(xml.getlocator("//locators/StatusQuerySearchbutton"));
		Thread.sleep(10000);
		Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on submit button");
//		Thread.sleep(5000);
////>>>>>>>>>>>>GRID DATA CONCEPT 
//		if (isElementPresent(xml.getlocator("//locators/GriddataforTX"))) 
//		{
//			String transType = Gettext(getwebelement(xml.getlocator("//locators/TransactionType")));
//			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Transaction Id is : " + transType);
//			System.out.println("*****************************************************************************" + transType);
//			implicitwait(20);
//			String transStatus = Gettext(getwebelement(xml.getlocator("//locators/StatusQueryTransactionStatus")));
//			System.out.println("*****************************************************************************" + transStatus);
//			if (transStatus.contains("Firm order commitment")) 
//			{
//				GreenLog("Status Query has been passed with Transaction Status Firm order commitment");
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Transaction Status is :Firm order commitment");
//			} 
//			else if (transStatus.contains("Porting initiated")) 
//			{
//				GreenLog("Status Query has been passed with Transaction Status Porting initiated");
//				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Transaction Status is :Porting initiated");
//			}
//			else 
//			{
//				boolean flag3 = false;
//				String date3 = null;
//				DateFormat dateFormat = new SimpleDateFormat("HH:mm");
//				String date1 = dateFormat.format(new Date(System.currentTimeMillis()));
//				Date dd = new Date(System.currentTimeMillis() + 10 * 60 * 1000);
//				String date2 = dateFormat.format(dd);
//				do 
//				{
//					Thread.sleep(2000);
//					Pagerefresh();
//					Thread.sleep(2000);
//					flag3 = isElementPresent(xml.getlocator("//locators/QuesryForCompleted"));
//					date3 = dateFormat.format(new Date(System.currentTimeMillis()));
//					System.out.println(date3);
//					System.out.println(date2);
//					Thread.sleep(1000);
//					Pagerefresh();
//					if (date3.trim().equals(date2.trim()) && flag3 == false) 
//					{
//						System.out.println("System is waiting for 10 minutes to change status, But still status in not changed!");
//						RedLog2("waited 10 minute to change Status from InProgress to Complted!");
//						Assert.fail("waited 10 minute to change Status from InProgress to Complted!");
//						break;
//					}
//				} 
//				while (flag3 == false);
//				// waitandForElementDisplayed("//locators/StatusQueryTransactionStatus");
//				try 
//				{
//					String transStatus1 = Gettext(getwebelement(xml.getlocator("//locators/StatusQueryTransactionStatus")));
//					Thread.sleep(5000);
//					if (transStatus1.contains("Completed")) 
//					{
//						GreenLog("Status Query has been passed with Transaction Status Completed");
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Transaction Status is :Completed");
//					} 
//					else if (transStatus1.contains("In Progress")) 
//					{
//						Assert.fail("System is taking longer time to change Status from InProgress to Complted!");
//					}
//					else if (transStatus1.contains("Firm order commitment")) 
//					{
//						GreenLog("Status Query has been passed with Transaction Status Firm order commitment");
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Status is :Firm order commitment");
//					} 
//					else if (transStatus1.contains("Porting initiated")) 
//					{
//						GreenLog("Status Query has been passed with Transaction Status Porting initiated");
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Status is :Porting initiated");
//					}
//					else 
//					{
//						RedLog("it seems like there is no any Transaction Status value which contained Completed or In Process or there is no any record found for this transaction ID");
//						Assert.fail();
//					}
//				}
//				catch (StaleElementReferenceException e) 
//				{
//
//				}
//			}
//		}
	}
	
	
	public void DownloadExcel(String Filaname) throws InterruptedException, DocumentException 
	{
		String downloadPath = null;
		try 
		{
			downloadPath = GetExcelpath("Excel");
		} 
		catch (Exception e) 
		{

			e.printStackTrace();
		}
		// Assert.assertTrue(isFileDownloaded(downloadPath, Filaname), "Failed to
		// download Expected document");
		Clickon(getwebelement(xml.getlocator("//locators/DownloadExcel")));
		GreenLog("Step: Excel has been downloded : Successfully");
	}
	
	
	public void PortIn(Object[][] Inputdata) throws Exception 
	{
		for (int i = 0; i < Inputdata.length; i++) 
		{
			if (Inputdata[i][0].toString().trim().equals("Port-In")) 
			{		
				switch(Inputdata[i][2].toString())
				{
				case "UNITED KINGDOM":
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on ANH and next button");
						}
						Thread.sleep(1000); 
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on ANH");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//write code for 20 filds
					Thread.sleep(2000);
					//int lent=(int) Inputdata[i][160];
					int lent=Integer.parseInt((String) Inputdata[i][160]);
						SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
						implicitwait(5);
						Select(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
						implicitwait(5);
						Select(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
						implicitwait(5);	
					
						if(lent>1) 	
						{
						Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
						//main billing Number
						SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
						implicitwait(5);
						//local are code 
						Select(getwebelement(xml.getlocator("//locators/localAreacodefor2")), Inputdata[i][162].toString());
						implicitwait(5);
						//Main Number
						SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
						implicitwait(5);
						//Range Start
						SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
						implicitwait(5);
						//Range End
						SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
						implicitwait(5);
						//current Provider
						Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
						implicitwait(5);
						//167
						if(lent > 2 && lent < 21)
						{
							int afterSecond=lent-2;
							int count=167;
							for(int total=0;total<afterSecond;total++)
							{
								Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//local are code 
								Select(getwebelement(xml.getlocator("//locators/localAreacodefor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//Main Number
								SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//Range Start
								SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//Range End
								/*count=171*/
								SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//current Provider
								/*count=172*/
								Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
							}
						}
					}
					
					
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					implicitwait(5);
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Thread.sleep(1000);
						Clickon(getwebelement(xml.getlocator("//locators/DSUyes")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						Select(getwebelement(xml.getlocator("//locators/ActiavteLineType")),Inputdata[i][116].toString());
						log("Selct the Line Type:-"+Inputdata[i][116].toString());
//						Select(getwebelement(xml.getlocator("//locators/Activatetarrif")),Inputdata[i][117].toString());
//						log("Selct the tarrif:-"+Inputdata[i][117].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateEntryType")),Inputdata[i][118].toString());
						log("Selct the Entry Type:-"+Inputdata[i][118].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateTypeface")),Inputdata[i][119].toString());
						log("Selct the Activate Typeface:-"+Inputdata[i][119].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingCategory")),Inputdata[i][120].toString());
						log("Selct the Activate ListingCategory:-"+Inputdata[i][120].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingType")),Inputdata[i][121].toString());
						log("Selct the Activate ActivateListingType:-"+Inputdata[i][121].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivatePriority")),Inputdata[i][122].toString());
						log("Selct the Activate Priority:-"+Inputdata[i][122].toString());
					}
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				case "AUSTRIA":
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on A98 and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							//a98
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/A98").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/A98").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on A98");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
					log("Add button is not avilable so we cant put 20 data");
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/Choosefile")),"C:\\Users\\ASrivastava5-adm\\Desktop\\fasdfasd.docx" );
					//button.click();
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					implicitwait(5);
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/checkboxforDSU")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64]);
					}
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				case "ITALY":
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on A5X and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							//A5X
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/A5X").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/A5X").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on A5X");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/portinprovience")),Inputdata[i][25].toString());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/NewProviencePortIn")),Inputdata[i][132].toString());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
					log("Add button is not avilable so we cant drop 20 data");
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/secretcode")),Inputdata[i][98].toString().trim());
					implicitwait(5);
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/firstnameportin")), Inputdata[i][82].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/lastnameportin")), Inputdata[i][83].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/phonenumber")), Inputdata[i][84].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/emailidportin")), Inputdata[i][85].toString().trim());
					implicitwait(5);
					implicitwait(5);
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					implicitwait(5);
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/checkboxforDSU")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/siretNumber")), Inputdata[i][88].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Fill the VAT number: " + Inputdata[i][88].toString());	
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivationProvience")), Inputdata[i][132].toString());
						log("Step: fill the Activation Provience:-"+Inputdata[i][132].toString());
						Select(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						if(Inputdata[i][133].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/ActivationCheckboxmail")));
							log("click on the Send Advertising mail check box");
						}
						if(Inputdata[i][134].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/amalgamate")));
							log("click on the Amalgamate Numbers with VAT/Tax code check box");
						}
						if(Inputdata[i][135].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/ActivationReceivedmail")));
							log("click on the Receive Advertising calls check box");
						}
						if(Inputdata[i][136].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/Searchbasisontelephonenumber")));
							log("click on the Search on the basis of telephone number check box");
						}	
					}					
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				
				case "SWITZERLAND":
				{
					//BFI
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on BFI and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							//BFI
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/BFI").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/BFI").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on BFI");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString().trim());
					implicitwait(5);
					Thread.sleep(3000);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/Municipality")),Inputdata[i][86].toString() );
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/MuncipalityPortIN2")),Inputdata[i][277].toString() );
					implicitwait(5);
					}
					//**********************************
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor4")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
					Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor5").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}
					Thread.sleep(2000);
					//ServiceTypeUpdate
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
						Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					}
					
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
				     if(Inputdata[i][101].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/DSUyes2")));
							log("click on the Directory service update check box"); 
							Thread.sleep(3000);
//							Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//							log("Selct the order type for DSU:-"+Inputdata[i][102]);
							SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
							log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
							SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
							log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64]);
							Select(getwebelement(xml.getlocator("//locators/ActivaitonLanguage")),Inputdata[i][129].toString());
							log("Selct the Activation language:-"+Inputdata[i][129]);	
							if(Inputdata[i][135].toString().equals("Yes"))
							{
								Clickon(getwebelement(xml.getlocator("//locators/ActivationReceivedmail")));
								log("click on the Receive Advertising calls check box");
							}	
						}
					implicitwait(5);
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}   
				case "SWEDEN":
				{
					//BE4
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on BE4 and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							//BE4
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/BE4").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/BE4").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on BE4");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
					}
					else {
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
						implicitwait(5);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/streetNumber")),Inputdata[i][103].toString().trim());
					implicitwait(5);
					//SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					Thread.sleep(1000);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][113].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/StreetNumberPortIn")),Inputdata[i][278].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
//					Select(getwebelement(xml.getlocator("//locators/MuncipalityPortIN2")),Inputdata[i][277].toString() );
//					implicitwait(5);
					//**********************************
////					if(Inputdata[i][81].toString().equals("RFS")){
////						log("old version of RFS");
////					}
////					else
////					{
////					WaitforElementtobeclickable(xml.getlocator("//locators/validbutton2"));
////					Clickon(getwebelement(xml.getlocator("//locators/validbutton2")));
////					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
////					Thread.sleep(3000);
//////					Set<String> handles = driver.getWindowHandles();
//////					Iterator<String> iterator = handles.iterator();
//////					String parent = iterator.next();
//////					String curent = iterator.next();
//////					System.out.println("Window handel" + curent);
//////					driver.switchTo().window(curent);
////					 String parentWinHandle = driver.getWindowHandle();
////						Set<String> totalopenwindow=driver.getWindowHandles();
////						if(totalopenwindow.size()>1) 
////						{
////						for(String handle: totalopenwindow)
////						{
////				            if(!handle.equals(parentWinHandle))
////				            {
////				            driver.switchTo().window(handle);
////				            
////				            }
////						}
////						}
////					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
////					{
////						RedLog("Excel sheet data are incorrect");
////					}
////					else
////					{//
////						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
////						Thread.sleep(2000);
////						try {
////							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
////						} catch (Exception e) {
////							// TODO Auto-generated catch block
////							e.printStackTrace();
////						}
////						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
////						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
////					}
////					Thread.sleep(2000);
//////					driver.close();
//////					Thread.sleep(2000);
////					driver.switchTo().window(parentWinHandle);
//////					
//					Select(getwebelement(xml.getlocator("//locators/Municipality")),Inputdata[i][86].toString() );
//					implicitwait(5);
//					}
					Thread.sleep(2000);
					//int lent=(int) Inputdata[i][160];
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor3")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
					Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor5").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}

					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					if(isElementPresent(xml.getlocator("//locators/subscriberID")))
					{
						SendKeys(getwebelement(xml.getlocator("//locators/subscriberID")), Inputdata[i][97].toString().trim());
					}
					//subscriberID
					Thread.sleep(3000);
					Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
					Thread.sleep(5000);
					
					//last udpate 1 june 2020
				 	Thread.sleep(2000);
			     	WaitforElementtobeclickable(xml.getlocator("//locators/validbutton2"));
					Clickon(getwebelement(xml.getlocator("//locators/validbutton2")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
					Thread.sleep(3000);
//					Set<String> handles = driver.getWindowHandles();
//					Iterator<String> iterator = handles.iterator();
//					String parent = iterator.next();
//					String curent = iterator.next();
//					System.out.println("Window handel" + curent);
//					driver.switchTo().window(curent);
					 String parentWinHandle = driver.getWindowHandle();
						Set<String> totalopenwindow=driver.getWindowHandles();
						if(totalopenwindow.size()>1) 
						{
						for(String handle: totalopenwindow)
						{
				            if(!handle.equals(parentWinHandle))
				            {
				            driver.switchTo().window(handle);
				            
				            }
						}
						}
					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
					{
						RedLog("Excel sheet data are incorrect");
					}
					else
					{//
						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
						Thread.sleep(2000);
						try {
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
					}
					Thread.sleep(2000);
//					driver.close();
//					Thread.sleep(2000);
					driver.switchTo().window(parentWinHandle);
				
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				case "IRELAND":
				{
					//BE2
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on BE2 and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							//BE2
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/BE2").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/BE2").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on BE2");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
						implicitwait(5);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
//					Select(getwebelement(xml.getlocator("//locators/MuncipalityPortIN2")),Inputdata[i][277].toString() );
//					implicitwait(5);
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/localAreacodefor2")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
					Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							Select(getwebelement(xml.getlocator("//locators/localAreacodefor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}

					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					Thread.sleep(2000);
					Clickon(getwebelement(xml.getlocator("//locators/customerInstructionForm")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try  
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					implicitwait(5);
					if(Inputdata[i][101].toString().equals("Yes"))
					{ 
						Clickon(getwebelement(xml.getlocator("//locators/DSUport"))); 
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						Select(getwebelement(xml.getlocator("//locators/selectCity")), Inputdata[i][114].toString()); 
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						Select(getwebelement(xml.getlocator("//locators/ActiavteLineType")),Inputdata[i][116].toString());
						log("Selct the Line Type:-"+Inputdata[i][116].toString());
//						Select(getwebelement(xml.getlocator("//locators/Activatetarrif")),Inputdata[i][117].toString());
//						log("Selct the tarrif:-"+Inputdata[i][117].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateEntryType")),Inputdata[i][118].toString());
						log("Selct the Entry Type:-"+Inputdata[i][118].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateTypeface")),Inputdata[i][119].toString());
						log("Selct the Activate Typeface:-"+Inputdata[i][119].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingCategory")),Inputdata[i][120].toString());
						log("Selct the Activate ListingCategory:-"+Inputdata[i][120].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingType")),Inputdata[i][121].toString());
						log("Selct the Activate ActivateListingType:-"+Inputdata[i][121].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivatePriority")),Inputdata[i][122].toString());
						log("Selct the Activate Priority:-"+Inputdata[i][122].toString());
					}
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				case "NETHERLANDS":
				{
					//A93
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on A93 and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/A93").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/A93").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on A93");
							Thread.sleep(2000);
							break;
						}
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/vatnumber")),Inputdata[i][88].toString().trim());
					}
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
//					Select(getwebelement(xml.getlocator("//locators/MuncipalityPortIN2")),Inputdata[i][277].toString() );
//					implicitwait(5);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
//					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/localAreacodefor2")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
	//				Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
		//					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							Select(getwebelement(xml.getlocator("//locators/localAreacodefor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
			//				Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}

					if(isElementPresent(xml.getlocator("//locators/portindate")))
					{
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					}
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					if(Inputdata[i][101].toString().equals("Yes"))
					{ 
					
						Clickon(getwebelement(xml.getlocator("//locators/DSUagain")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						Select(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
					}
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				case "PORTUGAL":
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on BFE and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/BFE").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/BFE").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on BFE");
							Thread.sleep(2000);
							break;
						}
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/bi")), Inputdata[i][89].toString().trim());
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
					}
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/nif2")), Inputdata[i][88].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/providedCVP")), Inputdata[i][90].toString().trim());
					
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")), Inputdata[i][51].toString().trim());
					log("enter OCN:"+Inputdata[i][51].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/localAreacodefor2")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
					Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							Select(getwebelement(xml.getlocator("//locators/localAreacodefor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}

					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					implicitwait(5);
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/DSUyes2")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
//						Select(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
//						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
					}
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				case "SPAIN":
				{
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++)
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on A3A and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/A3A").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/A3A").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on A3A");
							Thread.sleep(2000);
							break;
						}
					}
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/cifNifNumber")),Inputdata[i][88].toString().trim());
						implicitwait(5);
						
						
						SendKeys(getwebelement(xml.getlocator("//locators/streetNumber")),Inputdata[i][103].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/Streettype")), Inputdata[i][104].toString().trim());
						implicitwait(5);
						Thread.sleep(2000);
						SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
						implicitwait(5);
						//Streettype
						Thread.sleep(2000);
						SendKeys(getwebelement(xml.getlocator("//locators/provinces2")),Inputdata[i][25].toString());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
						implicitwait(5);
						//**********************************
						//Now for new Address details fileds
						Thread.sleep(2000);
						if(Inputdata[i][113].toString().isEmpty())
						{
							log("Dont need new address details");
						}
						else
						{
						SendKeys(getwebelement(xml.getlocator("//locators/StreetNumberPortIn")),Inputdata[i][278].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/StreetTypePortIn")), Inputdata[i][279].toString().trim());
						implicitwait(5);
						Thread.sleep(2000);
						SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/NewProviencePortIn")),Inputdata[i][277].toString());
						implicitwait(5);
						Select(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
						implicitwait(5);
						}
						//**********************************
//						WaitforElementtobeclickable(xml.getlocator("//locators/validbutton2"));
//						Clickon(getwebelement(xml.getlocator("//locators/validbutton2")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
//						Thread.sleep(3000);
////						Set<String> handles = driver.getWindowHandles();
////						Iterator<String> iterator = handles.iterator();
////						String parent = iterator.next();
////						String curent = iterator.next();
////						System.out.println("Window handel" + curent);
////						driver.switchTo().window(curent);
//						 String parentWinHandle = driver.getWindowHandle();
//							Set<String> totalopenwindow=driver.getWindowHandles();
//							if(totalopenwindow.size()>1) 
//							{
//							for(String handle: totalopenwindow)
//							{
//					            if(!handle.equals(parentWinHandle))
//					            {
//					            driver.switchTo().window(handle);
//					            
//					            }
//							}
//							}
//						if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
//						{
//							RedLog("Excel sheet data are incorrect");
//						}
//						else
//						{//
//							Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
//							Thread.sleep(2000);
//							try {
//								safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
//							} catch (Exception e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//							//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
//						}
//						Thread.sleep(2000);
////						driver.close();
////						Thread.sleep(2000);
//						driver.switchTo().window(parentWinHandle);
//						
						int lent=Integer.parseInt((String) Inputdata[i][160]);
						SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
						implicitwait(5);
						Select(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
						implicitwait(5);
						Select(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString());
						implicitwait(5);
						if(lent>1) 	
						{
						Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
						//main billing Number
						SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor22")), Inputdata[i][161].toString());
						implicitwait(5);
						//local are code 
						Select(getwebelement(xml.getlocator("//locators/localAreacodefor22")), Inputdata[i][162].toString());
						implicitwait(5);
						//Main Number
						SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor22")), Inputdata[i][163].toString());
						implicitwait(5);
						//Range Start
						SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor22")), Inputdata[i][164].toString().trim());
						implicitwait(5);
						//Range End
						SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor22")), Inputdata[i][165].toString().trim());
						implicitwait(5);
						//current Provider
						Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor22")),Inputdata[i][166].toString().trim());
						implicitwait(5);
						//167
						if(lent > 2 && lent < 21)
						{
							int afterSecond=lent-2;
							int count=167;
							for(int total=0;total<afterSecond;total++)
							{
								Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
								Thread.sleep(2000);
								SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//local are code 
								Select(getwebelement(xml.getlocator("//locators/localAreacodefor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//Main Number
								SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//Range Start
								SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//Range End
								/*count=171*/
								SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
								//current Provider
								/*count=172*/
								Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
								implicitwait(5);
								count++;
							}
						}
					}

						javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
						implicitwait(5);
						if(isElementPresent((xml.getlocator("//locators/window"))))
						{
						Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
						implicitwait(5);
						}
						Clickon(getwebelement(xml.getlocator("//locators/invoice")));
						Thread.sleep(5000);
						System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
						StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
						Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
				     implicitwait(5);
				     Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
				     Thread.sleep(5000);
				     System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss1 = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss1, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot1 = new Robot();
				     robot1.keyPress(KeyEvent.VK_ENTER);
				     robot1.keyRelease(KeyEvent.VK_ENTER);
				     robot1.keyPress(KeyEvent.VK_CONTROL);
				     robot1.keyPress(KeyEvent.VK_V);
				     robot1.keyRelease(KeyEvent.VK_V);
				     robot1.keyRelease(KeyEvent.VK_CONTROL);
				     robot1.keyPress(KeyEvent.VK_ENTER);
				     robot1.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
				     implicitwait(5);
				     if(Inputdata[i][105].toString().equals("List Number Options "))
						{
							Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
							if(Inputdata[i][106].toString().equals("Yes"))
							{
								Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
							}
							if(Inputdata[i][107].toString().equals("Yes"))
							{
								Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
								ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
							}
						}
						else
						{
							Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
						}
				     	Thread.sleep(2000);
				     	WaitforElementtobeclickable(xml.getlocator("//locators/validbutton2"));
						Clickon(getwebelement(xml.getlocator("//locators/validbutton2")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
						Thread.sleep(3000);
//						Set<String> handles = driver.getWindowHandles();
//						Iterator<String> iterator = handles.iterator();
//						String parent = iterator.next();
//						String curent = iterator.next();
//						System.out.println("Window handel" + curent);
//						driver.switchTo().window(curent);
						 String parentWinHandle = driver.getWindowHandle();
							Set<String> totalopenwindow=driver.getWindowHandles();
							if(totalopenwindow.size()>1) 
							{
							for(String handle: totalopenwindow)
							{
					            if(!handle.equals(parentWinHandle))
					            {
					            driver.switchTo().window(handle);
					            
					            }
							}
							}
						if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
						{
							RedLog("Excel sheet data are incorrect");
						}
						else
						{//
							Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
							Thread.sleep(2000);
							try {
								safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
						}
						Thread.sleep(2000);
//						driver.close();
//						Thread.sleep(2000);
						driver.switchTo().window(parentWinHandle);
						
				     Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
				     Thread.sleep(2000);
				     implicitwait(5);
				     log("Fill all the filled and click on the search button");	
				     break;
				}
				case "GERMANY":
				{
					//A2G
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0)
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on A2G and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/A2G").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/A2G").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on A2G");
							Thread.sleep(2000);
							break;
						}
					}
//					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")), Inputdata[i][51].toString().trim());
//					log("enter OCN:"+Inputdata[i][51].toString());
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameportin")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameportin")), Inputdata[i][83].toString().trim());
						implicitwait(5);
						javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth")));
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					}
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
					//sanajana code
					WaitforElementtobeclickable(xml.getlocator("//locators/SecondValidateBtn"));
					Clickon(getwebelement(xml.getlocator("//locators/SecondValidateBtn")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
					
					Thread.sleep(10000);
//					Set<String> handles = driver.getWindowHandles();
//					Iterator<String> iterator = handles.iterator();
//					String parent = iterator.next();
//					String curent = iterator.next();
//					System.out.println("Window handel" + curent);
//					driver.switchTo().window(curent);
					 String parentWinHandle = driver.getWindowHandle();
						Set<String> totalopenwindow=driver.getWindowHandles();
						if(totalopenwindow.size()>1) 
						{
						for(String handle: totalopenwindow)
						{
				            if(!handle.equals(parentWinHandle))
				            {
				            driver.switchTo().window(handle);
				            
				            }
						}
						}
					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
					{
						RedLog("Excel sheet data are incorrect");
					}
					else
					{//
						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
						Thread.sleep(2000);
						try 
						{
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						} 
						catch (Exception e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
					}
					Thread.sleep(2000);
					driver.switchTo().window(parentWinHandle);
					Thread.sleep(3000);
				
					//sanjana code end
					Select(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					//Select(getwebelement(xml.getlocator("//locators/nonGeoLocalAreacode")), Inputdata[i][].toString());
					implicitwait(5);
					Thread.sleep(2000);
					//Object lent=Inputdata[i][160];
					//
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					System.out.println(lent);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
//					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
	//				Select(getwebelement(xml.getlocator("//locators/localAreacodefor2")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
		//			SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
			//		Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
//							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
//							Select(getwebelement(xml.getlocator("//locators/localAreacodefor2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
//							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
//							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}
					Thread.sleep(5000);
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					log("date is :"+Inputdata[i][34].toString());
					implicitwait(5);
					Thread.sleep(3000);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					log("window :"+Inputdata[i][138].toString());
					implicitwait(5);
					}
					Thread.sleep(3000);
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     }
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					Thread.sleep(5000);
					if(Inputdata[i][101].toString().equals("Yes"))
					{ 
						Clickon(getwebelement(xml.getlocator("//locators/DSUagain")));
						log("click on the Directory service update check box");
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						Thread.sleep(3000);
					}
					Thread.sleep(2000);
//					WaitforElementtobeclickable(xml.getlocator("//locators/SecondValidateBtn"));
//					Clickon(getwebelement(xml.getlocator("//locators/SecondValidateBtn")));
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
//					Thread.sleep(3000);
////					Set<String> handles = driver.getWindowHandles();
////					Iterator<String> iterator = handles.iterator();
////					String parent = iterator.next();
////					String curent = iterator.next();
////					System.out.println("Window handel" + curent);
////					driver.switchTo().window(curent);
//					 String parentWinHandle = driver.getWindowHandle();
//						Set<String> totalopenwindow=driver.getWindowHandles();
//						if(totalopenwindow.size()>1) 
//						{
//						for(String handle: totalopenwindow)
//						{
//				            if(!handle.equals(parentWinHandle))
//				            {
//				            driver.switchTo().window(handle);
//				            
//				            }
//						}
//						}
//					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
//					{
//						RedLog("Excel sheet data are incorrect");
//					}
//					else
//					{//
//						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
//						Thread.sleep(2000);
//						try 
//						{
//							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
//						} 
//						catch (Exception e) 
//						{
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
//					}
//					Thread.sleep(2000);
//					driver.switchTo().window(parentWinHandle);
					Thread.sleep(3000);
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;	
				}
				case "FRANCE":
				{
					//A29
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on A29 and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/A29").replace("serviceprofile", Inputdata[i][3].toString())));
							//System.out.println(getwebelement(xml.getlocator("//locators/A29").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on A29");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/portinfrancedep")),Inputdata[i][92].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the department number Field: " + Inputdata[i][92].toString());
					Select(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString());
					implicitwait(5);
					log("City is:-"+Inputdata[i][11].toString());
					Select(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					log("postcode is:-"+Inputdata[i][12].toString());
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/DepartmentPortIn")),Inputdata[i][280].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the department number Field: " + Inputdata[i][92].toString());
					
					Select(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					log("Main Billing numberis :-"+Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					Thread.sleep(2000);
					Clickon(getwebelement(xml.getlocator("//locators/MainNumber")));
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor4")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					Clickon(getwebelement(xml.getlocator("//locators/MainNumberFor2")));
					//SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
					Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor5").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							Clickon(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))));
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
						Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					}

					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					Thread.sleep(2000);
					SendKeys(getwebelement(xml.getlocator("//locators/siretNumber")), Inputdata[i][93].toString().trim());
					implicitwait(5);
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     }
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					implicitwait(5);
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/DSUagain")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						Select(getwebelement(xml.getlocator("//locators/numberStatusDepartment")), Inputdata[i][130].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Department for DSU: " + Inputdata[i][130].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationNumber")), Inputdata[i][131].toString());
						log("Step: fill the Comapny Registration Number:-"+Inputdata[i][131].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivationEmailID")), Inputdata[i][85].toString());
						log("Step: fill the Comapny ActivationEmailID:-"+Inputdata[i][85].toString());	
					}
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				case "BELGIUM":
				{
				//A92
					
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on A2G and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/A2G").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on button");
							Thread.sleep(2000);
							break;
						}
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameportin")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameportin")), Inputdata[i][83].toString().trim());
						implicitwait(5);
//						SendKeys(getwebelement(xml.getlocator("//locators/customerTitle")), Inputdata[i][94].toString().trim());
//						implicitwait(5);
//						javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth")));
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString());
						implicitwait(5);
//						SendKeys(getwebelement(xml.getlocator("//locators/registeredName")),Inputdata[i][95].toString());
//						implicitwait(5);
//						SendKeys(getwebelement(xml.getlocator("//locators/vatnumber")),Inputdata[i][88].toString());
					}
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/customerlanguage")), Inputdata[i][96].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					
					SendKeys(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					Thread.sleep(2000);
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/localAreacodefor2")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
					Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							Select(getwebelement(xml.getlocator("//locators/localAreacodefor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}

					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
						Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					}
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					
					
					//last update 00001 - june  - 2020
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
				     //****************
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/DSUagain")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						Select(getwebelement(xml.getlocator("//locators/whitepages")),Inputdata[i][123].toString());
						log("Selct the white pages:-"+Inputdata[i][123]);
						Select(getwebelement(xml.getlocator("//locators/ElectronicMedia")),Inputdata[i][124].toString());
						log("Selct the Electronic Media:-"+Inputdata[i][124]);
						Select(getwebelement(xml.getlocator("//locators/ActivationDirectoryExsistence")),Inputdata[i][125].toString());
						log("Selct the directory exsistence:-"+Inputdata[i][125]);
						Select(getwebelement(xml.getlocator("//locators/ActivationExcportSales")),Inputdata[i][126].toString());
						log("Selct the export sales:-"+Inputdata[i][126]);
						Select(getwebelement(xml.getlocator("//locators/ActivationReserve")),Inputdata[i][127].toString());
						log("Selct the Reserve Query:-"+Inputdata[i][127]);
						Select(getwebelement(xml.getlocator("//locators/Activationservicetype2")),Inputdata[i][137].toString());
						log("Selct the Activation service type 2:-"+Inputdata[i][137]);
						Select(getwebelement(xml.getlocator("//locators/ActivationDeviceType")),Inputdata[i][128].toString());
						log("Selct the Activation DeviceType:-"+Inputdata[i][128]);
						Select(getwebelement(xml.getlocator("//locators/ActivaitonLanguage")),Inputdata[i][129].toString());
						log("Selct the Activation language:-"+Inputdata[i][129]);	
					}
//					SendKeys(getwebelement(xml.getlocator("//locators/AddressExtension")), Inputdata[i][].toString());
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the AddressExtension: " + Inputdata[i][].toString());
					Select(getwebelement(xml.getlocator("//locators/DirectroyListingOptions")),Inputdata[i][109].toString());
					log("Selct the Directroy Listing Options:-"+Inputdata[i][109]);
					implicitwait(5);	
					Thread.sleep(2000);
					WaitforElementtobeclickable(xml.getlocator("//locators/SecondValidateBtn"));
					Clickon(getwebelement(xml.getlocator("//locators/SecondValidateBtn")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
					Thread.sleep(3000);
//					Set<String> handles = driver.getWindowHandles();
//					Iterator<String> iterator = handles.iterator();
//					String parent = iterator.next();
//					String curent = iterator.next();
//					System.out.println("Window handel" + curent);
//					driver.switchTo().window(curent);
					 String parentWinHandle = driver.getWindowHandle();
						Set<String> totalopenwindow=driver.getWindowHandles();
						if(totalopenwindow.size()>1) 
						{
						for(String handle: totalopenwindow)
						{
				            if(!handle.equals(parentWinHandle))
				            {
				            driver.switchTo().window(handle);
				            }
						}
						}
					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
					{
						RedLog("Excel sheet data are incorrect");
					}
					else
					{//
						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
						Thread.sleep(2000);
						try 
						{
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						} 
						catch (Exception e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
					}
					Thread.sleep(2000);
//					driver.close();
//					Thread.sleep(2000);
					driver.switchTo().window(parentWinHandle);
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				} 
				case "DENMARK": 
				{
					//BFC
					WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
					Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
					log("click on managing porting");
					Thread.sleep(1000);
					Clickon(getwebelement(xml.getlocator("//locators/RequestPortin")));
					implicitwait(10);
					log("click on request portin");
					int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
					System.out.println(allData);
					for (int k = 0; k <= allData; k++) 
					{
						if (k != 0 && k % 10 == 0) 
						{
							Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
							log("click on BFC and next button");
						}
						Thread.sleep(1000);
						String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
						System.out.println(data);
						Thread.sleep(1000);
						if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
						{
							Thread.sleep(2000);
							//BFC
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/BFC").replace("serviceprofile", Inputdata[i][3].toString())));
							System.out.println(getwebelement(xml.getlocator("//locators/BFC").replace("serviceprofile", Inputdata[i][3].toString())));
							log("click on BFC");
							// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
							Thread.sleep(2000);
							break;
						}
					}
					SendKeys(getwebelement(xml.getlocator("//locators/customernamepost")),Inputdata[i][7].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber2")),Inputdata[i][9].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Street2")), Inputdata[i][10].toString().trim());
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/city2")), Inputdata[i][11].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/postcode2")), Inputdata[i][12].toString());
					implicitwait(5);
					//**********************************
					//Now for new Address details fileds
					Thread.sleep(2000);
					if(Inputdata[i][112].toString().isEmpty())
					{
						log("Dont need new address details");
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/PortInNewBuildingName")),Inputdata[i][112].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/Newstreetportin")), Inputdata[i][113].toString().trim());
					implicitwait(5);
					
					Select(getwebelement(xml.getlocator("//locators/selectcityPortIn")), Inputdata[i][114].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/NewPostcodePortIn")), Inputdata[i][115].toString());
					implicitwait(5);
					}
					//**********************************
					int lent=Integer.parseInt((String) Inputdata[i][160]);
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumber")), Inputdata[i][32].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")), Inputdata[i][5].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumber")), Inputdata[i][13].toString());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")), Inputdata[i][14].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/CurrentProvider")),Inputdata[i][33].toString().trim());
					implicitwait(5);
					if(lent>1) 	
					{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					//main billing Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor2")), Inputdata[i][161].toString());
					implicitwait(5);
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor4")), Inputdata[i][162].toString());
					implicitwait(5);
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor2")), Inputdata[i][163].toString());
					implicitwait(5);
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor2")), Inputdata[i][164].toString().trim());
					implicitwait(5);
					//Range End
					SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor2")), Inputdata[i][165].toString().trim());
					implicitwait(5);
					//current Provider
					Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor2")),Inputdata[i][166].toString().trim());
					implicitwait(5);
					//167
					if(lent > 2 && lent < 21)
					{
						int afterSecond=lent-2;
						int count=167;
						for(int total=0;total<afterSecond;total++)
						{
							Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
							Thread.sleep(2000);
							SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//local are code 
							SendKeys(getwebelement(xml.getlocator("//locators/localAreacodefor5").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Main Number
							SendKeys(getwebelement(xml.getlocator("//locators/MainNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range Start
							SendKeys(getwebelement(xml.getlocator("//locators/RangeFromfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//Range End
							/*count=171*/
							SendKeys(getwebelement(xml.getlocator("//locators/RangeToFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
							//current Provider
							/*count=172*/
							Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
							implicitwait(5);
							count++;
						}
					}
				}

					//last update 01 june 2020
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
						Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					}
					javaexecutotSendKeys(Inputdata[i][34].toString(),getwebelement(xml.getlocator("//locators/portindate")));
					implicitwait(5);
					if(isElementPresent((xml.getlocator("//locators/window"))))
					{
					Select(getwebelement(xml.getlocator("//locators/window")), Inputdata[i][138].toString());
					implicitwait(5);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/subscriberID")), Inputdata[i][97].toString().trim());
					implicitwait(5);
					
					
					//last update 01 - june - 2020
					SendKeys(getwebelement(xml.getlocator("//locators/firstnameportin")), Inputdata[i][82].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/lastnameportin")), Inputdata[i][83].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/phonenumber")), Inputdata[i][84].toString().trim());
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/emailidportin")), Inputdata[i][85].toString().trim());
					implicitwait(5);
					
					
					
					
					Clickon(getwebelement(xml.getlocator("//locators/Choosefile")));
					Thread.sleep(5000);
					System.out.println(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     StringSelection ss = new StringSelection(System.getProperty("user.dir")+"\\doc file\\fasdfasd.docx");
				     Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				     //imitate mouse events like ENTER, CTRL+C, CTRL+V
				     Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_V);
				     robot.keyRelease(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_ENTER);
				     robot.keyRelease(KeyEvent.VK_ENTER);  
				     try 
				     {
						Thread.sleep(3000);
				     } 
				     catch (InterruptedException e) 
				     {
						// TODO Auto-generated catch block
						e.printStackTrace();
				     }
					implicitwait(5);
					Clickon(getwebelement(xml.getlocator("//locators/searchportin")));
					Thread.sleep(2000);
					implicitwait(5);
					log("Fill all the filled and click on the search button");
					break;
				}
				default:
				{
					RedLog("please select the valid country");
					break;
				}
				}
				//changes-start
				Thread.sleep(2000);
				String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/portinTransactionlink")));
				TransactionId.set(Transactionresult);
				log("Transaction id received");
				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequest")));
				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequest")));
				// =========================================================================
				log("click on the update part in request");
				waitandForElementDisplayed(xml.getlocator("//locators/statusQueryTransactionID"));
				log(TransactionId.get().toString().trim().toLowerCase());
				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),TransactionId.get().toString().trim().toLowerCase());
				Thread.sleep(3000);
				System.out.println(TransactionId.get());
				log("send the transaction ID: " + TransactionId.get());
				// =========================================================================
				Thread.sleep(10000);
				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
				Thread.sleep(3000);
				log("click on the search Button");
				String CurrentStatus1 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
				CurrentStatus.set(CurrentStatus1);
				log("CurrentStatus received");
				log("check the current status");
				if (CurrentStatus.get().contains("Validation In Progress")) 
				{
					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
					log("click on the checkbox");
					implicitwait(5);
					SendKeys(getwebelement(xml.getlocator("//locators/ActionPortin")), "Submitted to operator");
					log("select the valid action portin");
					Thread.sleep(5000);
					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
					log("click on the go button");
				}
				else 
				{
					RedLog("CurrentStatus is incorrect");
					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
				}
				Thread.sleep(3000);
				if(Inputdata[i][2].toString().equalsIgnoreCase("GERMANY"))
				{
					Thread.sleep(3000);
					Clear(getwebelement(xml.getlocator("//locators/portInCurrentfend")));
					Thread.sleep(3000);
					SendKeys(getwebelement(xml.getlocator("//locators/portInCurrentfend")), Inputdata[i][281].toString());
					log("Current Porvider:"+Inputdata[i][281].toString());
				}
				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
				log("click on the submit button");
				Thread.sleep(2000);
				AcceptJavaScriptMethod();
				log("submitted succesfully");
				Thread.sleep(10000);
				boolean flag = false;
				do 
				{
					flag = isElementPresent(xml.getlocator("//locators/verifystatus2"));
					
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: ChildCurrentStatus is : " + ChildCurrentStat.get());
					log("if child Current Status shown than go to the next step button otherwise click on the refresh button");
					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
				} 
				while (!flag == true);
				log("childcurrent status id successfully submited");
				Thread.sleep(5000);
				String ChildCurrentStatus = Gettext(getwebelement(xml.getlocator("//locators/ChildStatus")));
				ChildCurrentStat.set(ChildCurrentStatus);
				log("get the child status: " + ChildCurrentStat.get());
				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
				String ParentCurrentStatus = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
				ParentCurrentStat.set(ParentCurrentStatus);
				log("ParentCurrentStatus shown");
				// =========================================================================
				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
				log("now again check the parent status");
				Thread.sleep(1000);
				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
				log("click on the update port in request");
				// =========================================================================
				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
				Thread.sleep(5000);
				log("send the transaction ID: " + ParentCurrentStat.get());
				// =========================================================================
				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
				log("click on the search button");
				Thread.sleep(3000);
				String CurrentStatus2 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
				CurrentStatus.set(CurrentStatus2);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
				log("CurrentStatus received");
				log("check the current status");
				if (CurrentStatus.get().contains("Submitted to operator")) 
				{
					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
					log("click on the checkbox");
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/ActionPortin")), "Firm order commitment");
					Thread.sleep(15000);
					log("select the valid action portin");
					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
					log("click onthe go button");
				}
				else 
				{
					RedLog("CurrentStatus is incorrect");
					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
				}
				// =========================================================================
				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
				log("click on the submit button");
				implicitwait(5);
				Thread.sleep(4000);
				AcceptJavaScriptMethod();
				log("submitted succesfully");
				Thread.sleep(10000);
				boolean flag1 = false;
				do 
				{
					flag1 = isElementPresent(xml.getlocator("//locators/verifystatus2"));
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: ChildCurrentStatus is : " + ChildCurrentStat.get());
					log("if child Current Status shown than jump on the next step otherwise click on the refresh button");
					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
				} 
				while (flag1 == false);
				log("childcurrent status id successfully submited");
				Thread.sleep(1000);
				// =========================================================================
				log("get the child status: " + ChildCurrentStat.get());
				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
				String ParentCurrentStatus1 = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
				ParentCurrentStat.set(ParentCurrentStatus1);
				log("ParentCurrentStatus shown: " + ParentCurrentStat.get());
				// =========================================================================
				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
				log("now again check the parent status");
				Thread.sleep(1000);
				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
				log("click on the update port in request");
				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
				Thread.sleep(5000);
				log("send the transaction ID: " + ParentCurrentStat.get());
				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
				log("click on the search button");
				Thread.sleep(3000);
				String CurrentStatus3 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
				CurrentStatus.set(CurrentStatus3);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
				log("CurrentStatus received");
				log("check the current status");
				if (CurrentStatus.get().contains("Firm order commitment")) 
				{
					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
					log("click on the checkbox");
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/ActionPortin")), "Activate Port");
					Thread.sleep(10000);
					log("select the valid action portin");
					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
					log("click onthe go button");
				} 
				else 
				{
					RedLog("CurrentStatus is incorrect");
					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
				}
				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
				log("click on the submit button");
				Thread.sleep(4000);
				implicitwait(5);
				AcceptJavaScriptMethod();
				log("submitted succesfully");
				Thread.sleep(10000);
//				if(isElementPresent(xml.getlocator("//locators/errorportdatesubmit")))
//				{
//					RedLog("error data is inocrrect");
//				}
				boolean flag2 = false;
				Thread.sleep(10000);
				String date3 = null;
				DateFormat dateFormat = new SimpleDateFormat("HH:mm");
				Date dd = new Date(System.currentTimeMillis() + 20 * 60 * 1000);
				String date2 = dateFormat.format(dd);
				do 
				{
					Thread.sleep(2000);
					Pagerefresh();
					Pagerefresh();
					flag2 = isElementPresent(xml.getlocator("//locators/ChildStatusmain"));
					log(date3);
					log("if child Current Status shown than jump to the next step otherwise click on the refresh button");
					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					date3 = dateFormat.format(new Date(System.currentTimeMillis()));
					System.out.println(date3);
					System.out.println(date2);
					Thread.sleep(1000);
					Pagerefresh();
					if (date3.trim().equals(date2.trim())) 
					{
						System.out.println("System is waiting for 20 minutes to change status, But still status in not changed!");
						Assert.fail("System is taking longer time to change Status from InProgress to Complted!");
						break;
					}
				}
				while (flag2 == false);
				log("childcurrent status id successfully submited");
				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
				String ParentCurrentStatus12 = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
				ParentCurrentStat.set(ParentCurrentStatus12);
				log("ParentCurrentStatus shown: " + ParentCurrentStat.get());
				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
				log("now again check the parent status");
				Thread.sleep(1000);
				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
				log("click on the update port in request");
				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
				log("send the transaction ID: " + ParentCurrentStat.get());
				Thread.sleep(5000);
				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
				log("click on the search button");
				Thread.sleep(3000);
				String CurrentStatus4 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
				CurrentStatus.set(CurrentStatus4);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
				log("CurrentStatus received");
				log("check the current status");
				if (CurrentStatus.get().contains("Porting initiated")) 
				{
					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
					log("Click on the checkbox button");
					implicitwait(5);
					Select(getwebelement(xml.getlocator("//locators/ActionPortin")), "Test successful");
					Thread.sleep(10000);
					log("test successful");
					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
					log("click on the go button");
				} 
				else 
				{
					RedLog("CurrentStatus is incorrect");
					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
				}
				// ----------------------------------
				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
				log("click on the submit button");
				Thread.sleep(2000);
				AcceptJavaScriptMethod();
				log("submitted succesfully");
				Thread.sleep(10000);
				boolean flag3 = false;
				DateFormat dateFormat2 = new SimpleDateFormat("HH:mm");
				String date1234 = dateFormat2.format(new Date(System.currentTimeMillis()));
				Date dd1 = new Date(System.currentTimeMillis() + 20 * 60 * 1000);
				String date234 = dateFormat.format(dd1);
				do 
				{
					Thread.sleep(2000);
					Pagerefresh();
					Pagerefresh();
					flag3 = isElementPresent(xml.getlocator("//locators/verifystatus2"));
					//log(date3);
					log("if child Current Status shown than jump to the next step otherwise click on the refresh button");
					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					//date3 = dateFormat.format(new Date(System.currentTimeMillis()));
					System.out.println(date1234);
					System.out.println(date234);
					Thread.sleep(1000);
					Pagerefresh();
					if (date1234.trim().equals(date234.trim())) 
					{
						System.out.println(
								"System is waiting for 20 minutes to change status, But still status in not changed!");
						Assert.fail("System is taking longer time to change Status from InProgress to Complted!");
						break;
					}
				}
				while (flag3 == false);
				Thread.sleep(5000);
				log("childcurrent status id successfully submited");
				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
				String ParentCurrentStatus13 = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
				ParentCurrentStat.set(ParentCurrentStatus13);
				log("ParentCurrentStatus shown: " + ParentCurrentStat.get());
				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
				log("now again check the parent status");
				Thread.sleep(1000);
				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
				log("click on the update port in request");
				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
				Thread.sleep(5000);
				log("send the transaction ID: " + ParentCurrentStat.get());
				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
				log("click on the search button");
				Thread.sleep(3000);
				String CurrentStatus5 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
				CurrentStatus.set(CurrentStatus5);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
				log("CurrentStatus received");
				Assert.assertTrue(CurrentStatus.get().contains("Completed"), "last step unresponsivesss");
				log("Port In scenerion is completed successfully!!");
//				//changes-end
				
				
				
				
				
				
				
				
				
				//chnages 2
//				Thread.sleep(2000);
//				String Transactionresult = Gettext(getwebelement(xml.getlocator("//locators/portinTransactionlink")));
//				TransactionId.set(Transactionresult);
//				log("Transaction id received");
//				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequest")));
//				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequest")));
//				// =========================================================================
//				log("click on the update part in request");
//				waitandForElementDisplayed(xml.getlocator("//locators/statusQueryTransactionID"));
//				log(TransactionId.get().toString().trim().toLowerCase());
//				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),TransactionId.get().toString().trim().toLowerCase());
//				Thread.sleep(3000);
//				System.out.println(TransactionId.get());
//				log("send the transaction ID: " + TransactionId.get());
//				// =========================================================================
//				Thread.sleep(10000);
//				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
//				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
//				Thread.sleep(3000);
//				log("click on the search Button");
////				String CurrentStatus1 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
////				CurrentStatus.set(CurrentStatus1);
//				log("CurrentStatus received");
//				log("check the current status");
////				if (CurrentStatus.get().contains("Validation In Progress")) 
////				{
////					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
////					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
//					log("click on the checkbox");
////					implicitwait(5);
////					SendKeys(getwebelement(xml.getlocator("//locators/ActionPortin")), "Submitted to operator");
//					log("select the valid action portin");
////					Thread.sleep(5000);
////					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
////					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
//					log("click on the go button");
////				}
////				else 
////				{
////					RedLog("CurrentStatus is incorrect");
////					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
////				}
////				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//				log("click on the submit button");
////				Thread.sleep(2000);
////				AcceptJavaScriptMethod();
//				log("submitted succesfully");
////				Thread.sleep(10000);
////				boolean flag = false;
////				do 
////				{
////					flag = isElementPresent(xml.getlocator("//locators/verifystatus2"));
////					
////					ExtentTestManager.getTest().log(LogStatus.PASS," Step: ChildCurrentStatus is : " + ChildCurrentStat.get());
//					log("if child Current Status shown than go to the next step button otherwise click on the refresh button");
////					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
////				} 
////				while (!flag == true);
//				log("childcurrent status id successfully submited");
////				Thread.sleep(5000);
////				String ChildCurrentStatus = Gettext(getwebelement(xml.getlocator("//locators/ChildStatus")));
////				ChildCurrentStat.set(ChildCurrentStatus);
////				log("get the child status: " + ChildCurrentStat.get());
////				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
////				String ParentCurrentStatus = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
////				ParentCurrentStat.set(ParentCurrentStatus);
//				log("ParentCurrentStatus shown");
//				// =========================================================================
////				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
////				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
//				log("now again check the parent status");
////				Thread.sleep(1000);
////				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
////				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
//				log("click on the update port in request");
//				// =========================================================================
////				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
////				Thread.sleep(5000);
////				log("send the transaction ID: " + ParentCurrentStat.get());
////				// =========================================================================
////				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
////				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
//				log("click on the search button");
////				Thread.sleep(3000);
////				String CurrentStatus2 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
////				CurrentStatus.set(CurrentStatus2);
////				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
//				log("CurrentStatus received");
//				log("check the current status");
////				if (CurrentStatus.get().contains("Submitted to operator")) 
////				{
////					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
////					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
//					log("click on the checkbox");
////					implicitwait(5);
////					Select(getwebelement(xml.getlocator("//locators/ActionPortin")), "Firm order commitment");
////					Thread.sleep(15000);
//					log("select the valid action portin");
////					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
////					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
//					log("click onthe go button");
////				}
////				else 
////				{
////					RedLog("CurrentStatus is incorrect");
////					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
////				}
//				// =========================================================================
////				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//				log("click on the submit button");
////				implicitwait(5);
////				Thread.sleep(4000);
////				AcceptJavaScriptMethod();
//				log("submitted succesfully");
////				Thread.sleep(10000);
////				boolean flag1 = false;
////				do 
////				{
////					flag1 = isElementPresent(xml.getlocator("//locators/verifystatus2"));
////					ExtentTestManager.getTest().log(LogStatus.PASS," Step: ChildCurrentStatus is : " + ChildCurrentStat.get());
//					log("if child Current Status shown than jump on the next step otherwise click on the refresh button");
////					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
////				} 
////				while (flag1 == false);
//				log("childcurrent status id successfully submited");
//				Thread.sleep(1000);
//				// =========================================================================
////				log("get the child status: " + ChildCurrentStat.get());
////				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
////				String ParentCurrentStatus1 = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
////				ParentCurrentStat.set(ParentCurrentStatus1);
////				log("ParentCurrentStatus shown: " + ParentCurrentStat.get());
//				// =========================================================================
////				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
////				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
//				log("now again check the parent status");
////				Thread.sleep(1000);
////				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
////				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
//				log("click on the update port in request");
////				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
////				Thread.sleep(5000);
////				log("send the transaction ID: " + ParentCurrentStat.get());
////				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
////				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
//				log("click on the search button");
////				Thread.sleep(3000);
////				String CurrentStatus3 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
////				CurrentStatus.set(CurrentStatus3);
////				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
//				log("CurrentStatus received");
//				log("check the current status");
////				if (CurrentStatus.get().contains("Firm order commitment")) 
////				{
////					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
////					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
//					log("click on the checkbox");
////					implicitwait(5);
////					Select(getwebelement(xml.getlocator("//locators/ActionPortin")), "Activate Port");
////					Thread.sleep(10000);
//					log("select the valid action portin");
////					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
////					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
//					log("click onthe go button");
////				} 
////				else 
////				{
////					RedLog("CurrentStatus is incorrect");
////					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
////				}
////				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//				log("click on the submit button");
////				Thread.sleep(4000);
////				implicitwait(5);
////				AcceptJavaScriptMethod();
//				log("submitted succesfully");
////				Thread.sleep(10000);
////				if(isElementPresent(xml.getlocator("//locators/errorportdatesubmit")))
////				{
////					RedLog("error data is inocrrect");
////				}
////				boolean flag2 = false;
////				Thread.sleep(10000);
////				String date3 = null;
////				DateFormat dateFormat = new SimpleDateFormat("HH:mm");
////				Date dd = new Date(System.currentTimeMillis() + 20 * 60 * 1000);
////				String date2 = dateFormat.format(dd);
////				do 
////				{
////					Thread.sleep(2000);
////					Pagerefresh();
////					Pagerefresh();
////					flag2 = isElementPresent(xml.getlocator("//locators/ChildStatusmain"));
////					log(date3);
//					log("if child Current Status shown than jump to the next step otherwise click on the refresh button");
////					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
////					date3 = dateFormat.format(new Date(System.currentTimeMillis()));
////					System.out.println(date3);
////					System.out.println(date2);
////					Thread.sleep(1000);
////					Pagerefresh();
////					if (date3.trim().equals(date2.trim())) 
////					{
////						System.out.println("System is waiting for 20 minutes to change status, But still status in not changed!");
////						Assert.fail("System is taking longer time to change Status from InProgress to Complted!");
////						break;
////					}
////				}
////				while (flag2 == false);
//				log("childcurrent status id successfully submited");
////				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
////				String ParentCurrentStatus12 = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
////				ParentCurrentStat.set(ParentCurrentStatus12);
////				log("ParentCurrentStatus shown: " + ParentCurrentStat.get());
////				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
////				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
//				log("now again check the parent status");
////				Thread.sleep(1000);
////				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
////				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
//				log("click on the update port in request");
////				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
////				log("send the transaction ID: " + ParentCurrentStat.get());
////				Thread.sleep(5000);
////				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
////				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
//				log("click on the search button");
////				Thread.sleep(3000);
////				String CurrentStatus4 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
////				CurrentStatus.set(CurrentStatus4);
////				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
//				log("CurrentStatus received");
//				log("check the current status");
////				if (CurrentStatus.get().contains("Porting initiated")) 
////				{
////					WaitforElementtobeclickable((xml.getlocator("//locators/checkbox")));
////					Clickon(getwebelement(xml.getlocator("//locators/checkbox")));
//					log("Click on the checkbox button");
////					implicitwait(5);
////					Select(getwebelement(xml.getlocator("//locators/ActionPortin")), "Test successful");
////					Thread.sleep(10000);
//					log("test successful");
////					WaitforElementtobeclickable((xml.getlocator("//locators/GoButon")));
////					Clickon(getwebelement(xml.getlocator("//locators/GoButon")));
//					log("click on the go button");
////				} 
////				else 
////				{
////					RedLog("CurrentStatus is incorrect");
////					Assert.assertTrue(CurrentStatus.get().contains("Validation In Progress"),"Current status is not valid");
////				}
//				// ----------------------------------
////				WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////				Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//				log("click on the submit button");
////				Thread.sleep(2000);
////				AcceptJavaScriptMethod();
//				log("submitted succesfully");
////				Thread.sleep(10000);
////				boolean flag3 = false;
////				DateFormat dateFormat2 = new SimpleDateFormat("HH:mm");
////				String date1234 = dateFormat2.format(new Date(System.currentTimeMillis()));
////				Date dd1 = new Date(System.currentTimeMillis() + 20 * 60 * 1000);
////				String date234 = dateFormat.format(dd1);
////				do 
////				{
////					Thread.sleep(2000);
////					Pagerefresh();
////					Pagerefresh();
////					flag3 = isElementPresent(xml.getlocator("//locators/verifystatus2"));
////					log(date3);
//					log("if child Current Status shown than jump to the next step otherwise click on the refresh button");
////					WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
////					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
////					date3 = dateFormat.format(new Date(System.currentTimeMillis()));
////					System.out.println(date1234);
////					System.out.println(date234);
////					Thread.sleep(1000);
////					Pagerefresh();
////					if (date1234.trim().equals(date234.trim())) 
////					{
////						System.out.println(
////								"System is waiting for 20 minutes to change status, But still status in not changed!");
////						Assert.fail("System is taking longer time to change Status from InProgress to Complted!");
////						break;
////					}
////				}
////				while (flag3 == false);
////				Thread.sleep(5000);
//				log("childcurrent status id successfully submited");
////				Assert.assertEquals(ChildCurrentStat.get(), "Completed", "Child status is " + ChildCurrentStat.get());
////				String ParentCurrentStatus13 = Gettext(getwebelement(xml.getlocator("//locators/ParentStatus")));
////				ParentCurrentStat.set(ParentCurrentStatus13);
////				log("ParentCurrentStatus shown: " + ParentCurrentStat.get());
////				WaitforElementtobeclickable(xml.getlocator("//locators/ManagePorting"));
////				Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
//				log("now again check the parent status");
////				Thread.sleep(1000);
////				WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequesthover")));
////				Clickon(getwebelement(xml.getlocator("//locators/updateportinRequesthover")));
//				log("click on the update port in request");
////				SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),ParentCurrentStat.get().toString().trim().toLowerCase());
////				Thread.sleep(5000);
////				log("send the transaction ID: " + ParentCurrentStat.get());
////				WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
////				Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
//				log("click on the search button");
////				Thread.sleep(3000);
////				String CurrentStatus5 = Gettext(getwebelement(xml.getlocator("//locators/currentStatusPortin")));
////				CurrentStatus.set(CurrentStatus5);
////				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: CurrentStatus is : " + CurrentStatus.get());
//				log("CurrentStatus received");
////				Assert.assertTrue(CurrentStatus.get().contains("Completed"), "last step unresponsivesss");
//				log("Port In scenerion is completed successfully!!");
////			
				//changes 2 end
				
			} 
			else 
			{
			}
		}
	}
	public void portoutforreserverd(Object[][] Inputdata)throws Exception 
	{
		for (int i = 0; i < Inputdata.length; i++) 
		{
//				if(Inputdata[i][5].toString()!=("Port-out"))
//			{
//					System.out.println("ok");
//		Clickon(getwebelement(xml.getlocator("//locators/TransactionId")));
//		//String winHandleBefore = driver.getWindowHandle();
//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Transaction ID");
//		Thread.sleep(5000);
//		Set<String> handles = driver.getWindowHandles();
//		Iterator<String> iterator = handles.iterator();
//		String parent = iterator.next();
//		String curent = iterator.next();
//		System.out.println("Window handel" + curent);
//		driver.switchTo().window(curent);
//		// Perform the actions on new window
//		//String LocalAreaCode = null;
//		implicitwait(20);
//		boolean flag = false;
//		String date3 = null;
//		DateFormat dateFormat = new SimpleDateFormat("HH:mm");
//		//String date1 = dateFormat.format(new Date(System.currentTimeMillis()));
//		Date dd = new Date(System.currentTimeMillis() + 20 * 60 * 1000);
//		String date2 = dateFormat.format(dd);
//		do {
//			Thread.sleep(2000);
//			// TransactionStatus1 =
//			// Gettext(getwebelement(xml.getlocator("//locators/TransactionStatus")));
//			Pagerefresh();
//			Pagerefresh();
//			flag = isElementPresent(xml.getlocator("//locators/CompletedTxStatus"));
//			date3 = dateFormat.format(new Date(System.currentTimeMillis()));
//			System.out.println(date3);
//			System.out.println(date2);
//			Thread.sleep(1000);
//			Pagerefresh();
//			if (date3.trim().equals(date2.trim())) 
//			{
//				System.out.println("System is waiting for 20 minutes to change status, But still status in not changed!");
//				Assert.fail("System is taking longer time to change Status from InProgress to Complted!");
//				break;
//			}
//		}
//		while (flag == false);
//		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Status :completed for Activation number");
//		String mainNumber = Gettext(getwebelement(xml.getlocator("//locators/mainNumberone")));
//		ExtentTestManager.getTest().log(LogStatus.PASS," Step: get the main number from Activated number ");
//		MainNumber.set(mainNumber);
//		String rangestart = Gettext(getwebelement(xml.getlocator("//locators/RangeStartone")));
//		ExtentTestManager.getTest().log(LogStatus.PASS," Step: get the Range start number from Activated number ");
//		StartRange.set(rangestart);
//		String rangeEnd = Gettext(getwebelement(xml.getlocator("//locators/RangeEndone")));
//		ExtentTestManager.getTest().log(LogStatus.PASS," Step: get the Range end number from Activated number ");
//		EndRange.set(rangeEnd);
//		String postcode = Gettext(getwebelement(xml.getlocator("//locators/PostCodeforport-out")));
//		ExtentTestManager.getTest().log(LogStatus.PASS," Step: get the PostCode from Activated number ");
//		PostCode.set(postcode);
//		driver.switchTo().window(parent);
//			}
		Thread.sleep(2000);
		
		Moveon(getwebelement(xml.getlocator("//locators/ManagePorting")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Move on to manage porting ");
		Thread.sleep(2000);
		Clickon(getwebelement(xml.getlocator("//locators/RequestPort-out")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Request Port-out");
		implicitwait(10);
		Thread.sleep(5000);
//		int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
//		int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));
//		System.out.println(allData);
//		for (int k = 0; k <= allData; k++) 
//		{
//			if (k != 0 && k % 10 == 0) 
//			{
//				Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
//				Thread.sleep(2000);
//			}
//			Thread.sleep(1000);
//			String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
//			System.out.println(data);
//			if (data.contains(Inputdata[i][3].toString().trim())|| data == Inputdata[i][3].toString().trim()) 
//			{
//				Thread.sleep(2000);
//				try 
//				{
//					safeJavaScriptClick(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
//					System.out.println(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
//				} 
//				catch (Exception e) 
//				{
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on Service profile: "+ Inputdata[i][3].toString().trim());
//				Thread.sleep(2000);
//				break;
//			}
//		}	
		int allData = getwebelementscount(xml.getlocator("//locators/CommonANH"));//
		System.out.println(allData);
		for (int k = 0; k <= allData; k++) 
		{
			if (k != 0 && k % 10 == 0) 
			{
				Clickon(getwebelement(xml.getlocator("//locators/NEXT")));
				log("click on ANH and next button");
			}
			Thread.sleep(1000); 
			String data = GetText(getwebelement(xml.getlocator("//locators/ServiceProfileone").replace("index", String.valueOf(k + 1))));
			System.out.println(data);
			Thread.sleep(1000);
			if (data.contains(Inputdata[i][3].toString().trim()) || data == Inputdata[i][3].toString().trim()) 
			{
				Thread.sleep(2000);
				safeJavaScriptClick(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
				System.out.println(getwebelement(xml.getlocator("//locators/ANH").replace("serviceprofile", Inputdata[i][3].toString())));
				log("click on ANH");
				// Clickon(getwebelement(xml.getlocator("//locators/ANH")));
				Thread.sleep(2000);
				break;
			}
		}

		switch(Inputdata[i][2].toString())
		{
		case "UNITED KINGDOM":
		{
			Select(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
		
			
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Operator
			// Name "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "AUSTRIA":
		{
			SendKeys(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the OperatorName "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "ITALY":
		{
			SendKeys(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Operator Name "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			Thread.sleep(3000);
			break;
		}
		case "SWITZERLAND":
		{
			//BFI
			SendKeys(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the OperatorName "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			Thread.sleep(3000);
			break;
		}
		case "SWEDEN":
		{
			//BE4
			Select(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the OperatorName "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "IRELAND":
		{
			//BE2
			Select(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Operator Name "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "NETHERLANDS":
		{
			//A93
			break;
		}
		case "PORTUGAL":
		{
			Select(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);	int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the OperatorName "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "SPAIN":
		{
			Select(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),	Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Operator
			// Name "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "GERMANY":
		{
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][99].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Operator
			// Name "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/localareacode2")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeStart2")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEnd2")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					//Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					//SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(3000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "FRANCE":
		{
			SendKeys(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Operator
			// Name "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")),Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "BELGIUM":
		{
			SendKeys(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			// ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Operator
			// Name "+Inputdata[i][23].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			Select(getwebelement(xml.getlocator("//locators/Postcode-portout")),Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		case "DENMARK":
		{
			SendKeys(getwebelement(xml.getlocator("//locators/LocalAreaCodeonePortout")),Inputdata[i][5].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: gSelect the Local Area code: " + Inputdata[i][5].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/mainNumberportout")),Inputdata[i][13].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the main number for port-out number:  " + Inputdata[i][13].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Rangestartport-out")),Inputdata[i][14].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range Start number for port-out: " + Inputdata[i][14].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/RangeEndport-out")), Inputdata[i][15].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the Range End number for port-out: " + Inputdata[i][15].toString());
			implicitwait(20);
			int lent=Integer.parseInt((String) Inputdata[i][160]);
			//161
			if(lent > 1 && lent < 21)
			{
				int afterSecond=lent-1;
				int count=161;
				for(int total=0;total<afterSecond;total++)
				{
					Clickon(getwebelement(xml.getlocator("//locators/AddbtnPort-In")));
					Thread.sleep(2000);
					//SendKeys(getwebelement(xml.getlocator("//locators/MainBillingNumberFor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//local are code 
					Select(getwebelement(xml.getlocator("//locators/PortoutLocalAreaCode3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Main Number
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutMainNumber2").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range Start
					SendKeys(getwebelement(xml.getlocator("//locators/PortoutRangeStart").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//Range End
					/*count=171*/
					SendKeys(getwebelement(xml.getlocator("//locators/potoutRangeTo").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
					//current Provider
					/*count=172*/
					//Select(getwebelement(xml.getlocator("//locators/CurrentProviderfor3").replace("number", String.valueOf(total + 1))), Inputdata[i][count].toString());
					implicitwait(5);
					count++;
				}
			}
		
			Thread.sleep(4000);
			javaexecutotSendKeys(Inputdata[i][27].toString(),getwebelement(xml.getlocator("//locators/portoutdate")));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Port-out date from Calender: "+ Inputdata[i][27].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/OperatorName-Portout")),Inputdata[i][28].toString());
			log(" Step: Enter the Operator Name:  " + Inputdata[i][28].toString());
			SendKeys(getwebelement(xml.getlocator("//locators/Postcode-portout")), Inputdata[i][12].toString());
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Enter the PostCode for port-out: " + Inputdata[i][12].toString());
			implicitwait(20);
			Thread.sleep(3000);
			break;
		}
		default:
		{
			RedLog("please select the valid country");
			break;
		}
		}		
		Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step:Click on submit button");
		Thread.sleep(1000);
		waitandForElementDisplayed(xml.getlocator("//locators/TransactionResult"));
		String portoutTX = GetText(getwebelement(xml.getlocator("//locators/TransactionResult")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: getting the Transaction ID");
		log(portoutTX);
		TransactionIdForPortout.set(portoutTX);
		WaitforElementtobeclickable((xml.getlocator("//locators/updateportinRequest")));
		Clickon(getwebelement(xml.getlocator("//locators/updateportinRequest")));
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on For update Portin Request");
		waitandForElementDisplayed(xml.getlocator("//locators/statusQueryTransactionID"));
		log(TransactionIdForPortout.get().toString().trim().toLowerCase());
		SendKeys(getwebelement(xml.getlocator("//locators/statusQueryTransactionID")),TransactionIdForPortout.get().toString().trim().toLowerCase());
		Thread.sleep(5000);
		System.out.println(TransactionIdForPortout.get());
		WaitforElementtobeclickable((xml.getlocator("//locators/StatusQuerySearchbutton")));
		Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Search button ");
		Clickon(getwebelement(xml.getlocator("//locators/StatusQuerySearchbutton")));
		Thread.sleep(2000);
		WaitforElementtobeclickable((xml.getlocator("//locators/portout-Go")));
		Clickon(getwebelement(xml.getlocator("//locators/portout-Go")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Go button ");
		waitandForElementDisplayed(xml.getlocator("//locators/transactionType-portout"));
		Select(getwebelement(xml.getlocator("//locators/transactionType-portout")),Inputdata[i][29].toString());
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: the Tranasction Type: " + Inputdata[i][29].toString());
		Thread.sleep(1000);
		waitandForElementDisplayed(xml.getlocator("//locators/TransactionID-forPortout"));
		log(TransactionIdForPortout.get().toString().trim());
		SendKeys(getwebelement(xml.getlocator("//locators/TransactionID-forPortout")),TransactionIdForPortout.get().toString().trim().toLowerCase());
		waitandForElementDisplayed(xml.getlocator("//locators/changeType-portout"));
		ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Enter the Transaction ID for port-out Request: "+ Inputdata[i][30].toString());
		Select(getwebelement(xml.getlocator("//locators/changeType-portout")),Inputdata[i][30].toString());
		Thread.sleep(1000);
		waitandForElementDisplayed(xml.getlocator("//locators/NewStatus-portout"));
		Select(getwebelement(xml.getlocator("//locators/NewStatus-portout")),Inputdata[i][31].toString());
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Select the Status from dropdown: " + Inputdata[i][31].toString());
		WaitforElementtobeclickable((xml.getlocator("//locators/SubmitButton")));
		Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Submit button");
		implicitwait(20);
		Clickon(getwebelement(xml.getlocator("//locators/TransactionResult")));
		//String winHandleBefore1 = driver.getWindowHandle();
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Click on Transaction Id to validate the Trasaction status");
		Thread.sleep(5000);
		Set<String> handles0 = driver.getWindowHandles();
		Iterator<String> iterator0 = handles0.iterator();
		//String parent0 = iterator0.next();
		String curent0 = iterator0.next();
		System.out.println("Window handel" + curent0);
		driver.switchTo().window(curent0);
		// Perform the actions on new window
		//String LocalAreaCode0 = null;
		implicitwait(20);
		boolean flag0 = false;
		do
		{
			Thread.sleep(2000);
			Pagerefresh();
			Pagerefresh();
			flag0 = isElementPresent(xml.getlocator("//locators/CompletedTxStatus"));
			ExtentTestManager.getTest().log(LogStatus.PASS," Step: Checking the Transaction Status is Completed or not");
			Thread.sleep(1000);
			Pagerefresh();
		}
		while (flag0 == false);
		String TransactionStatus1 = Gettext(getwebelement(xml.getlocator("//locators/CompletedTxStatus")));
		TransactionPortoutstatus.set(TransactionStatus1);
		Assert.assertTrue(TransactionPortoutstatus.get().contains("Completed"));
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Assertion Status has been passed for port -out");
		ExtentTestManager.getTest().log(LogStatus.PASS," Step: Transaction Status has been Completed for port-out");
	}
	}

	public void ActivateSwitchCountriesName(Object[][] Inputdata) throws IOException, InterruptedException, DocumentException
	{
		for (int i = 0; i < Inputdata.length; i++) 
		{
			switch(Inputdata[i][2].toString())
			{
				case "UNITED KINGDOM":
				{
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
//					if(isElementPresent(xml.getlocator("//locators/updatecustomerRef")))
//					{
//					SendKeys(getwebelement(xml.getlocator("//locators/updatecustomerRef")),Inputdata[i][276].toString());
//					//log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
//					}
					//NumActivUpdatedCustRefrence
					SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building name: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Thread.sleep(1000);
						Clickon(getwebelement(xml.getlocator("//locators/DSUyes")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						Select(getwebelement(xml.getlocator("//locators/ActiavteLineType")),Inputdata[i][116].toString());
						log("Selct the Line Type:-"+Inputdata[i][116].toString());
//						Select(getwebelement(xml.getlocator("//locators/Activatetarrif")),Inputdata[i][117].toString());
//						log("Selct the tarrif:-"+Inputdata[i][117].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateEntryType")),Inputdata[i][118].toString());
						log("Selct the Entry Type:-"+Inputdata[i][118].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateTypeface")),Inputdata[i][119].toString());
						log("Selct the Activate Typeface:-"+Inputdata[i][119].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingCategory")),Inputdata[i][120].toString());
						log("Selct the Activate ListingCategory:-"+Inputdata[i][120].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingType")),Inputdata[i][121].toString());
						log("Selct the Activate ActivateListingType:-"+Inputdata[i][121].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivatePriority")),Inputdata[i][122].toString());
						log("Selct the Activate Priority:-"+Inputdata[i][122].toString());
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					break;
				}
				case "ITALY":
				{
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					Select(getwebelement(xml.getlocator("//locators/freeacprovience")), Inputdata[i][25].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Fill provience: " + Inputdata[i][25].toString());
					Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());	
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Fill the Post Code: " + Inputdata[i][12].toString());							
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/checkboxforDSU")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/siretNumber")), Inputdata[i][88].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Fill the VAT number: " + Inputdata[i][88].toString());	
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivationProvience")), Inputdata[i][132].toString());
						log("Step: fill the Activation Provience:-"+Inputdata[i][132].toString());
						Select(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						if(Inputdata[i][133].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/ActivationCheckboxmail")));
							log("click on the Send Advertising mail check box");
						}
						if(Inputdata[i][134].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/amalgamate")));
							log("click on the Amalgamate Numbers with VAT/Tax code check box");
						}
						if(Inputdata[i][135].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/ActivationReceivedmail")));
							log("click on the Receive Advertising calls check box");
						}
						if(Inputdata[i][136].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/Searchbasisontelephonenumber")));
							log("click on the Search on the basis of telephone number check box");
						}	
					}		
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					break;	
				}
				case "GERMANY":
				{
					
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
						javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth")));
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					}
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][8].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS,	" Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street field: " + Inputdata[i][10].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
//					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
//					if(Inputdata[i][101].toString().equals("Yes"))
//					{
//						Clickon(getwebelement(xml.getlocator("//locators/DSUyes")));
//						log("click on the Directory service update check box");
//						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
//						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
//						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
//						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
//						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
//						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
//						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
//						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
//						Thread.sleep(3000);
//					}
					Thread.sleep(3000);
					implicitwait(5);	
					//Thread.sleep(2000);
					WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
					Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
					//Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
					//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
					
					
					
					Thread.sleep(10000);
//					Set<String> handles = driver.getWindowHandles();
//					Iterator<String> iterator = handles.iterator();
//					String parent = iterator.next();
//					String curent = iterator.next();
//					System.out.println("Window handel" + curent);
//					driver.switchTo().window(curent);
					 String parentWinHandle = driver.getWindowHandle();
						Set<String> totalopenwindow=driver.getWindowHandles();
						if(totalopenwindow.size()>1) 
						{
						for(String handle: totalopenwindow)
						{
				            if(!handle.equals(parentWinHandle))
				            {
				            driver.switchTo().window(handle);
				            
				            }
						}
						}
					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
					{
						RedLog("Excel sheet data are incorrect");
					}
					else
					{//
						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
						Thread.sleep(2000);
						try 
						{
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						} 
						catch (Exception e) 
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
					}
					Thread.sleep(2000);
//					driver.close();
//					Thread.sleep(2000);
					driver.switchTo().window(parentWinHandle);
//					
//					if(Inputdata[i][105].toString().equals("List Number Options "))
//					{
//						Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
//						if(Inputdata[i][106].toString().equals("Yes"))
//						{
//							Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
//						}
//						if(Inputdata[i][107].toString().equals("Yes"))
//						{
//							Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
//							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
//						}
//					}
//					else
//					{
//						Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
//					}
//					WaitforElementtobeclickable(xml.getlocator("//locators/btnSubmit"));
//					Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
//					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
//					
					
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);								
					break;
				}
				case "SPAIN":
				{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					if(Inputdata[i][81].toString().equals("RFS"))
					{
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
								Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
								Inputdata[i][9].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the building number Field: " + Inputdata[i][9].toString());
						Select(getwebelement(xml.getlocator("//locators/freeacprovience")), Inputdata[i][25].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the City Field: " + Inputdata[i][11].toString());								
						
						SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the Post Code: " + Inputdata[i][12].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),
								Inputdata[i][8].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the Building name field: " + Inputdata[i][8].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the Building name field: " + Inputdata[i][8].toString());
						Select(getwebelement(xml.getlocator("//locators/cityandtown")), Inputdata[i][11].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the City Field: " + Inputdata[i][11].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
						log("NIF is:-"+Inputdata[i][88].toString());
						implicitwait(5);						
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					}
					
					
//					if(Inputdata[i][81].toString().equals("RFS"))
//					{
//						Select(getwebelement(xml.getlocator("//locators/freeacprovience")), Inputdata[i][25].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the provience: " + Inputdata[i][25].toString());
//						Select(getwebelement(xml.getlocator("//locators/selectCity")), Inputdata[i][11].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
//						log("NIF is:-"+Inputdata[i][88].toString());
//						implicitwait(5);	
//						Thread.sleep(2000);
//						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
//						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
//						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
//					}
					else
					{
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
						
						SendKeys(getwebelement(xml.getlocator("//locators/ActivationStreetNumber")),Inputdata[i][9].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street number: " + Inputdata[i][9].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivationStreetType")),Inputdata[i][104].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street type: " + Inputdata[i][104].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/provinceUpdate")), Inputdata[i][25].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the provience: " + Inputdata[i][25].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/freenif")), Inputdata[i][88].toString().trim());
					log("NIF is:-"+Inputdata[i][88].toString());
					implicitwait(5);	
					Thread.sleep(2000);
					WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
					Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
					
					
					
					Thread.sleep(3000);
//					Set<String> handles = driver.getWindowHandles();
//					Iterator<String> iterator = handles.iterator();
//					String parent = iterator.next();
//					String curent = iterator.next();
//					System.out.println("Window handel" + curent);
//					driver.switchTo().window(curent);
					 String parentWinHandle = driver.getWindowHandle();
						Set<String> totalopenwindow=driver.getWindowHandles();
						if(totalopenwindow.size()>1) 
						{
						for(String handle: totalopenwindow)
						{
				            if(!handle.equals(parentWinHandle))
				            {
				            driver.switchTo().window(handle);
				            
				            }
						}
						}
					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
					{
						RedLog("Excel sheet data are incorrect");
					}
					else
					{//
						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
						Thread.sleep(2000);
						try {
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
					}
					Thread.sleep(2000);
//					driver.close();
//					Thread.sleep(2000);
					driver.switchTo().window(parentWinHandle);
//					
					if(Inputdata[i][105].toString().equals("List Number Options "))
					{
						Clickon(getwebelement(xml.getlocator("//locators/listnumberOptions")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
						if(Inputdata[i][106].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/DirectoryUpdate")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Basic directory entry check box");
						}
						if(Inputdata[i][107].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/salesMarketingUpdate")));
							ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Sales Marketing Entry check box");
						}
					}
					else
					{
						Clickon(getwebelement(xml.getlocator("//locators/UnlistNumberOptions")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the list number options radio button");
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/btnSubmit"));
					Clickon(getwebelement(xml.getlocator("//locators/btnSubmit")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					}
					
					
						
						Thread.sleep(3000);
					
					
					break;
				}
				case "AUSTRIA":
				{
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building name: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/checkboxforDSU")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64]);
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);	
					break;
				}
				case "SWITZERLAND":
				{
					//BFI
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					Select(getwebelement(xml.getlocator("//locators/munciapalityactivate")), Inputdata[i][86].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the muncipality Field: " + Inputdata[i][86].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/DSUyes2")));
						log("click on the Directory service update check box"); 
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64]);
						Select(getwebelement(xml.getlocator("//locators/ActivaitonLanguage")),Inputdata[i][129].toString());
						log("Selct the Activation language:-"+Inputdata[i][129]);	
						if(Inputdata[i][135].toString().equals("Yes"))
						{
							Clickon(getwebelement(xml.getlocator("//locators/ActivationReceivedmail")));
							log("click on the Receive Advertising calls check box");
						}	
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					//munciapalityactivate
					break;
				}
				case "SWEDEN":
				{
					//BE4
					
					
					if(Inputdata[i][81].toString().equals("RFS"))
					{
						if(Inputdata[i][86].toString().equals("Residential"))
						{
							implicitwait(5);
							Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
							implicitwait(5);
							SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
							implicitwait(5);
							SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
							javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth")));

						}
						else 
						{
							implicitwait(5);
							Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
							SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),
									Inputdata[i][7].toString());
							ExtentTestManager.getTest().log(LogStatus.PASS,
									" Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
						}
						SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),
								Inputdata[i][8].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the Building name field: " + Inputdata[i][8].toString());									
						SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the Building name field: " + Inputdata[i][8].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the Post Code: " + Inputdata[i][12].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),
								Inputdata[i][9].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the building number Field: " + Inputdata[i][9].toString());
						Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS,
								" Step: Fill the City Field: " + Inputdata[i][11].toString());
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
						implicitwait(10);
					
					}
					else
					{
						Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
						log("Service Type is:"+Inputdata[i][54].toString());
//						if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
//						{
//						Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
//						log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
//						}
					SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationname")), Inputdata[i][131].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Company Registration nnumber: " + Inputdata[i][131].toString());
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
						//javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth")));
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						//customername=company name for sweden 
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][108].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][108].toString());
					}
					implicitwait(3);
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					implicitwait(3);

					SendKeys(getwebelement(xml.getlocator("//locators/ActivationStreetNumber")),Inputdata[i][103].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street number: " + Inputdata[i][103].toString());	
					implicitwait(3);
					Thread.sleep(2000);
					//citytemp
					if(Inputdata[i][81].toString().equals("RFS"))
					{
						Select(getwebelement(xml.getlocator("//locators/citytemp")), Inputdata[i][11].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
						implicitwait(3);
					}
					else
					{
					SendKeys(getwebelement(xml.getlocator("//locators/extraCity")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					implicitwait(3);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					
					Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
					Thread.sleep(2000);
					if(Inputdata[i][81].toString().equals("RFS"))
					{
						WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
						Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					}
					else
					{
					WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
					Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
					
					
					
					Thread.sleep(3000);
//					Set<String> handles = driver.getWindowHandles();
//					Iterator<String> iterator = handles.iterator();
//					String parent = iterator.next();
//					String curent = iterator.next();
//					System.out.println("Window handel" + curent);
//					driver.switchTo().window(curent);
					 String parentWinHandle = driver.getWindowHandle();
						Set<String> totalopenwindow=driver.getWindowHandles();
						if(totalopenwindow.size()>1) 
						{
						for(String handle: totalopenwindow)
						{
				            if(!handle.equals(parentWinHandle))
				            {
				            driver.switchTo().window(handle);
				            
				            }
						}
						}
					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
					{
						RedLog("Excel sheet data are incorrect");
					}
					else
					{//
						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
						Thread.sleep(2000);
						try {
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
					}
					Thread.sleep(2000);
//					driver.close();
//					Thread.sleep(2000);
					driver.switchTo().window(parentWinHandle);
//					
					Thread.sleep(3000);
//					Select(getwebelement(xml.getlocator("//locators/secretListing")), Inputdata[i][109].toString());
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the secret listing: " + Inputdata[i][109].toString());
					Thread.sleep(5000);
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					}
					}
					break;
				}
				case "IRELAND":
				{
					//BE2
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					}
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][8].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());	
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					if(Inputdata[i][101].toString().equals("Yes"))
					{ 
						Clickon(getwebelement(xml.getlocator("//locators/DSUyes"))); 
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						Select(getwebelement(xml.getlocator("//locators/selectCity")), Inputdata[i][114].toString()); 
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						Select(getwebelement(xml.getlocator("//locators/ActiavteLineType")),Inputdata[i][116].toString());
						log("Selct the Line Type:-"+Inputdata[i][116].toString());
//						Select(getwebelement(xml.getlocator("//locators/Activatetarrif")),Inputdata[i][117].toString());
//						log("Selct the tarrif:-"+Inputdata[i][117].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateEntryType")),Inputdata[i][118].toString());
						log("Selct the Entry Type:-"+Inputdata[i][118].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateTypeface")),Inputdata[i][119].toString());
						log("Selct the Activate Typeface:-"+Inputdata[i][119].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingCategory")),Inputdata[i][120].toString());
						log("Selct the Activate ListingCategory:-"+Inputdata[i][120].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivateListingType")),Inputdata[i][121].toString());
						log("Selct the Activate ActivateListingType:-"+Inputdata[i][121].toString());
						Select(getwebelement(xml.getlocator("//locators/ActivatePriority")),Inputdata[i][122].toString());
						log("Selct the Activate Priority:-"+Inputdata[i][122].toString());
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					break;	
				}
				case "NETHERLANDS":
				{
					//A93
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
							{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
							}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the vat adding number Field: " + Inputdata[i][88].toString());
					}
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());
					Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					if(Inputdata[i][101].toString().equals("Yes"))
					{
					
						Clickon(getwebelement(xml.getlocator("//locators/checkboxforDSU")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					break;
				}
				case "PORTUGAL":
				{
				//BFE
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
			Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
			log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/bi")),Inputdata[i][89].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the vat adding number Field: " + Inputdata[i][89].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/portugalnif")),Inputdata[i][88].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the vat adding number Field: " + Inputdata[i][88].toString());
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/nif")),Inputdata[i][88].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the vat adding number Field: " + Inputdata[i][88].toString());
					}
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][8].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/DSUyes2")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
//						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
//						log("Selct the order type for DSU:-"+Inputdata[i][102]);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
//						Select(getwebelement(xml.getlocator("//locators/ActiavteCity")), Inputdata[i][114].toString());
//						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					break;	
				}	
				case "FRANCE":
				{
					//A29
					if(isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate")))
					{
			Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
			log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					
					SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][8].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());	
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					Select(getwebelement(xml.getlocator("//locators/departmentaddingnumber")),Inputdata[i][92].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the department: " + Inputdata[i][92].toString());
					Select(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					if(Inputdata[i][101].toString().equals("Yes"))
					{
						Clickon(getwebelement(xml.getlocator("//locators/DSUyes")));
						log("click on the Directory service update check box");
						Thread.sleep(3000);
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateCustomernmae")), Inputdata[i][110].toString());
						log("Step: fill the Business/End Customer Name:-"+Inputdata[i][110].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingName")), Inputdata[i][111].toString());
						log("Step: fill the Activation Building name:-"+Inputdata[i][111].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingNumber")), Inputdata[i][112].toString());
						log("Step: fill the Activation Building Number:-"+Inputdata[i][112].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivateBuildingStreeet")), Inputdata[i][113].toString());
						log("Step: fill the Activation Building Street:-"+Inputdata[i][113].toString());
						Select(getwebelement(xml.getlocator("//locators/numberStatusDepartment")), Inputdata[i][130].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Department for DSU: " + Inputdata[i][130].toString());
						Select(getwebelement(xml.getlocator("//locators/selectCity")), Inputdata[i][114].toString());
						log("Step: fill the Activation Building City/town:-"+Inputdata[i][114].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivatePostcode")), Inputdata[i][115].toString());
						log("Step: fill the Activation Building post code:-"+Inputdata[i][115].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/CompanyRegistrationNumber")), Inputdata[i][131].toString());
						log("Step: fill the Comapny Registration Number:-"+Inputdata[i][131].toString());
						SendKeys(getwebelement(xml.getlocator("//locators/ActivationEmailID")), Inputdata[i][85].toString());
						log("Step: fill the Comapny ActivationEmailID:-"+Inputdata[i][85].toString());	
					}
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					break;
				}
				case "BELGIUM":
				{
				//A92
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					if (isElementPresent(xml.getlocator("//locators/ServiceTypeUpdate"))) 
					{
					Select(getwebelement(xml.getlocator("//locators/ServiceTypeUpdate")), Inputdata[i][54].toString());
					log("Service Type is:"+Inputdata[i][54].toString());
					}
					if(Inputdata[i][86].toString().equals("Residential"))
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radioresed")));
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/firstnameofsw")), Inputdata[i][82].toString().trim());
						implicitwait(5);
						SendKeys(getwebelement(xml.getlocator("//locators/lastnameofsw")), Inputdata[i][83].toString().trim());
//						javascriptInput(Inputdata[i][91].toString(),getwebelement(xml.getlocator("//locators/dateofbirth")));
//						SendKeys(getwebelement(xml.getlocator("//locators/customertitleaddingnumber")), Inputdata[i][94].toString().trim());	
						//customertitleaddingnumber
					}
					else 
					{
						implicitwait(5);
						Clickon(getwebelement(xml.getlocator("//locators/radiobussiness")));
						SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/vataddingnumber")),Inputdata[i][88].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the vat adding number Field: " + Inputdata[i][88].toString());
//						SendKeys(getwebelement(xml.getlocator("//locators/registerednameaddingnumber")),Inputdata[i][95].toString());
//						ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the vat adding number Field: " + Inputdata[i][95].toString());
						//registerednameaddingnumber						
					}
					Select(getwebelement(xml.getlocator("//locators/customerlanguage")),Inputdata[i][96].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the vat adding number Field: " + Inputdata[i][96].toString());
//					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][8].toString());
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][8].toString());	
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the street: " + Inputdata[i][10].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/city")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					Thread.sleep(2000);
//					Clickon(getwebelement(xml.getlocator("//locators/postcodebel")));
//					Select(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
//					SendKeys(getwebelement(xml.getlocator("//locators/postcodebel")), Inputdata[i][12].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/postcodebel")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
//					SendKeys(getwebelement(xml.getlocator("//locators/AddressExtension")), Inputdata[i][].toString());
//					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the AddressExtension: " + Inputdata[i][].toString());
					//AddressExtension
					
//					if(Inputdata[i][101].toString().equals("Yes"))
//					{
//						Clickon(getwebelement(xml.getlocator("//locators/checkboxforDSU")));
//						log("click on the Directory service update check box");
//						Thread.sleep(3000);
////						Select(getwebelement(xml.getlocator("//locators/OderTypeDSU")),Inputdata[i][102].toString());
////						log("Selct the order type for DSU:-"+Inputdata[i][102]);
//						SendKeys(getwebelement(xml.getlocator("//locators/telephoneNumberDSU")), Inputdata[i][64].toString());
//						log("Step: fill the Telephone number for DSU:-"+Inputdata[i][64].toString());
//						Select(getwebelement(xml.getlocator("//locators/whitepages")),Inputdata[i][123].toString());
//						log("Selct the white pages:-"+Inputdata[i][123]);
//						Select(getwebelement(xml.getlocator("//locators/ElectronicMedia")),Inputdata[i][124].toString());
//						log("Selct the Electronic Media:-"+Inputdata[i][124]);
//						Select(getwebelement(xml.getlocator("//locators/ActivationDirectoryExsistence")),Inputdata[i][125].toString());
//						log("Selct the directory exsistence:-"+Inputdata[i][125]);
//						Select(getwebelement(xml.getlocator("//locators/ActivationExcportSales")),Inputdata[i][126].toString());
//						log("Selct the export sales:-"+Inputdata[i][126]);
//						Select(getwebelement(xml.getlocator("//locators/ActivationReserve")),Inputdata[i][127].toString());
//						log("Selct the Reserve Query:-"+Inputdata[i][127]);
//						Select(getwebelement(xml.getlocator("//locators/Activationservicetype2")),Inputdata[i][137].toString());
//						log("Selct the Activation service type 2:-"+Inputdata[i][137]);
//						Select(getwebelement(xml.getlocator("//locators/ActivationDeviceType")),Inputdata[i][128].toString());
//						log("Selct the Activation DeviceType:-"+Inputdata[i][128]);
//						Select(getwebelement(xml.getlocator("//locators/ActivaitonLanguage")),Inputdata[i][129].toString());
//						log("Selct the Activation language:-"+Inputdata[i][129]);	
//					}
					Select(getwebelement(xml.getlocator("//locators/DirectroyListingOptions")),Inputdata[i][109].toString());
					log("Selct the Directroy Listing Options:-"+Inputdata[i][109]);
					Thread.sleep(3000);
					implicitwait(5);	
					Thread.sleep(2000);
					WaitforElementtobeclickable(xml.getlocator("//locators/Validbutotn"));
					Clickon(getwebelement(xml.getlocator("//locators/Validbutotn")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Valid Address button");
					
					
					
					Thread.sleep(10000);
//					Set<String> handles = driver.getWindowHandles();
//					Iterator<String> iterator = handles.iterator();
//					String parent = iterator.next();
//					String curent = iterator.next();
//					System.out.println("Window handel" + curent);
//					driver.switchTo().window(curent);
					 String parentWinHandle = driver.getWindowHandle();
						Set<String> totalopenwindow=driver.getWindowHandles();
						if(totalopenwindow.size()>1) 
						{
						for(String handle: totalopenwindow)
						{
				            if(!handle.equals(parentWinHandle))
				            {
				            driver.switchTo().window(handle);
				            
				            }
						}
						}
					if (isElementPresent(xml.getlocator("//locators/errortxt"))) 
					{
						RedLog("Excel sheet data are incorrect");
					}
					else
					{//
						Clickon(getwebelement(xml.getlocator("//locators/secondradiowindow")));
						Thread.sleep(2000);
						try {
							safeJavaScriptClick(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						//Clickon(getwebelement(xml.getlocator("//locators/valdiateAddressupdate")));
						ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the close button");
					}
					Thread.sleep(2000);
//					driver.close();
//					Thread.sleep(2000);
					driver.switchTo().window(parentWinHandle);
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					//customerlanguage
					break;
				}
				case "DENMARK":
				{
					//BFC
					if(isElementPresent(xml.getlocator("//locators/ActivationSubresellerOCN")))
					{
					Select(getwebelement(xml.getlocator("//locators/ActivationSubresellerOCN")),Inputdata[i][51].toString());
					log("Selct the Activation Subreseller OCN:-"+Inputdata[i][51]);
					}
					SendKeys(getwebelement(xml.getlocator("//locators/CustomerName")),Inputdata[i][7].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Customer name Field: " + Inputdata[i][7].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingName")),Inputdata[i][8].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][8].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/BuildingNumber")),	Inputdata[i][9].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the building number Field: " + Inputdata[i][9].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/Street")), Inputdata[i][10].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Building name field: " + Inputdata[i][10].toString());
					Select(getwebelement(xml.getlocator("//locators/cityyes")), Inputdata[i][11].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the City Field: " + Inputdata[i][11].toString());
					SendKeys(getwebelement(xml.getlocator("//locators/PostCode")), Inputdata[i][12].toString());
					ExtentTestManager.getTest().log(LogStatus.PASS," Step: Fill the Post Code: " + Inputdata[i][12].toString());
					WaitforElementtobeclickable(xml.getlocator("//locators/SubmitButton"));
					Clickon(getwebelement(xml.getlocator("//locators/SubmitButton")));
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Submit button");
					implicitwait(10);
					break;
				}
				default:
				{
					RedLog("please select the valid country");
					break;
				}	
			}
			System.out.println("Switch end");
		}
		System.out.println("loop end and method end also");
	}
	}
			
