package Testscript;

import org.testng.annotations.Test;

import Driver.DataReader;
import Driver.DriverTestcase;

public class NumberHosting extends DriverTestcase {
	
	
	@Test(dataProviderClass=DataReader.class,dataProvider="NumberHosting")
	public void NumberInquiry(Object[][] Data) throws Exception 
	{
		
		
		numberHostingHelper.get().OpenApplication();
		Login.get().Login("NH");
		numberHostingHelper.get().Search(Data);
		numberHostingHelper.get().PortIn(Data);
		
		
	
	
		
	}
	

}
